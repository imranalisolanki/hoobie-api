import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Member, MemberWithRelations } from './member.model';

@model()
export class CardDetails extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @belongsTo(() => Member)
  memberId: string

  @property({
    type: 'number',
    required: true,
  })
  lastFour: number;

  @property({
    type: 'number',
    required: true,
  })
  expMonth: number;

  @property({
    type: 'number',
    required: true,
  })
  expYear: number;

  @property({
    type: 'string',
    required: true,
  })
  cardToken: string;

  @property({
    type: 'string',
    required: true,
  })
  cardId: string;

  @property({
    type: 'string',
  })
  cardType?: string;

  @property({
    type: 'boolean',
    default: false,
  })
  isDefault?: boolean;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;


  constructor(data?: Partial<CardDetails>) {
    super(data);
  }
}

export interface CardDetailsRelations {
  member?: MemberWithRelations,
}

export type CardDetailsWithRelations = CardDetails & CardDetailsRelations;
