import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Job, JobWithRelations } from './job.model';
import { MemberWithRelations, Member } from './member.model';
import { ContractWithRelations, Contract } from './contract.model';

@model({ settings: {} })
export class Dispute extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
    required: true,
  })
  description: string;

  @belongsTo(() => Job)
  jobId: string

  @belongsTo(() => Contract)
  contractId: string

  @belongsTo(() => Member)
  resourceId: string

  @belongsTo(() => Member)
  clientId: string

  @property({
    type: 'string',
    default: 'active',
  })
  state?: string; // active, close, resolve

  @property({
    type: 'string',
  })
  disputeId?: string;

  @property({
    type: 'string',
  })
  memberId?: string;

  @property({
    type: 'string',
  })
  memberType?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;


  constructor(data?: Partial<Dispute>) {
    super(data);
  }
}

export interface DisputeRelations {
  client?: MemberWithRelations,
  resource?: MemberWithRelations,
  job?: JobWithRelations,
  contract?: ContractWithRelations
}

export type DisputeWithRelations = Dispute & DisputeRelations;
