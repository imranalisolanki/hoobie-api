import {Entity, model, property} from '@loopback/repository';

@model()
export class Requirements extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  categoryId: string;

  @property({
    type: 'string',
    required: true,
  })
  requirementName: string;

  @property({
    type: 'date',
  })
  created?: string;


  constructor(data?: Partial<Requirements>) {
    super(data);
  }
}

export interface RequirementsRelations {
  // describe navigational properties here
}

export type RequirementsWithRelations = Requirements & RequirementsRelations;
