import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Member, MemberWithRelations } from './member.model';
import { Job, JobWithRelations } from './job.model';
import { Contract, ContractWithRelations } from './contract.model';

@model()
export class Payment extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'number',
    required: true,
  })
  amount: number;

  @belongsTo(() => Member)
  memberId: string

  @belongsTo(() => Job)
  jobId: string

  @belongsTo(() => Contract)
  contractId: string

  @property({
    type: 'string',
    required: false,
  })
  transactionId: string;

  @property({
    type: 'string',
    required: false,
  })
  jobWorkId: string;

  @belongsTo(() => Member)
  byMemberId: string

  @property({
    type: 'number',
    required: false,
  })
  textAmount: number;

  @property({
    type: 'number',
    required: false,
  })
  transferAmount: number;

  @property({
    type: 'string',
    required: true,
  })
  state: string; // success, error

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    required: false,
    default: () => new Date()
  })
  refunded?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  paymentTransferDate?: string;

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;


  constructor(data?: Partial<Payment>) {
    super(data);
  }
}

export interface PaymentRelations {
  member?: MemberWithRelations,
  byMember?: MemberWithRelations,
  job?: JobWithRelations,
  contract?: ContractWithRelations,
}

export type PaymentWithRelations = Payment & PaymentRelations;
