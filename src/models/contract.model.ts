import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Member, MemberWithRelations } from './member.model';
import { Job, JobWithRelations } from './job.model';
import { Proposal, ProposalWithRelations } from './proposal.model';

@model({ settings: {} })
export class Contract extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'number',
    required: true,
    default: 0,
  })
  amount: number;

  @property({
    type: 'number',
    required: true,
    default: 0,
  })
  time?: number;

  @belongsTo(() => Member)
  resourceId: string

  @belongsTo(() => Member)
  clientId: string

  @belongsTo(() => Member)
  endById: string

  @belongsTo(() => Job)
  jobId: string

  @belongsTo(() => Proposal)
  proposalId: string

  @property({
    type: 'string',
    default: 'active', //active, complete, suspend, hold
  })
  state: string;

  @property({
    type: 'boolean',
    default: false
  })
  resourceCompletedStatus: boolean;

  @property({
    type: 'boolean',
    default: false
  })
  resourceReleaseStatus: boolean;

  @property({
    type: 'boolean',
    default: false
  })
  clientEscrowReleaseStatus: boolean;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'date',
  })
  ended?: string | any;


  constructor(data?: Partial<Contract>) {
    super(data);
  }
}

export interface ContractRelations {
  job?: JobWithRelations,
  client?: MemberWithRelations,
  resource?: MemberWithRelations,
  endBy?: MemberWithRelations,
  proposal?: ProposalWithRelations
}

export type ContractWithRelations = Contract & ContractRelations;
