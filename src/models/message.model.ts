import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Member, MemberWithRelations } from './member.model';

@model({ settings: {} })
export class Message extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  message?: string;

  @belongsTo(() => Member)
  senderId: string

  @belongsTo(() => Member)
  receiverId: string

  @property({
    type: 'string',
    required: true,
  })
  roomId: string;

  @property({
    type: 'string',
  })
  file?: string;

  @property({
    type: 'array',
    itemType: 'string',
  })
  seenBy?: string[];

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;


  constructor(data?: Partial<Message>) {
    super(data);
  }
}

export interface MessageRelations {
  sender?: MemberWithRelations,
  receiver?: MemberWithRelations,
}

export type MessageWithRelations = Message & MessageRelations;
