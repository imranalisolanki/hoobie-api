import { Entity, model, property } from '@loopback/repository';

@model({ settings: {} })
export class TermAndCondition extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  memberId: string;

  @property({
    type: 'string',
    required: false,
  })
  title: string;

  @property({
    type: 'string',
    required: true,
  })
  termCondition: string;

  @property({
    type: 'string',
    required: true,
  })
  type: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  constructor(data?: Partial<TermAndCondition>) {
    super(data);
  }
}

export interface TermAndConditionRelations {
  // describe navigational properties here
}

export type TermAndConditionWithRelations = TermAndCondition & TermAndConditionRelations;
