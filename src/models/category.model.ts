import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Member, MemberWithRelations } from './member.model';

@model({ settings: {} })
export class Category extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  icon?: string;

  @belongsTo(() => Member)
  memberId: string

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;

  @property({
    type: 'number'
  })
  amount?: number;

  constructor(data?: Partial<Category>) {
    super(data);
  }
}

export interface CategoryRelations {
  member?: MemberWithRelations,
}

export type CategoryWithRelations = Category & CategoryRelations;
