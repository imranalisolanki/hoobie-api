import { Entity, model, property } from '@loopback/repository';

@model({ settings: {} })
export class CronLogs extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;
  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;
  @property({
    type: 'number',
    default: 0,
  })
  status?: number;
  @property({
    type: 'string',
  })
  message?: string;

  @property({
    type: 'string',
  })
  jobId?: string;

  constructor(data?: Partial<CronLogs>) {
    super(data);
  }
}

export interface CronLogsRelations {
  // describe navigational properties here
}

export type CronLogsWithRelations = CronLogs & CronLogsRelations;
