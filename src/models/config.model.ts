import { Entity, model, property } from '@loopback/repository';

@model({ settings: {} })
export class Config extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'number',
    required: true,
  })
  clientCharge: number;

  @property({
    type: 'number',
    required: true,
  })
  resourceCharge: number;

  @property({
    type: 'boolean',
    default: false
  })
  chargeType: boolean;

  @property({
    type: 'string',
    required: true,
  })
  memberId?: string;


  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  constructor(data?: Partial<Config>) {
    super(data);
  }
}

export interface ConfigRelations {
  // describe navigational properties here
}

export type ConfigWithRelations = Config & ConfigRelations;
