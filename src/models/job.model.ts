import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Category, CategoryWithRelations } from './category.model';
import { Member, MemberWithRelations } from './member.model';

@model({ settings: {} })
export class Job extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'number',
    required: true,
    default: 0,
  })
  budget: number;

  @property({
    type: 'number',
    default: 0,
  })
  time?: number;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'array',
    itemType: 'string',
    default: [],
  })
  image?: string[];

  @belongsTo(() => Category)
  categoryId: string

  @belongsTo(() => Member)
  memberId: string

  @property({
    type: 'object',
    default: { lat: 0, lng: 0 }
  })
  location?: object;

  @property({
    type: 'array',
    itemType: 'number'
  })
  matchLocation?: number;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'object',
    default: { jobStartDate: "25-01-2020", jobStartTime: "10.15", jobEndDate: "22-02-2020", jobEndTime: "12:30" }
  })
  jobStartObject?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;

  @property({
    type: 'string',
    default: 'active', //active, close
  })
  state?: string;

  @property({
    type: 'string',
    default: '',
  })
  otherCategory?: string;

  @property({
    type: 'array',
    itemType: 'string',
    default: [],
  })
  tag?: string[];

  @property({
    type: 'array',
    itemType: 'object',
    default: [],
  })
  requirements?: object;

  @property({
    type: 'array',
    itemType: 'object',
    default: [],
  })
  timeSlots?: string[];

  @property({
    type: 'string',
    default: '',
  })
  jobType?: string;

  @property({
    type: 'object',
    default: { local: '', remote: '' },
  })
  jobLocation?: object;

  @property({
    type: 'object',
    default: { startDate: '', endDate: '' },
  })
  timePeriod?: string;

  @property({
    type: 'object',
    default: { dailyHours: 5, activeDays: 10, totalHoursCount: 5, selectedHoursType: '', totalDaysCount: 10, timeUnitAndQuantity: 10 },
  })
  jobObject?: string;

  constructor(data?: Partial<Job>) {
    super(data);
  }
}

export interface JobRelations {
  member?: MemberWithRelations,
  category?: CategoryWithRelations,
}

export type JobWithRelations = Job & JobRelations;
