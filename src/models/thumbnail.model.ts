import {Entity, model, property} from '@loopback/repository';

@model()
export class Thumbnail extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  imageUrl?: string;

  @property({
    type: 'date',
  })
  created?: string;


  constructor(data?: Partial<Thumbnail>) {
    super(data);
  }
}

export interface ThumbnailRelations {
  // describe navigational properties here
}

export type ThumbnailWithRelations = Thumbnail & ThumbnailRelations;
