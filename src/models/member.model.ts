import { Entity, model, property, AnyObject, hasMany } from '@loopback/repository';
import { Category } from './category.model';



@model({
  settings: {
    hiddenProperties: ['password']
  }
})
export class Member extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'string',
    default: '',
  })
  name?: string;

  @property({
    type: 'string',
    required: true,
    index: { unique: true }
  })
  email: string;

  @property({
    type: 'string',
    default: '',
  })
  username?: string;

  @property({
    type: 'string',
  })
  password?: string;

  @property({
    type: 'string',
    default: '',
  })
  image?: string;

  @property({
    type: 'string',
    default: '',
  })
  adminLevel?: string;

  @property({
    type: 'string',
    default: '',
  })
  mobile?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;

  @property({
    type: 'number',
    default: 2,
  })
  role?: number; // [0,1,2] 0 => 'admin',1=>'staff',2=>'user'

  @property({
    type: 'object',
    default: { city: '', state: '', country: '', zipcode: '', street1: '', street2: '', fullText: '' },
  })
  address?: object;

  @property({
    type: 'object',
    default: { lat: 0, lng: 0 }
  })
  location?: object;

  @property({
    type: 'array',
    itemType: 'string',
    default: [],
  })
  interestIds?: string[];

  @property({
    type: 'array',
    itemType: 'string',
    default: [],
  })
  saveResourceId?: string[];

  @property({
    type: 'array',
    itemType: 'string',
    default: [],
  })
  saveJobsId?: string[];
  /* @hasMany(() => Category)
  interestIds?: string[]; */

  @property({
    type: 'object',
    default: { title: '', description: '', ratePerHour: 0, hoursPerWeek: 0, rating: 0, successRate: 0 },
  })
  jobSeeker?: AnyObject;

  @property({
    type: 'object',
    default: { title: '', description: '', rating: 0 },
  })
  jobManager?: AnyObject;

  @property({
    type: 'object',
    default: { facebookId: '', twitterId: '', googleId: '' },
  })
  social?: AnyObject;

  @property({
    type: 'boolean',
    default: false,
  })
  isProfileComplete?: boolean;

  @property({
    type: 'boolean',
    default: false,
  })
  isVerified?: boolean;

  @property({
    type: 'number',
    default: null,
  })
  verificationToken?: number;

  @property({
    type: 'string',
    default: '',
  })
  fcmToken?: string;

  @property({
    type: 'string',
    default: '',
  })
  apnsToken?: string;

  @property({
    type: 'string',
    default: ''
  })
  stripeCustomerId?: string;

  @property({
    type: 'string',
    default: ''
  })
  stripeTransferId?: string;

  @property({
    type: 'object',
    default: { stripeUserId: "", currency: "" }
  })
  stripeAccount: AnyObject;

  constructor(data?: Partial<Member>) {
    super(data);
  }
}

export interface MemberRelations {
  // describe navigational properties here
}

export type MemberWithRelations = Member & MemberRelations;
