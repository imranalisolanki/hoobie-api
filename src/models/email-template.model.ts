import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Member, MemberWithRelations } from './member.model';

@model({ settings: {} })
export class EmailTemplate extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  subject: string;

  @property({
    type: 'string',
    required: true,
  })
  message: string;

  @property({
    type: 'string',
    required: true,
  })
  slug: string;

  @belongsTo(() => Member)
  memberId: string

  @property({
    type: 'date',
  })
  created?: string;

  @property({
    type: 'date',
  })
  modified?: string;

  @property({
    type: 'number',
    required: true,
    default: 1,
  })
  status: number;


  constructor(data?: Partial<EmailTemplate>) {
    super(data);
  }
}

export interface EmailTemplateRelations {
  member?: MemberWithRelations,
}

export type EmailTemplateWithRelations = EmailTemplate & EmailTemplateRelations;
