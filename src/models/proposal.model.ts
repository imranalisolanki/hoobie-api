import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Member, MemberWithRelations } from './member.model';
import { Job, JobWithRelations } from './job.model';
import { Invitation, InvitationWithRelations } from './invitation.model';

@model({ settings: {} })
export class Proposal extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @belongsTo(() => Job)
  jobId: string

  @property({
    type: 'number',
    required: true,
  })
  amount: number;

  @property({
    type: 'number',
    default: 0,
  })
  serviceCharge: number;

  @property({
    type: 'number',
    required: true,
  })
  payableAmount: number;

  @property({
    type: 'number',
    required: true,
  })
  time: number;

  @property({
    type: 'string',
    default: '',
  })
  message?: string;

  @belongsTo(() => Member)
  memberId: string

  @belongsTo(() => Invitation)
  invitationId: string

  @property({
    type: 'array',
    itemType: 'object',
    default: [],
  })
  files?: object[];

  @property({
    type: 'array',
    itemType: 'string',
    default: [],
  })
  images?: string[];

  @property({
    type: 'string',
    default: 'submitted', //submitted, discussion, close, decline
  })
  state?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;


  constructor(data?: Partial<Proposal>) {
    super(data);
  }
}

export interface ProposalRelations {
  member?: MemberWithRelations,
  job?: JobWithRelations,
  invitation?: InvitationWithRelations
}

export type ProposalWithRelations = Proposal & ProposalRelations;
