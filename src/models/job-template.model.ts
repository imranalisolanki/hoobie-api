import { Entity, model, property } from '@loopback/repository';

@model({ settings: {} })
export class JobTemplate extends Entity {
  @property({
    type: 'string',
  })
  title?: string;

  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'number',
  })
  budget?: number;

  @property({
    type: 'number',
  })
  time?: number;

  @property({
    type: 'array',
    itemType: 'object',
    default: [],
  })
  requirements?: object;

  @property({
    type: 'object',
    default: { local: '', remote: '' },
  })
  jobLocation?: object;
  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'array',
    itemType: 'string',
  })
  image?: string[];

  @property({
    type: 'string',
  })
  categoryId?: string;

  @property({
    type: 'string',
  })
  memberId?: string;

  @property({
    type: 'string',
  })
  jobType?: string;

  @property({
    type: 'object',
    default: { lat: 0, lng: 0 }
  })
  location?: object;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'string',
  })
  otherCategory?: string;

  @property({
    type: 'array',
    itemType: 'string',
  })
  tag?: string[];

  @property({
    type: 'array',
    itemType: 'object',
    default: [],
  })
  timeSlots?: string[];

  @property({
    type: 'object',
    default: { startDate: '', endDate: '' },
  })
  timePeriod?: string;

  @property({
    type: 'object',
    default: { dailyHours: 5, activeDays: 10, totalHoursCount: 5, selectedHoursType: '', totalDaysCount: 10, timeUnitAndQuantity: 10 },
  })
  jobObject?: string;

  constructor(data?: Partial<JobTemplate>) {
    super(data);
  }
}

export interface JobTemplateRelations {
  // describe navigational properties here
}

export type JobTemplateWithRelations = JobTemplate & JobTemplateRelations;
