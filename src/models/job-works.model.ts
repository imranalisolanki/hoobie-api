import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Member, MemberWithRelations } from './member.model';
import { Job, JobWithRelations } from './job.model';
import { Contract, ContractWithRelations } from './contract.model';

@model({ settings: { strict: false } })
export class JobWorks extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  title?: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'number',
  })
  relesedAmount?: number;

  @property({
    type: 'number',
  })
  hours?: number;

  @property({
    type: 'boolean',
    default: false,
  })
  isReleased?: boolean;

  @property({
    type: 'array',
    itemType: 'string',
  })
  images?: string[];

  @property({
    type: 'array',
    itemType: 'object',
  })
  attachments?: string[];

  @belongsTo(() => Contract)
  contractId: string


  @belongsTo(() => Member)
  memberId: string

  @belongsTo(() => Job)
  jobId: string

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<JobWorks>) {
    super(data);
  }
}

export interface JobWorksRelations {
  member?: MemberWithRelations,
  job?: JobWithRelations,
  contract?: ContractWithRelations,
}

export type JobWorksWithRelations = JobWorks & JobWorksRelations;
