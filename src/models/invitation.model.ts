import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Job, JobWithRelations } from './job.model';
import { Member, MemberWithRelations } from './member.model';

@model({ settings: {} })
export class Invitation extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @belongsTo(() => Job)
  jobId: string

  @belongsTo(() => Member)
  resourceId: string

  @belongsTo(() => Member)
  clientId: string

  @property({
    type: 'string',
    required: true,
  })
  message: string;

  @property({
    type: 'string',
    default: 'active' // active, accept, decline
  })
  state?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;


  constructor(data?: Partial<Invitation>) {
    super(data);
  }
}

export interface InvitationRelations {
  client?: MemberWithRelations,
  resource?: MemberWithRelations,
  job?: JobWithRelations,
}

export type InvitationWithRelations = Invitation & InvitationRelations;
