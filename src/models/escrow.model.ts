import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Member, MemberWithRelations } from './member.model';
import { Job, JobWithRelations } from './job.model';
import { Contract, ContractWithRelations } from './contract.model';

@model({ settings: {} })
export class Escrow extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'number',
    required: true,
  })
  amount: number;


  @belongsTo(() => Member)
  memberId: string

  @belongsTo(() => Job)
  jobId: string

  @property({
    type: 'string',
  })
  state?: string;

  @property({
    type: 'string',
    required: true,
  })
  chargeId: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  escroCreatedAt?: string;

  @property({
    type: 'date',
    required: false,
    default: () => new Date()
  })
  refunded?: string;

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;

  @property({
    type: 'boolean',
    default: false,
  })
  isReleased?: boolean;

  @property({
    type: 'boolean',
    default: false,
  })
  isPending?: boolean;

  @property({
    type: 'number'
  })
  stripeCharges?: number;

  @property({
    type: 'number',
    required: true
  })
  totalPaidamount: number;
  @property({
    type: 'number'
  })
  breezeCharge?: number;

  constructor(data?: Partial<Escrow>) {
    super(data);
  }
}

export interface EscrowRelations {
  member?: MemberWithRelations,
  job?: JobWithRelations,
}

export type EscrowWithRelations = Escrow & EscrowRelations;
