import { Entity, model, property } from '@loopback/repository';

@model({ settings: {} })
export class AdminStaffs extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'string'
  })
  image: string;

  @property({
    type: 'string',
    required: true,
  })
  department: string;

  @property({
    type: 'number',
    required: true,
  })
  role: number;

  @property({
    type: 'number',
    default: 0,
  })
  status?: number;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  constructor(data?: Partial<AdminStaffs>) {
    super(data);
  }
}

export interface AdminStaffsRelations {
  // describe navigational properties here
}

export type AdminStaffsWithRelations = AdminStaffs & AdminStaffsRelations;
