import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Job, JobWithRelations } from './job.model';
import { Member, MemberWithRelations } from './member.model';
import { Contract, ContractWithRelations } from './contract.model';
@model({ settings: {} })
export class RefundPayments extends Entity {
  @property({
    type: 'number',
    required: true,
  })
  amount: number;

  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @belongsTo(() => Member)
  byMemberId: string

  @belongsTo(() => Member)
  toMemberId: string

  @belongsTo(() => Job)
  jobId: string

  @belongsTo(() => Contract)
  contractId: string

  @property({
    type: 'string',
  })
  state?: string;

  @property({
    type: 'number',
    default: 0,
  })
  status?: number;

  @property({
    type: 'string',
    required: true,
  })
  transactionId: string;

  @property({
    type: 'string',
    required: true,
  })
  chargeId: string;
  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  constructor(data?: Partial<RefundPayments>) {
    super(data);
  }
}

export interface RefundPaymentsRelations {
  // describe navigational properties here
  toMember?: MemberWithRelations,
  byMember?: MemberWithRelations,
  job?: JobWithRelations,
  contract?: ContractWithRelations,
}

export type RefundPaymentsWithRelations = RefundPayments & RefundPaymentsRelations;
