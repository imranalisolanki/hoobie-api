import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Job, JobWithRelations } from './job.model';
import { Contract, ContractWithRelations } from './contract.model';
import { Member, MemberWithRelations } from './member.model';

@model({ settings: {} })
export class Review extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'number',
    default: 0,
    jsonSchema: {
      minimum: 0,
      maximum: 5,
    }
  })
  clarity?: number;

  @property({
    type: 'number',
    default: 0,
    jsonSchema: {
      minimum: 0,
      maximum: 5,
    }
  })
  competence?: number;

  @property({
    type: 'number',
    default: 0,
    jsonSchema: {
      minimum: 0,
      maximum: 5,
    }
  })
  character?: number;

  @property({
    type: 'number',
    default: 0,
    jsonSchema: {
      minimum: 0,
      maximum: 5,
    }
  })
  commitment?: number;

  @property({
    type: 'number',
    default: 0,
    jsonSchema: {
      minimum: 0,
      maximum: 5,
    }
  })
  connection?: number;

  @property({
    type: 'number',
    default: 0,
    jsonSchema: {
      minimum: 0,
      maximum: 5,
    }
  })
  contribution?: number;

  @property({
    type: 'number',
    default: 0,
    jsonSchema: {
      minimum: 0,
      maximum: 5,
    }
  })
  compassion?: number;

  @property({
    type: 'number',
    default: 0,
    jsonSchema: {
      minimum: 0,
      maximum: 5,
    }
  })
  rating?: number;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'string',
  })
  description2?: string;

  @property({
    type: 'string',
  })
  description3?: string;

  @property({
    type: 'string',
    required: true
  })
  memberType?: string; // getting person type as a "client or resource"

  @belongsTo(() => Job)
  jobId: string

  @belongsTo(() => Contract)
  contractId: string

  @belongsTo(() => Member)
  byMemberId: string

  @belongsTo(() => Member)
  toMemberId: string

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;


  constructor(data?: Partial<Review>) {
    super(data);
  }
}

export interface ReviewRelations {
  byMember?: MemberWithRelations,
  toMember?: MemberWithRelations,
  job?: JobWithRelations,
  contract?: ContractWithRelations,
}

export type ReviewWithRelations = Review & ReviewRelations;
