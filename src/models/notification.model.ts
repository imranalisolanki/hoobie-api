import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Member, MemberWithRelations } from './member.model';
@model({ settings: {} })
export class Notification extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
    required: true,
  })
  message: string;

  @belongsTo(() => Member)
  memberId: string

  @property({
    type: 'string',
    required: false,
  })
  type: string;
  @property({
    type: 'string',
    required: false,
    default: "",
  })
  userType: string;
  @property({
    notificationObject: 'object',
    required: false,
  })
  notificationObject: object;

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'boolean',
    default: false,
  })
  isSeen?: boolean;


  constructor(data?: Partial<Notification>) {
    super(data);
  }
}

export interface NotificationRelations {
  member?: MemberWithRelations,
}

export type NotificationWithRelations = Notification & NotificationRelations;
