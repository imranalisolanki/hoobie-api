import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Member, MemberWithRelations } from './member.model';

@model({ settings: {} })
export class Support extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
    required: true,
  })
  description: string;

  @property({
    type: 'string',
  })
  ticketId?: string;

  @belongsTo(() => Member)
  memberId: string

  @property({
    type: 'date',
    default: () => new Date()
  })
  created?: string;

  @property({
    type: 'date',
    default: () => new Date()
  })
  modified?: string;

  @property({
    type: 'number',
    default: 1,
  })
  status?: number;


  constructor(data?: Partial<Support>) {
    super(data);
  }
}

export interface SupportRelations {
  member?: MemberWithRelations,
}

export type SupportWithRelations = Support & SupportRelations;
