import { BindingKey } from '@loopback/context';
import { PasswordHasher } from './services/hash.password.bcryptjs';
import { TokenService, UserService } from '@loopback/authentication';
import { Member } from './models';
import { Credentials } from './type-schema';
import { EmailService } from './services/email.service';
import { FCMService, ControllerService } from './services';

export namespace TokenServiceConstants {
  export const TOKEN_SECRET_VALUE = 'amp13T0k3nS3cr3t';
  export const TOKEN_EXPIRES_IN_VALUE = '2592000'; // 30 days(30 * 60 * 60 * 24)
}

export namespace TokenServiceBindings {
  export const TOKEN_SECRET = BindingKey.create<string>('authentication.jwt.secret');
  export const TOKEN_EXPIRES_IN = BindingKey.create<string>('authentication.jwt.expires.in.seconds');
  export const TOKEN_SERVICE = BindingKey.create<TokenService>('services.authentication.jwt.tokenservice');
}

export namespace PasswordHasherBindings {
  export const PASSWORD_HASHER = BindingKey.create<PasswordHasher>('services.hasher');
  export const ROUNDS = BindingKey.create<number>('services.hasher.round');
}

export namespace UserServiceBindings {
  export const USER_SERVICE = BindingKey.create<UserService<Member, Credentials>>('services.user.service');
}

export namespace EmailServiceBindings {
  export const MAIL_SERVICE = BindingKey.create<EmailService>('mailer.services');
}

export namespace FCMServiceBindings {
  export const FCM_SERVICE = BindingKey.create<FCMService>('fcm.push.services')
}

export namespace ControllerServiceBindings {
  export const CONTROLLER_SERVICE = BindingKey.create<ControllerService>('controller.service')
}
