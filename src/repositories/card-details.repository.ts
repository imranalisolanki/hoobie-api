import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { CardDetails, CardDetailsRelations, Member } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { MemberRepository } from './member.repository';

export class CardDetailsRepository extends DefaultCrudRepository<CardDetails, typeof CardDetails.prototype.id, CardDetailsRelations> {
  public readonly member: BelongsToAccessor<Member, typeof CardDetails.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('MemberRepository') public memberRepositoryGetter: Getter<MemberRepository>,
  ) {
    super(CardDetails, dataSource);

    this.member = this.createBelongsToAccessorFor('member', memberRepositoryGetter);

    this.registerInclusionResolver('member', this.member.inclusionResolver);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
