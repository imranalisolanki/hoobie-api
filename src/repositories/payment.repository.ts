import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Payment, PaymentRelations, Member, Job, Contract } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { MemberRepository } from './member.repository';
import { JobRepository } from './job.repository';
import { ContractRepository } from './contract.repository';

export class PaymentRepository extends DefaultCrudRepository<Payment, typeof Payment.prototype.id, PaymentRelations> {
  public readonly member: BelongsToAccessor<Member, typeof Payment.prototype.id>;
  public readonly job: BelongsToAccessor<Job, typeof Payment.prototype.id>;
  public readonly contract: BelongsToAccessor<Contract, typeof Payment.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('MemberRepository') public memberRepositoryGetter: Getter<MemberRepository>,
    @repository.getter('JobRepository') public jobRepositoryGetter: Getter<JobRepository>,
    @repository.getter('ContractRepository') public contractRepositoryGetter: Getter<ContractRepository>,
  ) {
    super(Payment, dataSource);

    this.member = this.createBelongsToAccessorFor('member', memberRepositoryGetter);
    this.job = this.createBelongsToAccessorFor('job', jobRepositoryGetter);
    this.contract = this.createBelongsToAccessorFor('contract', contractRepositoryGetter);

    this.registerInclusionResolver('member', this.member.inclusionResolver);
    this.registerInclusionResolver('job', this.job.inclusionResolver);
    this.registerInclusionResolver('contract', this.contract.inclusionResolver);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
