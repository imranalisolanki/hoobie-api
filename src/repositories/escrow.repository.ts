import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Escrow, EscrowRelations, Member, Job, Contract } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { MemberRepository } from './member.repository';
import { JobRepository } from './job.repository';
import { ContractRepository } from './contract.repository';

export class EscrowRepository extends DefaultCrudRepository<Escrow, typeof Escrow.prototype.id, EscrowRelations> {
  public readonly member: BelongsToAccessor<Member, typeof Escrow.prototype.id>;
  public readonly job: BelongsToAccessor<Job, typeof Escrow.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('MemberRepository') public memberRepositoryGetter: Getter<MemberRepository>,
    @repository.getter('JobRepository') public jobRepositoryGetter: Getter<JobRepository>,
  ) {
    super(Escrow, dataSource);

    this.member = this.createBelongsToAccessorFor('member', memberRepositoryGetter);
    this.job = this.createBelongsToAccessorFor('job', jobRepositoryGetter);

    this.registerInclusionResolver('member', this.member.inclusionResolver);
    this.registerInclusionResolver('job', this.job.inclusionResolver);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
