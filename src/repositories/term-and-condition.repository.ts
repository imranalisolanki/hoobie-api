import { DefaultCrudRepository } from '@loopback/repository';
import { TermAndCondition, TermAndConditionRelations } from '../models';
import { DbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class TermAndConditionRepository extends DefaultCrudRepository<
  TermAndCondition,
  typeof TermAndCondition.prototype.id,
  TermAndConditionRelations
  > {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(TermAndCondition, dataSource);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
