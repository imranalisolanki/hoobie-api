import { DefaultCrudRepository } from '@loopback/repository';
import { Notification, NotificationRelations } from '../models';
import { DbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class NotificationRepository extends DefaultCrudRepository<
  Notification,
  typeof Notification.prototype.id,
  NotificationRelations
  > {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Notification, dataSource);
    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
