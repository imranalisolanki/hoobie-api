import { DefaultCrudRepository } from '@loopback/repository';
import { AdminStaffs, AdminStaffsRelations } from '../models';
import { DbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class AdminStaffsRepository extends DefaultCrudRepository<
  AdminStaffs,
  typeof AdminStaffs.prototype.id,
  AdminStaffsRelations
  > {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(AdminStaffs, dataSource);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
