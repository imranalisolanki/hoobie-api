import { DefaultCrudRepository } from '@loopback/repository';
import { EmailTemplate, EmailTemplateRelations } from '../models';
import { DbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class EmailTemplateRepository extends DefaultCrudRepository<
  EmailTemplate,
  typeof EmailTemplate.prototype.id,
  EmailTemplateRelations
  > {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(EmailTemplate, dataSource);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
