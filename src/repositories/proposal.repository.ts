import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Proposal, ProposalRelations, Member, Job, Invitation } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { MemberRepository } from './member.repository';
import { JobRepository } from './job.repository';
import { InvitationRepository } from './invitation.repository';

export class ProposalRepository extends DefaultCrudRepository<Proposal, typeof Proposal.prototype.id, ProposalRelations> {
  public readonly member: BelongsToAccessor<Member, typeof Proposal.prototype.id>
  public readonly job: BelongsToAccessor<Job, typeof Proposal.prototype.id>
  public readonly invitation: BelongsToAccessor<Invitation, typeof Proposal.prototype.id>

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('MemberRepository') public memberRepositoryGetter: Getter<MemberRepository>,
    @repository.getter('JobRepository') public jobRepositoryGetter: Getter<JobRepository>,
    @repository.getter('InvitationRepository') public invitationRepositoryGetter: Getter<InvitationRepository>
  ) {
    super(Proposal, dataSource);

    this.member = this.createBelongsToAccessorFor('member', memberRepositoryGetter);
    this.job = this.createBelongsToAccessorFor('job', jobRepositoryGetter);
    this.invitation = this.createBelongsToAccessorFor('invitation', invitationRepositoryGetter);

    this.registerInclusionResolver('member', this.member.inclusionResolver);
    this.registerInclusionResolver('job', this.job.inclusionResolver);
    this.registerInclusionResolver('invitation', this.invitation.inclusionResolver);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
