import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Invitation, InvitationRelations, Member, Job } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { MemberRepository } from './member.repository';
import { JobRepository } from './job.repository';

export class InvitationRepository extends DefaultCrudRepository<Invitation, typeof Invitation.prototype.id, InvitationRelations> {
  public readonly client: BelongsToAccessor<Member, typeof Invitation.prototype.id>;
  public readonly resource: BelongsToAccessor<Member, typeof Invitation.prototype.id>;
  public readonly job: BelongsToAccessor<Job, typeof Invitation.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('MemberRepository') public clientRepositoryGetter: Getter<MemberRepository>,
    @repository.getter('MemberRepository') public resourceRepositoryGetter: Getter<MemberRepository>,
    @repository.getter('JobRepository') public jobRepositoryGetter: Getter<JobRepository>,
  ) {
    super(Invitation, dataSource);

    this.client = this.createBelongsToAccessorFor('client', clientRepositoryGetter);
    this.resource = this.createBelongsToAccessorFor('resource', resourceRepositoryGetter);
    this.job = this.createBelongsToAccessorFor('job', jobRepositoryGetter);

    this.registerInclusionResolver('client', this.client.inclusionResolver);
    this.registerInclusionResolver('resource', this.resource.inclusionResolver);
    this.registerInclusionResolver('job', this.job.inclusionResolver);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
