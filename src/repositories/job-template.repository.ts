import {DefaultCrudRepository} from '@loopback/repository';
import {JobTemplate, JobTemplateRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class JobTemplateRepository extends DefaultCrudRepository<
  JobTemplate,
  typeof JobTemplate.prototype.title,
  JobTemplateRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(JobTemplate, dataSource);
  }
}
