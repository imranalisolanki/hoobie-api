import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { JobWorks, JobWorksRelations, Member, Job, Contract } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { MemberRepository } from './member.repository';
import { JobRepository } from './job.repository';
import { ContractRepository } from './contract.repository'

export class JobWorksRepository extends DefaultCrudRepository<JobWorks, typeof JobWorks.prototype.id, JobWorksRelations> {
  public readonly member: BelongsToAccessor<Member, typeof JobWorks.prototype.id>;
  public readonly job: BelongsToAccessor<Job, typeof JobWorks.prototype.id>;
  public readonly Contract: BelongsToAccessor<Contract, typeof JobWorks.prototype.id>;
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('MemberRepository') public memberRepositoryGetter: Getter<MemberRepository>,
    @repository.getter('JobRepository') public jobRepositoryGetter: Getter<JobRepository>,
    @repository.getter('ContractRepository') public ContractRepositoryGetter: Getter<ContractRepository>,
  ) {
    super(JobWorks, dataSource);

    this.member = this.createBelongsToAccessorFor('member', memberRepositoryGetter);
    this.job = this.createBelongsToAccessorFor('job', jobRepositoryGetter);
    this.Contract = this.createBelongsToAccessorFor('contract', ContractRepositoryGetter)

    this.registerInclusionResolver('member', this.member.inclusionResolver);
    this.registerInclusionResolver('job', this.job.inclusionResolver);
    this.registerInclusionResolver('contract', this.Contract.inclusionResolver)
  }
}
