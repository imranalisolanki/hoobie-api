import { DefaultCrudRepository } from '@loopback/repository';
import { Page, PageRelations } from '../models';
import { DbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class PageRepository extends DefaultCrudRepository<
  Page,
  typeof Page.prototype.id,
  PageRelations
  > {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Page, dataSource);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
