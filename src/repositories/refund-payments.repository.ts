import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { RefundPayments, RefundPaymentsRelations, Member, Job, Contract } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { MemberRepository } from './member.repository';
import { JobRepository } from './job.repository';
import { ContractRepository } from './contract.repository';

export class RefundPaymentsRepository extends DefaultCrudRepository<RefundPayments, typeof RefundPayments.prototype.id, RefundPaymentsRelations> {
  public readonly toMember: BelongsToAccessor<Member, typeof RefundPayments.prototype.id>;
  public readonly byMember: BelongsToAccessor<Member, typeof RefundPayments.prototype.id>;
  public readonly job: BelongsToAccessor<Job, typeof RefundPayments.prototype.id>;
  public readonly contract: BelongsToAccessor<Contract, typeof RefundPayments.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('MemberRepository') public memberRepositoryGetter: Getter<MemberRepository>,
    @repository.getter('JobRepository') public jobRepositoryGetter: Getter<JobRepository>,
    @repository.getter('ContractRepository') public contractRepositoryGetter: Getter<ContractRepository>,
  ) {
    super(RefundPayments, dataSource);
    this.toMember = this.createBelongsToAccessorFor('toMember', memberRepositoryGetter);
    this.byMember = this.createBelongsToAccessorFor('byMember', memberRepositoryGetter);
    this.job = this.createBelongsToAccessorFor('job', jobRepositoryGetter);
    this.contract = this.createBelongsToAccessorFor('contract', contractRepositoryGetter);

    this.registerInclusionResolver('toMember', this.toMember.inclusionResolver);
    this.registerInclusionResolver('byMember', this.byMember.inclusionResolver);
    this.registerInclusionResolver('job', this.job.inclusionResolver);
    this.registerInclusionResolver('contract', this.contract.inclusionResolver);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
