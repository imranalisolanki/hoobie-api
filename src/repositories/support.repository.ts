import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Support, SupportRelations, Member } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import * as _ from 'lodash';
import { MemberRepository } from './member.repository';

export class SupportRepository extends DefaultCrudRepository<Support, typeof Support.prototype.id, SupportRelations> {
  public readonly member: BelongsToAccessor<Member, typeof Support.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('MemberRepository') public memberRepositoryGetter: Getter<MemberRepository>,
  ) {
    super(Support, dataSource);

    this.member = this.createBelongsToAccessorFor('member', memberRepositoryGetter);

    this.registerInclusionResolver('member', this.member.inclusionResolver);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }

  /* Generate printable ticket Id for reference */
  async create(support: Support): Promise<Support> {
    support.ticketId = _.toUpper(Math.random().toString(36).substring(5));
    return super.create(support);
  }
}
