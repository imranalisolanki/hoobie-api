import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Dispute, DisputeRelations, Member, Job, Proposal, Contract } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import * as _ from 'lodash';
import { MemberRepository } from './member.repository';
import { JobRepository } from './job.repository';
import { ContractRepository } from './contract.repository';

export class DisputeRepository extends DefaultCrudRepository<Dispute, typeof Dispute.prototype.id, DisputeRelations> {
  public readonly client: BelongsToAccessor<Member, typeof Dispute.prototype.id>;
  public readonly resource: BelongsToAccessor<Member, typeof Dispute.prototype.id>;
  public readonly job: BelongsToAccessor<Job, typeof Dispute.prototype.id>;
  public readonly contract: BelongsToAccessor<Contract, typeof Dispute.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('MemberRepository') public clientRepositoryGetter: Getter<MemberRepository>,
    @repository.getter('MemberRepository') public resourceRepositoryGetter: Getter<MemberRepository>,
    @repository.getter('JobRepository') public jobRepositoryGetter: Getter<JobRepository>,
    @repository.getter('ContractRepository') public contractRepositoryGetter: Getter<ContractRepository>,
  ) {
    super(Dispute, dataSource);

    this.client = this.createBelongsToAccessorFor('client', clientRepositoryGetter);
    this.resource = this.createBelongsToAccessorFor('resource', resourceRepositoryGetter);
    this.job = this.createBelongsToAccessorFor('job', jobRepositoryGetter);
    this.contract = this.createBelongsToAccessorFor('contract', contractRepositoryGetter);

    this.registerInclusionResolver('client', this.client.inclusionResolver);
    this.registerInclusionResolver('resource', this.resource.inclusionResolver);
    this.registerInclusionResolver('job', this.job.inclusionResolver);
    this.registerInclusionResolver('contract', this.contract.inclusionResolver);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }

  /* Generate printable dispute Id for reference */
  async create(dispute: Dispute): Promise<Dispute> {
    dispute.disputeId = _.toUpper(Math.random().toString(36).substring(5));
    return super.create(dispute);
  }
}
