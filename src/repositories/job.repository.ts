import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Job, JobRelations, Member, Category } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { MemberRepository } from './member.repository';
import { CategoryRepository } from './category.repository';

export class JobRepository extends DefaultCrudRepository<Job, typeof Job.prototype.id, JobRelations> {
  public readonly member: BelongsToAccessor<Member, typeof Job.prototype.id>;
  public readonly category: BelongsToAccessor<Category, typeof Job.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('MemberRepository') public memberRepositoryGetter: Getter<MemberRepository>,
    @repository.getter('CategoryRepository') public categoryRepositoryGetter: Getter<CategoryRepository>,
  ) {
    super(Job, dataSource);

    this.member = this.createBelongsToAccessorFor('member', memberRepositoryGetter);
    this.category = this.createBelongsToAccessorFor('category', categoryRepositoryGetter);

    this.registerInclusionResolver('member', this.member.inclusionResolver);
    this.registerInclusionResolver('category', this.category.inclusionResolver);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
