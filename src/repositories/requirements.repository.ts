import { DefaultCrudRepository } from '@loopback/repository';
import { Requirements, RequirementsRelations } from '../models';
import { DbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class RequirementsRepository extends DefaultCrudRepository<Requirements, typeof Requirements.prototype.id, RequirementsRelations> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Requirements, dataSource);
  }
}
