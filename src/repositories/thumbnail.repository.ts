import { DefaultCrudRepository } from '@loopback/repository';
import { Thumbnail, ThumbnailRelations } from '../models';
import { DbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class ThumbnailRepository extends DefaultCrudRepository<Thumbnail, typeof Thumbnail.prototype.id, ThumbnailRelations> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Thumbnail, dataSource);
  }
}
