import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Message, MessageRelations, Member } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { MemberRepository } from './member.repository';

export class MessageRepository extends DefaultCrudRepository<Message, typeof Message.prototype.id, MessageRelations> {
  public readonly sender: BelongsToAccessor<Member, typeof Message.prototype.id>;
  public readonly receiver: BelongsToAccessor<Member, typeof Message.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('MemberRepository') public memberRepositoryGetter: Getter<MemberRepository>,
  ) {
    super(Message, dataSource);

    this.sender = this.createBelongsToAccessorFor('sender', memberRepositoryGetter);
    this.receiver = this.createBelongsToAccessorFor('receiver', memberRepositoryGetter);

    this.registerInclusionResolver('sender', this.sender.inclusionResolver);
    this.registerInclusionResolver('receiver', this.receiver.inclusionResolver);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
