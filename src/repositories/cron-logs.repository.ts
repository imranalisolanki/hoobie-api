import { DefaultCrudRepository } from '@loopback/repository';
import { CronLogs, CronLogsRelations } from '../models';
import { DbDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class CronLogsRepository extends DefaultCrudRepository<
  CronLogs,
  typeof CronLogs.prototype.id,
  CronLogsRelations
  > {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(CronLogs, dataSource);
    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
