import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Contract, ContractRelations, Member, Job, Proposal } from '../models';
import { DbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { MemberRepository } from './member.repository';
import { JobRepository } from './job.repository';
import { ProposalRepository } from './proposal.repository';

export class ContractRepository extends DefaultCrudRepository<Contract, typeof Contract.prototype.id, ContractRelations> {
  public readonly client: BelongsToAccessor<Member, typeof Contract.prototype.id>;
  public readonly resource: BelongsToAccessor<Member, typeof Contract.prototype.id>;
  public readonly job: BelongsToAccessor<Job, typeof Contract.prototype.id>;
  public readonly proposal: BelongsToAccessor<Proposal, typeof Contract.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('MemberRepository') public clientRepositoryGetter: Getter<MemberRepository>,
    @repository.getter('MemberRepository') public resourceRepositoryGetter: Getter<MemberRepository>,
    @repository.getter('JobRepository') public jobRepositoryGetter: Getter<JobRepository>,
    @repository.getter('ProposalRepository') public proposalRepositoryGetter: Getter<ProposalRepository>,

  ) {
    super(Contract, dataSource);

    this.client = this.createBelongsToAccessorFor('client', clientRepositoryGetter);
    this.resource = this.createBelongsToAccessorFor('resource', resourceRepositoryGetter);
    this.job = this.createBelongsToAccessorFor('job', jobRepositoryGetter);
    this.proposal = this.createBelongsToAccessorFor('resource', proposalRepositoryGetter);

    this.registerInclusionResolver('client', this.client.inclusionResolver);
    this.registerInclusionResolver('resource', this.resource.inclusionResolver);
    this.registerInclusionResolver('job', this.job.inclusionResolver);
    this.registerInclusionResolver('proposal', this.proposal.inclusionResolver);

    (this.modelClass as any).observe('persist', async (ctx: any) => {
      ctx.data.modified = new Date();
    });
  }
}
