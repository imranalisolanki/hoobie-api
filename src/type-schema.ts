export type Credentials = {
  email: string;
  password: string;
};

export type IMember = {
  id: string;
  fName: string;
  lName: string;
  image: string;
  email: string;
  created: string;
  modified: string;
  status: number;
  password: string;
};

export type IEmail = {
  to: string;
  from: string;
  subject: string;
  html: string;
}

export const UserProfileSchema = {
  type: 'object',
  required: ['id'],
  properties: {
    id: { type: 'string' },
    email: { type: 'string' },
    name: { type: 'string' },
  },
};

const CredentialsSchema = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email',
    },
    password: {
      type: 'string',
      minLength: 6,
    },
  },
};

export const CredentialsRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': { schema: CredentialsSchema },
  },
};
