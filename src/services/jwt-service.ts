import { inject } from '@loopback/context';
import { HttpErrors } from '@loopback/rest';
import { promisify } from 'util';
import { TokenService } from '@loopback/authentication';
import { UserProfile, securityId } from '@loopback/security';
import { TokenServiceBindings } from '../keys';
import { AnyObject } from '@loopback/repository';

const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);
const verifyAsync = promisify(jwt.verify);

export interface TokenService {
  verifyToken(token: string): Promise<UserProfile>;
  generateToken(userProfile: UserProfile): Promise<string>;
  generateResetToken(userProfile: UserProfile): Promise<string>;
}

export class JWTService implements TokenService {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SECRET) private jwtSecret: string,
    @inject(TokenServiceBindings.TOKEN_EXPIRES_IN) private jwtExpiresIn: string
  ) { }

  async verifyToken(token: string): Promise<UserProfile> {
    if (!token) {
      throw new HttpErrors.Unauthorized(`Error verifying token : 'token' is null`);
    }

    let userProfile: UserProfile;

    try {
      const decodedToken = await verifyAsync(token, this.jwtSecret);
      userProfile = Object.assign({ [securityId]: '', name: '', email: '' }, { [securityId]: decodedToken.id, name: decodedToken.name, email: decodedToken.email });
    } catch (err) {
      throw new HttpErrors.Unauthorized(`Error verifying token : ${err.message}`);
    }

    return userProfile;
  }

  async generateToken(userProfile: UserProfile): Promise<string> {
    if (!userProfile) {
      throw new HttpErrors.Unauthorized('Error generating token : userProfile is null');
    }

    // Generate a JSON Web Token
    const userInfoForToken = { id: userProfile[securityId], name: userProfile.name, email: userProfile.email };
    let token: string;
    try {
      token = await signAsync(userInfoForToken, this.jwtSecret, { expiresIn: Number(this.jwtExpiresIn) });
    } catch (err) {
      throw new HttpErrors.Unauthorized(`Error encoding token : ${err}`);
    }

    return token;
  }

  async generateResetToken(userProfile: AnyObject): Promise<string> {
    if (!userProfile) {
      throw new HttpErrors.Unauthorized('Error generating reset token : userProfile is null');
    }

    let token: string, resetExpireIn = 600; // 10 minutes
    try {
      token = await signAsync(userProfile, this.jwtSecret, { expiresIn: Number(resetExpireIn) });
    } catch (err) {
      throw new HttpErrors.Unauthorized(`Error encoding token : ${err}`);
    }

    return token;
  }
}
