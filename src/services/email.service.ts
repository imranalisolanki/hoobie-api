import { bind, BindingScope } from '@loopback/core';
import * as assert from 'assert';
import { repository } from '@loopback/repository';
import { EmailTemplateRepository } from '../repositories/email-template.repository';
import { HttpErrors } from '@loopback/rest';
import * as common from '../services/common';
import * as nodemailer from 'nodemailer';

@bind({ scope: BindingScope.TRANSIENT })
export class EmailService {
  constructor(
    @repository(EmailTemplateRepository) public emailTemplateRepository: EmailTemplateRepository
  ) { }

  async sendMail(data: any): Promise<object> {
    assert(data && data.to, 'Email Id is required!');
    assert(data && data.slug, 'Email Template slug Id is required!');
    assert(data && data.message, 'message body is required!');

    const template = await this.emailTemplateRepository.findOne({
      where: {
        slug: data.slug,
        status: 1
      }
    });

    if (!(template && template.id)) {
      throw new HttpErrors['NotFound'](`Email Template not found.`);
    }

    const siteOption = common.getSiteOptions();

    var mailObj = {
      to: data.to,
      subject: template.subject,
      from: data.from || siteOption.fromMail,
      html: common.filterEmailContent(template.message, data.message)
    }

    let transporter = nodemailer.createTransport(siteOption.email);
    return await transporter.sendMail(mailObj);
  }

}
