import { bind, BindingScope, inject } from '@loopback/core';
import { repository, AnyObject } from '@loopback/repository';
import { MemberRepository, ReviewRepository, NotificationRepository, ProposalRepository, JobRepository } from '../repositories';
import { ObjectID } from 'mongodb';
import * as _ from 'lodash';
import { FCMServiceBindings } from '../keys';
import { FCMService } from './fcm.service';
import { HttpErrors } from '@loopback/rest';

@bind({ scope: BindingScope.TRANSIENT })
export class ControllerService {
  constructor(
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @repository(ReviewRepository) public reviewRepository: ReviewRepository,
    @inject(FCMServiceBindings.FCM_SERVICE) public fcmService: FCMService,
    @repository(NotificationRepository) public notificationRepository: NotificationRepository,
    @repository(ProposalRepository) public proposalRepository: ProposalRepository,
    @repository(JobRepository) public jobRepository: JobRepository
  ) { }

  async refreshAverageRating(data: AnyObject): Promise<any> {
    if (data && data.memberId && data.memberType) {

      const member = await this.memberRepository.findOne({
        where: {
          id: data.memberId,
          status: 1
        }
      });

      if (member && member.id) {
        if (!this.reviewRepository.dataSource.connected) {
          await this.reviewRepository.dataSource.connect();
        }
        const reviewCollection = (this.reviewRepository.dataSource.connector as any).collection('Review');

        await Promise.all([
          this.memberRepository.findOne({
            where: {
              id: data.memberId
            }, fields: { id: true, name: true, fcmToken: true, jobSeeker: true, jobManager: true }
          }),
          reviewCollection.aggregate([
            {
              $match: {
                toMemberId: new ObjectID(String(data.memberId)),
                memberType: data.memberType // jobSeeker or jobManager
              }
            },
            {
              $group: {
                _id: '$toMemberId',
                avgRating: {
                  $avg: "$rating"
                }
              }
            }
          ]).get()
        ]).then(async (res: any) => {
          const member = res && res[0] || {}
          let ratingObj: any = _.assign({});
          if (data.memberType === 'jobSeeker') {
            ratingObj.rate = member && member.jobSeeker && member.jobSeeker.rating || 0;
          } else {
            ratingObj.rate = member && member.jobManager && member.jobManager.rating || 0;
          }
          const rating = res && res[1] && res[1][0] && res[1][0].avgRating || 0;
          // console.log(rating, 'rating rating rating')
          // console.log(ratingObj.rate, 'ratingObj.rate ratingObj.rate')
          let memberObj: any = _.assign({}, member);
          if (data.memberType === 'jobSeeker') {
            memberObj.jobSeeker.rating = rating;
          } else {
            memberObj.jobManager.rating = rating;
          }
          if (member && member.id) {
            var message = {
              // to: member.fcmToken,
              data: {
                name: member.name,
                memberId: member.id,
                type: "rankingDetails",
                userType: data && data.memberType || "",
                notificationObject: { memberId: member.id }
              },
              notification: {
                title: 'Hoobie',
                body: (ratingObj && ratingObj.rate <= rating) ? "Congratulations your ranking has increased!" : "Your Ranking has been reduced, check out ways to increase it!",
                priority: "high",
                sound: "default",
                vibrate: true,
              }
            };
            await this.fcmService.sendNotification({ message: message })
            let notification = {
              title: (ratingObj && ratingObj.rate <= rating) ? "Ranking has increased!" : "Your Ranking has been reduced!",
              message: (ratingObj && ratingObj.rate <= rating) ? "Congratulations your ranking has increased!" : "Your Ranking has been reduced, check out ways to increase it!",
              memberId: member.id,
              type: "rankingDetails",
              userType: data && data.memberType || "",
              notificationObject: { memberId: member.id }
            }
            await this.notificationRepository.create(notification)
          } else {
            throw new HttpErrors.NotFound("fcm token not found")
          }
          await this.memberRepository.updateById(data.memberId, memberObj);
        }).catch((err: any) => {
          console.debug(err);
        });
      }
    }
  }

  async rejectMembersProposalNotification(data: AnyObject): Promise<any> {
    // console.log(data, '-------****----------')
    if (data && data.jobId) {
      const proposalData = await this.proposalRepository.find({
        where: {
          jobId: data.jobId
        }
      })
      if (proposalData && proposalData.length) {
        const jobs = await this.jobRepository.findOne({
          where: {
            id: data.jobId
          }
        })
        let memberIds: Array<any> = _.map(proposalData, v => v.memberId)
        // console.log(memberIds, '--------')
        let proposalId: Array<any> = _.map(proposalData, v => v.id)
        const membersId = _.filter(memberIds, function (val: any) {
          return val != String(data.memberId)
        })
        // console.log(membersId, '----****----')
        const members = await this.memberRepository.find({
          where: {
            id: { inq: membersId }
          }, fields: { id: true, name: true, fcmToken: true, jobSeeker: true, jobManager: true }
        })
        if (members && members.length) {
          let _this = this
          _.forEach(members, async function (val: any, index) {
            // console.log(index, "index")
            if (val && val.id) {
              // console.log(index)
              var message = {
                // to: val.fcmToken,
                data: {
                  name: val.name,
                  memberId: val.id,
                  jobId: data.jobId,
                  type: 'ProposalDeclined',
                  userType: (val && val.jobSeeker && val.jobSeeker.title) ? "jobSeeker" : "jobManager",
                  notificationObject: { proposalId: proposalId && proposalId[index] || "" }
                },
                notification: {
                  title: 'Hoobie',
                  body: 'Your proposal for  ' + (jobs && jobs.title || '') + " unsuccessful",
                  priority: "high",
                  sound: "default",
                  vibrate: true,
                }
              };
              await _this.fcmService.sendNotification({ message: message })
              let notification = {
                title: "Your proposal has been unsuccessful ",
                message: 'Your proposal for  ' + (jobs && jobs.title || '') + " unsuccessful",
                memberId: val.id,
                type: "ProposalDeclined",
                userType: (val && val.jobSeeker && val.jobSeeker.title) ? "jobSeeker" : "jobManager",
                notificationObject: { proposalId: proposalId && proposalId[index] || "" }
              }
              await _this.notificationRepository.create(notification)
            }
          })
        }
      }
    }
  }

}
