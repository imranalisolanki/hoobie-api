import { HttpErrors } from '@loopback/rest';
import { Member } from '../models/member.model';
import { UserService } from '@loopback/authentication';
import { UserProfile, securityId } from '@loopback/security';
import { repository } from '@loopback/repository';
import { PasswordHasher } from './hash.password.bcryptjs';
import { PasswordHasherBindings } from '../keys';
import { inject } from '@loopback/context';
import { Credentials } from '../type-schema';
import { MemberRepository } from '../repositories';
import * as _ from 'lodash';

export class MemberService implements UserService<Member, Credentials> {
  constructor(
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER) public passwordHasher: PasswordHasher,
  ) { }

  async verifyCredentials(credentials: Credentials): Promise<Member> {
    const invalidCredentialsError = 'Invalid email or password.';

    const getMember = await this.memberRepository.findOne({ where: { email: credentials.email }, });

    if (!getMember) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }

    const passwordMatched = await this.passwordHasher.comparePassword(credentials.password, getMember.password || '');

    if (!passwordMatched) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }

    return getMember;
  }

  convertToUserProfile(user: Member): UserProfile {
    return {
      [securityId]: user.id || '',
      name: user.name || '',
      email: user.email || '',
    };
  }
}
