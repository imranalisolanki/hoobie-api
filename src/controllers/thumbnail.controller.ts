import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Thumbnail} from '../models';
import {ThumbnailRepository} from '../repositories';

export class ThumbnailController {
  constructor(
    @repository(ThumbnailRepository)
    public thumbnailRepository : ThumbnailRepository,
  ) {}

  @post('/thumbnails', {
    responses: {
      '200': {
        description: 'Thumbnail model instance',
        content: {'application/json': {schema: getModelSchemaRef(Thumbnail)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Thumbnail, {
            title: 'NewThumbnail',
            exclude: ['id'],
          }),
        },
      },
    })
    thumbnail: Omit<Thumbnail, 'id'>,
  ): Promise<Thumbnail> {
    return this.thumbnailRepository.create(thumbnail);
  }

  @get('/thumbnails/count', {
    responses: {
      '200': {
        description: 'Thumbnail model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Thumbnail)) where?: Where<Thumbnail>,
  ): Promise<Count> {
    return this.thumbnailRepository.count(where);
  }

  @get('/thumbnails', {
    responses: {
      '200': {
        description: 'Array of Thumbnail model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Thumbnail, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Thumbnail)) filter?: Filter<Thumbnail>,
  ): Promise<Thumbnail[]> {
    return this.thumbnailRepository.find(filter);
  }

  @patch('/thumbnails', {
    responses: {
      '200': {
        description: 'Thumbnail PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Thumbnail, {partial: true}),
        },
      },
    })
    thumbnail: Thumbnail,
    @param.query.object('where', getWhereSchemaFor(Thumbnail)) where?: Where<Thumbnail>,
  ): Promise<Count> {
    return this.thumbnailRepository.updateAll(thumbnail, where);
  }

  @get('/thumbnails/{id}', {
    responses: {
      '200': {
        description: 'Thumbnail model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Thumbnail, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Thumbnail)) filter?: Filter<Thumbnail>
  ): Promise<Thumbnail> {
    return this.thumbnailRepository.findById(id, filter);
  }

  @patch('/thumbnails/{id}', {
    responses: {
      '204': {
        description: 'Thumbnail PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Thumbnail, {partial: true}),
        },
      },
    })
    thumbnail: Thumbnail,
  ): Promise<void> {
    await this.thumbnailRepository.updateById(id, thumbnail);
  }

  @put('/thumbnails/{id}', {
    responses: {
      '204': {
        description: 'Thumbnail PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() thumbnail: Thumbnail,
  ): Promise<void> {
    await this.thumbnailRepository.replaceById(id, thumbnail);
  }

  @del('/thumbnails/{id}', {
    responses: {
      '204': {
        description: 'Thumbnail DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.thumbnailRepository.deleteById(id);
  }
}
