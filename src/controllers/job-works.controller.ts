import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {JobWorks} from '../models';
import {JobWorksRepository} from '../repositories';

export class JobWorksController {
  constructor(
    @repository(JobWorksRepository)
    public jobWorksRepository : JobWorksRepository,
  ) {}

  @post('/job-works', {
    responses: {
      '200': {
        description: 'JobWorks model instance',
        content: {'application/json': {schema: getModelSchemaRef(JobWorks)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(JobWorks, {exclude: ['id']}),
        },
      },
    })
    jobWorks: Omit<JobWorks, 'id'>,
  ): Promise<JobWorks> {
    return this.jobWorksRepository.create(jobWorks);
  }

  @get('/job-works/count', {
    responses: {
      '200': {
        description: 'JobWorks model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(JobWorks)) where?: Where<JobWorks>,
  ): Promise<Count> {
    return this.jobWorksRepository.count(where);
  }

  @get('/job-works', {
    responses: {
      '200': {
        description: 'Array of JobWorks model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(JobWorks)},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(JobWorks)) filter?: Filter<JobWorks>,
  ): Promise<JobWorks[]> {
    return this.jobWorksRepository.find(filter);
  }

  @patch('/job-works', {
    responses: {
      '200': {
        description: 'JobWorks PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(JobWorks, {partial: true}),
        },
      },
    })
    jobWorks: JobWorks,
    @param.query.object('where', getWhereSchemaFor(JobWorks)) where?: Where<JobWorks>,
  ): Promise<Count> {
    return this.jobWorksRepository.updateAll(jobWorks, where);
  }

  @get('/job-works/{id}', {
    responses: {
      '200': {
        description: 'JobWorks model instance',
        content: {'application/json': {schema: getModelSchemaRef(JobWorks)}},
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<JobWorks> {
    return this.jobWorksRepository.findById(id);
  }

  @patch('/job-works/{id}', {
    responses: {
      '204': {
        description: 'JobWorks PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(JobWorks, {partial: true}),
        },
      },
    })
    jobWorks: JobWorks,
  ): Promise<void> {
    await this.jobWorksRepository.updateById(id, jobWorks);
  }

  @put('/job-works/{id}', {
    responses: {
      '204': {
        description: 'JobWorks PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() jobWorks: JobWorks,
  ): Promise<void> {
    await this.jobWorksRepository.replaceById(id, jobWorks);
  }

  @del('/job-works/{id}', {
    responses: {
      '204': {
        description: 'JobWorks DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.jobWorksRepository.deleteById(id);
  }
}
