import { Count, CountSchema, Filter, repository, Where, AnyObject } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Notification } from '../models';
import { NotificationRepository, MemberRepository } from '../repositories';
import { inject } from '@loopback/core';
import { FCMServiceBindings } from '../keys';
import { FCMService } from '../services';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate } from '@loopback/authentication';
import { SecurityBindings, UserProfile, securityId } from '@loopback/security';
import * as _ from 'lodash';
export class NotificationController {
  constructor(
    @repository(NotificationRepository) public notificationRepository: NotificationRepository,
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @inject(FCMServiceBindings.FCM_SERVICE) public fcmService: FCMService
  ) { }

  @post('/notifications', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Notification model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Notification) } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Notification, { exclude: ['id'] }),
        },
      },
    })
    notification: Omit<Notification, 'id'>,
  ): Promise<any> {

    const notificationDetails = await this.notificationRepository.create(notification);
    if (notification && notification.memberId) {
      return await Promise.all([
        this.memberRepository.findOne({
          where: {
            id: currentUser[securityId]
          }, fields: { id: true, name: true }
        }),
        this.memberRepository.findOne({
          where: {
            id: notificationDetails.memberId
          }, fields: { id: true, name: true, fcmToken: true }
        })
      ]).then(async (res: any) => {
        let loginMember = res && res[0] || {}
        let notiMember = res && res[1] || {}
        if (notiMember && notiMember.fcmToken) {
          var message = {
            to: notiMember.fcmToken,
            data: {
              name: loginMember.name,
              memberId: loginMember.id
            },
            notification: {
              title: notificationDetails.title,
              body: notificationDetails.message,
              priority: "high",
              sound: "default",
              vibrate: true,
            }
          };
          await this.fcmService.sendNotification({ message: message })
          return notificationDetails;
        } else {
          throw new HttpErrors.NotFound("FcmToken not found")
        }
      })
    } else {
      throw new HttpErrors.NotFound("Member not found")
    }
  }

  @get('/notifications/count', {
    responses: {
      '200': {
        description: 'Notification model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Notification)) where?: Where<Notification>,
  ): Promise<Count> {
    return this.notificationRepository.count(where);
  }

  @get('/notifications', {
    responses: {
      '200': {
        description: 'Array of Notification model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Notification) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Notification)) filter?: Filter<Notification>,
  ): Promise<Notification[]> {
    return this.notificationRepository.find(filter);
  }

  @patch('/notifications', {
    responses: {
      '200': {
        description: 'Notification PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Notification, { partial: true }),
        },
      },
    })
    notification: Notification,
    @param.query.object('where', getWhereSchemaFor(Notification)) where?: Where<Notification>,
  ): Promise<Count> {
    return this.notificationRepository.updateAll(notification, where);
  }

  @get('/notifications/{id}', {
    responses: {
      '200': {
        description: 'Notification model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Notification) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Notification> {
    return this.notificationRepository.findById(id);
  }

  @patch('/notifications/{id}', {
    responses: {
      '204': {
        description: 'Notification PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Notification, { partial: true }),
        },
      },
    })
    notification: Notification,
  ): Promise<void> {
    await this.notificationRepository.updateById(id, notification);
  }

  @put('/notifications/{id}', {
    responses: {
      '204': {
        description: 'Notification PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() notification: Notification,
  ): Promise<void> {
    await this.notificationRepository.replaceById(id, notification);
  }

  @del('/notifications/{id}', {
    responses: {
      '204': {
        description: 'Notification DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.notificationRepository.deleteById(id);
  }

  @post('/notifications/getNotificationList', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Notification model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Notification) } },
      },
    },
  })
  @authenticate('jwt')
  async getNotificationList(
    @requestBody() data: AnyObject
  ): Promise<any> {
    let resNotification: Array<AnyObject> = [];
    const notification = await this.notificationRepository.find({
      order: [data.order]
    });

    if (notification && notification.length) {

      let memberIds = _.map(notification, v => v.memberId)
      return await Promise.all([
        this.memberRepository.find({
          where: {
            id: { inq: memberIds }
          }, fields: { id: true, name: true }
        })
      ]).then((res: any) => {

        let members = res && res[0] && res[0].length && _.groupBy(res[0], v => v.id)

        _.forEach(notification, function (val: any) {
          let obj = Object.assign({}, val)
          obj.member = members && members[val.memberId] && members[val.memberId][0] || {};
          resNotification.push(obj)
        })
        let start = data && data.skip || 0

        if (data && data.sorting === 'name') {
          resNotification = _.orderBy(resNotification, function (e) { return _.lowerCase(e.member.name) }, [data.sortBy]);
          resNotification = _.slice(resNotification, start, start + data.limit)
          return resNotification;
        } else {
          resNotification = _.slice(resNotification, start, start + data.limit)
          return resNotification;
        }
      })
    } else {
      return []
    }
  }

  @post('/notifications/sendPushNotification', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Notification model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Notification) } },
      },
    },
  })
  @authenticate('jwt')
  async sendPushNotification(
    @requestBody() data: AnyObject
  ): Promise<any> {
    let query: AnyObject = {}
    //  console.log(data)
    if (data && data.memberId && data.memberId.length) {
      query.id = { inq: data.memberId }
    } else {
      if (data && data.categoryIds && data.categoryIds.length) {
        query.interestIds = { in: data.categoryIds }
      } else {
        // query.interestIds = { in: data.categoryIds }
      }
    }
    const membersDetails = await this.memberRepository.find({
      where: query,
      fields: { id: true, name: true, fcmToken: true }
    });
    //return false
    if (membersDetails && membersDetails.length) {
      let _this = this;
      _.forEach(membersDetails, async function (val: any) {
        if (val && val.fcmToken) {
          var message = {
            to: val.fcmToken,
            data: {
              name: val.name,
              memberId: val.id
            },
            notification: {
              title: 'Breeze send Notification',
              body: data.message,
              priority: "high",
              sound: "default",
              vibrate: true,
            }
          };
          const noti = await _this.fcmService.sendNotification({ message: message })
          return noti;
        } else {
          return [];
        }
      })

    } else {
      throw new HttpErrors.NotFound("Members not found");
    }
  }
}
