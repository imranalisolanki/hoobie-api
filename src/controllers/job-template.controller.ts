import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { JobTemplate } from '../models';
import { JobTemplateRepository } from '../repositories';

export class JobTemplateController {
  constructor(
    @repository(JobTemplateRepository)
    public jobTemplateRepository: JobTemplateRepository,
  ) { }

  @post('/job-templates', {
    responses: {
      '200': {
        description: 'JobTemplate model instance',
        content: { 'application/json': { schema: getModelSchemaRef(JobTemplate) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(JobTemplate, { exclude: ['id'] }),
        },
      },
    })
    jobTemplate: Omit<JobTemplate, 'id'>,
  ): Promise<JobTemplate> {
    return this.jobTemplateRepository.create(jobTemplate);
  }

  @get('/job-templates/count', {
    responses: {
      '200': {
        description: 'JobTemplate model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(JobTemplate)) where?: Where<JobTemplate>,
  ): Promise<Count> {
    return this.jobTemplateRepository.count(where);
  }

  @get('/job-templates', {
    responses: {
      '200': {
        description: 'Array of JobTemplate model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(JobTemplate) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(JobTemplate)) filter?: Filter<JobTemplate>,
  ): Promise<JobTemplate[]> {
    return this.jobTemplateRepository.find(filter);
  }

  @patch('/job-templates', {
    responses: {
      '200': {
        description: 'JobTemplate PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(JobTemplate, { partial: true }),
        },
      },
    })
    jobTemplate: JobTemplate,
    @param.query.object('where', getWhereSchemaFor(JobTemplate)) where?: Where<JobTemplate>,
  ): Promise<Count> {
    return this.jobTemplateRepository.updateAll(jobTemplate, where);
  }

  @get('/job-templates/{id}', {
    responses: {
      '200': {
        description: 'JobTemplate model instance',
        content: { 'application/json': { schema: getModelSchemaRef(JobTemplate) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<JobTemplate> {
    return this.jobTemplateRepository.findById(id);
  }

  @patch('/job-templates/{id}', {
    responses: {
      '204': {
        description: 'JobTemplate PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(JobTemplate, { partial: true }),
        },
      },
    })
    jobTemplate: JobTemplate,
  ): Promise<void> {
    await this.jobTemplateRepository.updateById(id, jobTemplate);
  }

  @put('/job-templates/{id}', {
    responses: {
      '204': {
        description: 'JobTemplate PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() jobTemplate: JobTemplate,
  ): Promise<void> {
    await this.jobTemplateRepository.replaceById(id, jobTemplate);
  }

  @del('/job-templates/{id}', {
    responses: {
      '204': {
        description: 'JobTemplate DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.jobTemplateRepository.deleteById(id);
  }

  @post('/job-templates/multipleDelete', {
    responses: {
      '200': {
        description: 'Array of JobTemplate model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(JobTemplate) },
          },
        },
      },
    },
  })
  async multipleDelete(
    @requestBody() data: any)
    : Promise<any> {

    return this.jobTemplateRepository.deleteAll({ id: { inq: data.id } });
  }
}
