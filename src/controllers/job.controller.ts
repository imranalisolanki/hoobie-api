import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Job, Member, Contract } from '../models';
import { JobRepository, MemberRepository, ProposalRepository, ReviewRepository, ContractRepository, CategoryRepository, PaymentRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';
import { UserProfile, securityId, SecurityBindings } from '@loopback/security';
import { inject } from '@loopback/core';
import * as _ from 'lodash';
import { ObjectID } from 'mongodb';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';

export class JobController {
  constructor(
    @repository(JobRepository) public jobRepository: JobRepository,
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @repository(ProposalRepository) public proposalRepository: ProposalRepository,
    @repository(ReviewRepository) public reviewRepository: ReviewRepository,
    @repository(ContractRepository) public contractRepository: ContractRepository,
    @repository(CategoryRepository) public categoryRepository: CategoryRepository,
    @repository(PaymentRepository) public paymentRepository: PaymentRepository
  ) { }

  @post('/jobs', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Job model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Job) } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Job, { exclude: ['id'] }),
        },
      },
    })
    job: Omit<Job, 'id'>,
  ): Promise<Job> {
    return this.jobRepository.create(job);
  }

  @get('/jobs/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Job model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  @authenticate('jwt')
  async count(
    @param.query.object('where', getWhereSchemaFor(Job)) where?: Where<Job>,
  ): Promise<Count> {
    return this.jobRepository.count(where);
  }

  @get('/jobs', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Job model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Job) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async find(
    @param.query.object('filter', getFilterSchemaFor(Job)) filter?: Filter<Job>,
  ): Promise<any> {
    let resjobs: Array<AnyObject> = [];
    const jobs = await this.jobRepository.find(filter);
    if (jobs && jobs.length) {
      let jobIds: any[] = _.map(jobs, v => v.id);
      const proposalData = await this.proposalRepository.find({
        where: {
          jobId: { inq: jobIds }
        }
      });
      const proposal = proposalData && proposalData.length && _.groupBy(proposalData, v => v.jobId);

      _.forEach(jobs, function (val: any) {
        const obj = Object.assign({}, val);
        obj.bids = proposal && proposal[val.id] && proposal[val.id].length || 0;
        resjobs.push(obj);
      })
      return resjobs;
    }
  }


  @post('/jobs/getClosedJobs', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Job model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Job) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getClosedJobs(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    let resjobs: Array<AnyObject> = [];
    const jobs = await this.jobRepository.find({
      where: {
        memberId: data.memberId,
        state: "close"
      },
      limit: data.limit,
      skip: data.skip,
      order: ['modified DESC']
    });
    if (jobs && jobs.length) {
      let jobsId: any[] = _.map(jobs, function (v) { return v.id });

      return await Promise.all([
        this.reviewRepository.find({
          where: {
            jobId: { inq: jobsId },
            toMemberId: data.memberId,
            memberType: data.memberType
          },
          include: [
            { relation: "byMember" }
          ]
        }),
        this.contractRepository.find({
          where: {
            jobId: { inq: jobsId }
          }
        }),
        this.proposalRepository.find({
          where: {
            jobId: { inq: jobsId }
          }
        })
      ]).then(async (res: any) => {

        let review = res && res[0] || [];
        let contract = res && res[1] || [];
        let proposal = res && res[2] || [];
        const memberIds: Array<any> = _.map(review, v => String(v.byMemberId));

        const rsRating = await this.reviewRepository.find({
          where: {
            toMemberId: { inq: memberIds }
          },
          include: [
            { relation: "byMember" }
          ],
          fields: { id: true, memberType: true, rating: true, byMemberId: true, toMemberId: true, description: true, jobId: true, created: true, clarity: true, competence: true, character: true, commitment: true, connection: true, contribution: true, compassion: true, description2: true, description3: true }
        });

        const reviewData = review && review.length && _.groupBy(review, v => v.jobId);
        const rating = rsRating && rsRating.length && _.groupBy(rsRating, v => v.jobId);
        const contractData = contract && contract.length && _.groupBy(contract, v => v.jobId);
        const proposalData = proposal && proposal.length && _.groupBy(proposal, v => v.jobId);

        _.forEach(jobs, function (val: any) {
          let obj: AnyObject = Object.assign({}, val);
          obj.review = reviewData && reviewData[val.id] && reviewData[val.id][0] || {};
          obj.rsreview = rating && rating[val.id] && rating[val.id][0] || {};
          obj.contracts = contractData && contractData[val.id] && contractData[val.id][0] || {};
          obj.bids = proposalData && proposalData[val.id] && proposalData[val.id].length || 0;
          resjobs.push(obj)
        })
        return resjobs;
      })
    } else {
      //throw new HttpErrors.NotFound("Jobs not found")
      return []
    }
  }



  @get('/jobs/{id}', {
    responses: {
      '200': {
        description: 'Job model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<AnyObject> {
    const job = await this.jobRepository.findById(id);

    if (job && job.id) {
      let resJob: AnyObject = Object.assign({}, job, { bids: 0 });

      return await Promise.all([
        this.proposalRepository.find({
          where: {
            jobId: job.id
          }
        }).then(res => res),
        this.memberRepository.findOne({
          where: {
            id: job.memberId
          },
          fields: { id: true, name: true, image: true, jobManager: true }
        }).then(res => res),
        this.categoryRepository.findOne({
          where: {
            id: job.categoryId
          },
          fields: { id: true, name: true, icon: true }
        }).then(res => res),
        this.contractRepository.findOne({
          where: {
            jobId: job.id
          }
        }).then(res => res)
      ]).then((res: any) => {
        resJob.bids = res && res[0] && res[0].length || 0;
        resJob.postedBy = res && res[1] && res[1].id && Object.assign({}, res[1]) || {};
        resJob.category = res && res[2] && res[2].id && Object.assign({}, res[2]) || {};
        resJob.contracts = res && res[3] && res[3].id && Object.assign({}, res[3]) || {};

        return resJob;
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      throw new HttpErrors.NotFound('No job details found for given id.')
    }
  }

  @patch('/jobs/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Job PATCH success',
      },
    },
  })
  @authenticate('jwt')
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Job, { partial: true }),
        },
      },
    })
    job: Job,
  ): Promise<void> {
    await this.jobRepository.updateById(id, job);
  }

  @del('/jobs/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Job DELETE success',
      },
    },
  })
  @authenticate('jwt')
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.jobRepository.deleteById(id);
  }

  @post('/jobs/search', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Job model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async searchJobs(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    if (!this.jobRepository.dataSource.connected) {
      await this.jobRepository.dataSource.connect();
    }
    const jobCollection = (this.jobRepository.dataSource.connector as any).collection('Job');

    // const member = await this.memberRepository.findById(currentUser[securityId]);
    let query: AnyObject = {
      memberId: { neq: new ObjectID(currentUser[securityId]) },
      state: 'active'
    };
    let result: Array<AnyObject> = [];

    if (data && data.text) {
      query.or = [
        { title: new RegExp('.*' + data.text + '.*', 'i') },
        { tag: { inq: [new RegExp('.*' + data.text + '.*', 'i')] } },
        { otherCategory: new RegExp('.*' + data.text + '.*', 'i') }
      ]
    }

    if (data && data.category) {
      query.categoryId = data.category;
    }
    if (data.location) {
      query = {
        "$geoNear": {
          "near": { type: "Point", coordinates: [data.location && data.location.lng, data.location.lat] },
          "limit": data.limit,
          "maxDistance": data.range * 1000,
          "distanceField": "dist.calculated",
          "includeLocs": "dist.matchLocation",
          "distanceMultiplier": 1 / 1000,
          "spherical": true
        }
      }
    }

    let jobs: Array<AnyObject> = []
    if (data.location) {
      jobs = await jobCollection.aggregate([
        query
      ]).get()
    }
    //console.log(jobs, '---------')
    jobs = _.filter(jobs, function (val: any) {
      return val.memberId != currentUser[securityId]
    })
    //console.log(jobs, '*********')
    let jobIds = jobs && jobs.length && _.map(jobs, v => v._id)
    if (jobIds && jobIds.length) {
      query = {
        id: { inq: jobIds }
      }
    }

    jobs = await this.jobRepository.find({
      where: query,
      include: [
        { relation: "member" }
      ],
      skip: data.skip,
      limit: data.limit,
      order: ['id DESC']
    });
    if (jobs && jobs.length) {
      const jobIds: Array<any> = _.map(jobs, v => new ObjectID(v.id)!);
      const jobMemberIds: Array<any> = _.map(jobs, v => new ObjectID(v.memberId)!);

      if (!this.reviewRepository.dataSource.connected) {
        await this.reviewRepository.dataSource.connect();
      }
      const reviewCollection = (this.reviewRepository.dataSource.connector as any).collection('Review');

      return await Promise.all([
        this.proposalRepository.find({
          where: {
            jobId: { inq: jobIds }
          }
        }).then(res => res),
        reviewCollection.aggregate([
          {
            $match: {
              toMemberId: { $in: jobMemberIds },
              memberType: 'jobManager'
            }
          },
          {
            $group: {
              _id: '$jobId',
              avgRating: {
                $avg: "$rating"
              }
            }
          }
        ]).get()
      ]).then(async (res: any) => {
        const proposalGroup = res && res[0] && _.groupBy(res[0], function (v) { return v.jobId; })
        const ratingGroup = res && res[1] && _.groupBy(res[1], function (v) { return String(v._id); })

        _.forEach(jobs, function (val: any) {
          let obj: AnyObject = Object.assign({}, val);
          obj.bids = proposalGroup && proposalGroup[val.id] && proposalGroup[val.id].length || 0;
          obj.rating = ratingGroup && ratingGroup[val.id] && ratingGroup[val.id][0] && ratingGroup[val.id][0].avgRating || 0;
          result.push(obj);
        });

        return result;
      }).catch((err: any) => {
        console.debug(err);
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

  @get('/jobs/history/{memberId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Job model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  @authenticate('jwt')
  async jobsHistory(@param.path.string('memberId') id: string): Promise<AnyObject> {
    let resJob: Array<AnyObject> = [];

    const contracts = await this.contractRepository.find({
      where: {
        clientId: id,
        state: 'complete'
      },
      order: ['created DESC'],
      limit: 5,
      fields: { id: true, jobId: true, resourceId: true, amount: true, clientId: true, ended: true }
    })
    //memberType: 'jobManager'
    if (contracts && contracts.length) {
      const jobIds: Array<any> = _.map(contracts, v => v.jobId);
      const clientId: Array<any> = _.map(contracts, v => v.resourceId);
      return await Promise.all([
        this.jobRepository.find({
          where: {
            id: { inq: jobIds }
          },
          /*  fields: { id: true, title: true, budget: true, description: true, memberId: true, created: true } */
        }),
        this.reviewRepository.find({
          where: {
            jobId: { inq: jobIds },
            toMemberId: id
          },
          fields: { id: true, memberType: true, rating: true, byMemberId: true, toMemberId: true, description: true, jobId: true, created: true, clarity: true, competence: true, character: true, commitment: true, connection: true, contribution: true, compassion: true, description2: true, description3: true }
        }),
        this.memberRepository.find({
          where: {
            id: { inq: clientId }
          }, fields: { id: true, name: true, image: true }
        })
      ]).then(async (res: any) => {
        const job = res && res[0] || [];
        const reviews = res && res[1] || [];
        const clients = res && res[2] || [];
        const memberIds: Array<any> = _.map(reviews, v => String(v.byMemberId));

        const rsRating = await this.reviewRepository.find({
          where: {
            toMemberId: { inq: memberIds }
          },
          fields: { id: true, memberType: true, rating: true, byMemberId: true, toMemberId: true, description: true, jobId: true, created: true, clarity: true, competence: true, character: true, commitment: true, connection: true, contribution: true, compassion: true, description2: true, description3: true }
        });

        const tomemberIds: Array<any> = _.map(reviews, v => String(v.toMemberId));
        var memIds = _.union(memberIds, tomemberIds);
        const members = await this.memberRepository.find({
          where: {
            id: { inq: memIds }
          },
          fields: { id: true, name: true, image: true }
        });

        const memberGroup = members && members.length && _.groupBy(members, function (v) { return String(v.id); }) || {};
        const ratingGroup = reviews && reviews.length && _.groupBy(reviews, function (v) { return String(v.jobId); }) || {};
        const rsRatingGroup = rsRating && rsRating.length && _.groupBy(rsRating, function (v) { return String(v.jobId); }) || {};
        const clientGroup = clients && clients.length && _.groupBy(clients, function (v) { return String(v.id); }) || {};
        const jobs = job && job.length && _.groupBy(job, function (v) { return String(v.id); }) || {};

        _.forEach(contracts, (val: any) => {
          let obj: AnyObject = Object.assign({}, val);
          obj.job = jobs && jobs[val.jobId!] && Object.assign({}, jobs[val.jobId!][0]) || {};
          if (obj.job && obj.job.id) {
            obj.job.resourceMember = clientGroup && clientGroup[val.resourceId!] && Object.assign({}, clientGroup[val.resourceId!][0]) || {};
            obj.job.rating = ratingGroup && ratingGroup[val.jobId!] && Object.assign({}, ratingGroup[val.jobId!][0]) || {};
            obj.job.rsRating = rsRatingGroup && rsRatingGroup[val.jobId!] && Object.assign({}, rsRatingGroup[val.jobId!][0]) || {};
            if (obj.job.rating && obj.job.rating.id && obj.job.rating.byMemberId) {
              obj.job.rating.memberDetails = memberGroup && memberGroup[obj.job.rating.byMemberId] && memberGroup[obj.job.rating.byMemberId][0] || {};
            }
            if (obj.job.rsRating && obj.job.rsRating.id && obj.job.rsRating.byMemberId) {
              obj.job.rsRating.memberDetails = memberGroup && memberGroup[obj.job.rsRating.byMemberId] && memberGroup[obj.job.rsRating.byMemberId][0] || {};
            }
          }
          resJob.push(obj);
        });

        return resJob;
      }).catch((err: any) => {
        console.debug(err);
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else return [];
  }

  @get('/jobs/work-history/{memberId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Job model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  @authenticate('jwt')
  async workHistory(@param.path.string('memberId') id: string): Promise<AnyObject> {
    let resContract: Array<AnyObject> = [];

    let contracts: Contract[] = await this.contractRepository.find({
      where: {
        resourceId: id,
        state: 'complete'
      },
      limit: 5,
      order: ['modified DESC'],
      fields: { id: true, amount: true, state: true, time: true, jobId: true, created: true, modified: true, ended: true }
    });

    if (contracts && contracts.length) {
      const jobIds: Array<any> = _.map(contracts, v => v.jobId);
      //memberType: 'jobSeeker'
      return await Promise.all([
        this.jobRepository.find({
          where: {
            id: { inq: jobIds }
          },
          fields: { id: true, title: true, description: true, created: true, jobType: true }
        }).then(res => res),
        this.reviewRepository.find({
          where: {
            jobId: { inq: jobIds },
            toMemberId: id
          },
          fields: { id: true, memberType: true, rating: true, byMemberId: true, toMemberId: true, description: true, jobId: true, created: true, clarity: true, competence: true, character: true, commitment: true, connection: true, contribution: true, compassion: true, description2: true, description3: true }
        }).then(res => res),
        this.paymentRepository.find({
          where: {
            state: { inq: ["success", "refunded"] },
            memberId: id
          }
        }).then(res => res),
      ]).then(async (res: any) => {

        const memberIds: Array<any> = _.map(res[1], v => String(v.byMemberId));
        const rsRating = await this.reviewRepository.find({
          where: {
            toMemberId: { inq: memberIds }
          }, fields: { id: true, memberType: true, rating: true, byMemberId: true, toMemberId: true, description: true, jobId: true, created: true, clarity: true, competence: true, character: true, commitment: true, connection: true, contribution: true, compassion: true, description2: true, description3: true }
        });

        const tomemberIds: Array<any> = _.map(res[1], v => String(v.toMemberId));
        var memIds = _.union(memberIds, tomemberIds);
        const members = await this.memberRepository.find({
          where: {
            id: { inq: memIds }
          },
          fields: { id: true, name: true, image: true }
        });
        // let refundPayment: Array<any> = []
        // _.forEach(res[2], function (val: any) {
        //   if (val && val.state && val.state === 'refunded') {
        //     refundPayment.push(val)
        //   }
        // })
        let refundPayment = _.filter(res[2], function (val: any) {
          return val.state === 'refunded'
        })
        let refund = _.sumBy(refundPayment, v => v.amount);
        let earnPayment = _.sumBy(res[2], function (v: any) { return v.amount })
        let totalEarnAmount = (earnPayment - refund)

        const jobGroup = res && res[0] && res[0].length && _.groupBy(res[0], function (v) { return v.id; });
        const ratingGroup = res && res[1] && res[1].length && _.groupBy(res[1], function (v) { return v.jobId; });
        const memberGroup = members && members.length && _.groupBy(members, function (v) { return String(v.id); }) || {};
        const rsRatingGroup = rsRating && rsRating.length && _.groupBy(rsRating, function (v) { return String(v.jobId); }) || {};

        _.forEach(contracts, (val: Contract) => {
          let obj: AnyObject = Object.assign({}, val);
          obj.amount = totalEarnAmount
          obj.contractAmount = val.amount
          obj.job = jobGroup && jobGroup[val.jobId] && jobGroup[val.jobId][0] || {};
          if (obj.job && obj.job.id) {
            obj.job = JSON.parse(JSON.stringify(obj.job))
            obj.job.rating = ratingGroup && ratingGroup[val.jobId!] && Object.assign({}, ratingGroup[val.jobId!][0]) || {};
            obj.job.rsRating = rsRatingGroup && rsRatingGroup[val.jobId!] && Object.assign({}, rsRatingGroup[val.jobId!][0]) || {};
            if (obj.job.rating && obj.job.rating.id && obj.job.rating.byMemberId) {
              obj.job.rating.memberDetails = memberGroup && memberGroup[obj.job.rating.byMemberId] && memberGroup[obj.job.rating.byMemberId][0] || {};
            }
            if (obj.job.rsRating && obj.job.rsRating.id && obj.job.rsRating.byMemberId) {
              obj.job.rsRating.memberDetails = memberGroup && memberGroup[obj.job.rsRating.byMemberId] && memberGroup[obj.job.rsRating.byMemberId][0] || {};
            }
          }
          resContract.push(obj);
        });

        return resContract;
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

  @get('/jobs/myOpenJobs', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Job model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async myOpenJobs(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
  ): Promise<any> {
    const contracts = await this.contractRepository.find({
      where: {
        resourceId: currentUser[securityId],
        state: 'active'
      },
      fields: { id: true, jobId: true, amount: true, time: true, created: true }
    });

    if (contracts && contracts.length) {
      const jobIds: Array<any> = _.map(contracts, v => v.jobId);

      let result: Array<AnyObject> = [];

      const jobs = await this.jobRepository.find({
        where: {
          id: { inq: jobIds }
        },
        /* fields: { id: true, title: true, budget: true, time: true, created: true } */
      });

      return await Promise.all([
        this.proposalRepository.find({
          where: {
            jobId: { inq: jobIds }
          },
          fields: { id: true, jobId: true }
        }),
      ]).then((res: any) => {
        const proposalGroup = res && res[0] && _.groupBy(res[0], function (v) { return v.jobId; });
        const contractGroup = _.groupBy(contracts, function (v) { return v.jobId; });

        _.forEach(jobs, function (val: any) {
          let obj: AnyObject = Object.assign({}, val);
          obj.bids = proposalGroup && proposalGroup[val.id] && proposalGroup[val.id].length || 0;
          obj.contract = contractGroup && contractGroup[val.id] && contractGroup[val.id][0] || {};

          result.push(obj);
        });

        return result;
      }).catch((err: any) => {
        console.debug(err);
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

  @get('/jobs/myCloseJobs', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Job model instance',
        content: { 'application/json': { schema: { type: 'array' } } },
      },
    },
  })
  @authenticate('jwt')
  async myCloseJobs(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
  ): Promise<any> {
    let resContract: Array<AnyObject> = [];

    let contracts: Contract[] = await this.contractRepository.find({
      where: {
        resourceId: currentUser[securityId],
        state: 'complete'
      },
      fields: { id: true, amount: true, time: true, jobId: true, clientId: true, ended: true }
    });

    if (contracts && contracts.length) {
      const jobIds: Array<any> = _.map(contracts, v => v.jobId);
      const memberIds: Array<any> = _.map(contracts, v => v.clientId);

      return await Promise.all([
        this.jobRepository.find({
          where: {
            id: { inq: jobIds }
          },
          fields: { id: true, title: true }
        }),
        this.reviewRepository.find({
          where: {
            jobId: { inq: jobIds },
            toMemberId: currentUser[securityId],
            memberType: 'resource'
          },
          fields: { id: true, rating: true, byMemberId: true, description: true, jobId: true, created: true }
        }),
        this.memberRepository.find({
          where: {
            id: { inq: memberIds }
          },
          fields: { id: true, name: true, image: true }
        })
      ]).then((res: any) => {
        const jobGroup = res && res[0] && res[0].length && _.groupBy(res[0], function (v) { return v.id; });
        const ratingGroup = res && res[1] && res[1].length && _.groupBy(res[1], function (v) { return v.jobId; });
        const memberGroup = res && res[2] && res[2].length && _.groupBy(res[2], function (v) { return v.id; });

        _.forEach(contracts, (val: Contract) => {
          let obj: AnyObject = Object.assign({}, val);
          obj.rating = ratingGroup && ratingGroup[val.jobId!] && Object.assign({}, ratingGroup[val.jobId!][0]) || {};
          obj.job = jobGroup && jobGroup[val.jobId] && jobGroup[val.jobId][0] || {};
          obj.rating.member = memberGroup && memberGroup[val.clientId] && memberGroup[val.clientId][0] || {};

          resContract.push(obj);
        });

        return resContract;
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

  @get('/jobs/jobsForInvite', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Job model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  @authenticate('jwt')
  async jobsForInvite(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
  ): Promise<Job[]> {
    const contracts = await this.contractRepository.find({
      where: {
        clientId: currentUser[securityId],
      },
      fields: { id: true, jobId: true, resourceId: true, }
    })

    const jobIds: Array<any> = contracts && contracts.length ? _.map(contracts, v => v.jobId) : [];

    const jobs = await this.jobRepository.find({
      where: {
        id: { nin: jobIds },
        memberId: currentUser[securityId],
        state: { neq: 'close' }
      },
      order: ['created DESC'],
      fields: { id: true, title: true }
    });

    return jobs;
  }

  @post('/jobs/getJobsList', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Job model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  @authenticate('jwt')
  async getJobsList(
    @requestBody() data: AnyObject,
  ): Promise<any> {
    let query: AnyObject = { and: [] }
    query.and.push({
      status: { inq: [0, 1] }
    })
    if (data && data.title) {
      query.and.push({
        title: new RegExp('.*' + data.title + '.*', 'i')
      })
    }
    if (data && data.name) {
      query.and.push({
        name: new RegExp('.*' + data.name + '.*', 'i')
      })
    }
    if (data && data.state) {
      query.and.push({
        state: new RegExp('.*' + data.state + '.*', 'i')
      })
    }
  }
}
