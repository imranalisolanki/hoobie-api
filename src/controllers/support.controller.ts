import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Support } from '../models';
import { SupportRepository, MemberRepository } from '../repositories';
import { inject } from '@loopback/core';
import { SecurityBindings, UserProfile, securityId } from '@loopback/security';
import { authenticate } from '@loopback/authentication';
import { FCMServiceBindings, EmailServiceBindings } from '../keys';
import { FCMService, EmailService } from '../services';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { NotificationRepository } from '../repositories/notification.repository';

export class SupportController {
  constructor(
    @repository(SupportRepository) public supportRepository: SupportRepository,
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @repository(NotificationRepository) public notificationRepository: NotificationRepository,
    @inject(FCMServiceBindings.FCM_SERVICE) public fcmServices: FCMService,
    @inject(EmailServiceBindings.MAIL_SERVICE) public emailServices: EmailService
  ) { }

  @post('/supports', {
    responses: {
      '200': {
        description: 'Support model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Support) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Support, { exclude: ['id'] }),
        },
      },
    })
    support: Omit<Support, 'id'>,
  ): Promise<Support> {
    return this.supportRepository.create(support);
  }

  @get('/supports/count', {
    responses: {
      '200': {
        description: 'Support model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Support)) where?: Where<Support>,
  ): Promise<Count> {
    return this.supportRepository.count(where);
  }

  @get('/supports', {
    responses: {
      '200': {
        description: 'Array of Support model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Support) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Support)) filter?: Filter<Support>,
  ): Promise<Support[]> {
    return this.supportRepository.find(filter);
  }

  @patch('/supports', {
    responses: {
      '200': {
        description: 'Support PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Support, { partial: true }),
        },
      },
    })
    support: Support,
    @param.query.object('where', getWhereSchemaFor(Support)) where?: Where<Support>,
  ): Promise<Count> {
    return this.supportRepository.updateAll(support, where);
  }

  @get('/supports/{id}', {
    responses: {
      '200': {
        description: 'Support model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Support) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Support> {
    return this.supportRepository.findById(id);
  }

  @patch('/supports/{id}', {
    responses: {
      '204': {
        description: 'Support PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Support, { partial: true }),
        },
      },
    })
    support: Support,
  ): Promise<void> {
    await this.supportRepository.updateById(id, support);
  }

  @put('/supports/{id}', {
    responses: {
      '204': {
        description: 'Support PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() support: Support,
  ): Promise<void> {
    await this.supportRepository.replaceById(id, support);
  }

  @del('/supports/{id}', {
    responses: {
      '204': {
        description: 'Support DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.supportRepository.deleteById(id);
  }

  @post('/supports/sendEmailNotification', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of supports model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Support) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async sendEmailNotification(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    if (data && data.supportId) {
      const supportData = await this.supportRepository.findOne({
        where: {
          id: data.supportId
        }
      })
      if (supportData && supportData.id) {

        return await Promise.all([
          await this.memberRepository.findOne({
            where: {
              id: supportData.memberId
            }, fields: { id: true, name: true, email: true, fcmToken: true, jobManager: true, jobSeeker: true }
          }),
          await this.memberRepository.findOne({
            where: {
              id: currentUser[securityId]
            }, fields: { id: true, name: true, email: true, fcmToken: true }
          })
        ]).then(async (res: any) => {
          var memberNoti = res && res[0] || null
          var currentMember = res && res[1] || null
          if (memberNoti && memberNoti.fcmToken) {
            var message = {
              to: memberNoti.fcmToken,
              data: {
                name: currentMember.name,
                memberId: currentMember.id,
                type: 'supportDetail',
                userType: (memberNoti && memberNoti.jobSeeker && memberNoti.jobSeeker.title) ? "jobSeeker" : "jobManager",
                notificationObject: { supportId: supportData.id }
              },
              notification: {
                body: "Your Support Ticket " + supportData.ticketId,
                priority: "high",
                sound: "default",
                vibrate: true,
              }
            };
            // console.log(message)
            await this.fcmServices.sendNotification({ message: message })
            let notification = {
              title: "Your Support Ticket " + supportData.ticketId,
              message: data.message,
              type: 'supportDetail',
              memberId: supportData.memberId,
              userType: (memberNoti && memberNoti.jobSeeker && memberNoti.jobSeeker.title) ? "jobSeeker" : "jobManager",
              notificationObject: { supportId: supportData.id }
            }
            await this.notificationRepository.create(notification)
          } else if (memberNoti && memberNoti.email) {
            const mailOptions = {
              to: memberNoti.email,
              slug: 'breeze-support',
              message: {
                message: data.message,
                name: memberNoti.name,
                ticket: supportData.ticketId
              }
            };

            await this.emailServices.sendMail(mailOptions)
            let notification = {
              title: "Issue with ticket Id" + supportData.ticketId,
              message: data.message,
              memberId: supportData.memberId
            }
            await this.notificationRepository.create(notification)
          }
          return supportData
        })
      } else {
        throw new HttpErrors.NotFound("Support not found")
      }
    } else {
      throw new HttpErrors.NotFound("Support id is required!")
    }
  }
}
