import { inject } from '@loopback/context';
import { repository, AnyObject } from '@loopback/repository';
import { MemberRepository, JobRepository, ProposalRepository, ContractRepository, PaymentRepository, EscrowRepository } from '../repositories';
import { get, param, HttpErrors, post, getModelSchemaRef, requestBody } from '@loopback/rest';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate } from '@loopback/authentication';
import { SecurityBindings, UserProfile, securityId } from '@loopback/security';
import * as moment from 'moment';
import * as _ from 'lodash';
import { Job } from '../models';


export class ChartController {
  constructor(
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @repository(JobRepository) public jobRepository: JobRepository,
    @repository(ProposalRepository) public proposalRepository: ProposalRepository,
    @repository(ContractRepository) public contractRepository: ContractRepository,
    @repository(PaymentRepository) public paymentRepository: PaymentRepository,
    @repository(EscrowRepository) public escrowRepository: EscrowRepository
  ) { }

  @get('/charts/member', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Member model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async memberChart(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.string('startDate') startDate?: string,
    @param.query.string('endDate') endDate?: string
  ): Promise<any> {
    let result: Array<AnyObject> = [];

    let query: AnyObject = {
      created: { $lte: moment(endDate).toDate() }
    }

    if (startDate) {
      query.created['$gte'] = moment(startDate).toDate();
    }
    if (endDate) {
      query.created['$lte'] = moment(endDate).toDate();
    }

    if (!this.memberRepository.dataSource.connected) {
      await this.memberRepository.dataSource.connect();
    }
    const memberCollection = (this.memberRepository.dataSource.connector as any).collection('Member');

    return await Promise.all([
      memberCollection.aggregate([
        { $match: query },
        { $sort: { created: -1 } },
        {
          $group: {
            _id: {
              day: { $dayOfMonth: '$created' },
              month: { $month: '$created' },
              year: { $year: '$created' },
            },
            count: { $sum: 1 }
          }
        }
      ]).get(),
    ]).then((res: any) => {

      if (res && res[0] && res[0].length) {
        _.forEach(res[0], function (val) {
          result.push([moment(`${val._id.year}-${val._id.month}-${val._id.day}`, 'YYYY-MM-DD').valueOf(), val.count]);
        })
        result = _.sortBy(result, function (v) { return v[0]; }).reverse();
      }

      return result;
    }).catch((err: any) => {
      throw new HttpErrors.InternalServerError('Something went wrong!')
    });
  }

  @get('/charts/job', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Job model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async jobChart(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.string('startDate') startDate?: string,
    @param.query.string('endDate') endDate?: string
  ): Promise<any> {
    let result: Array<AnyObject> = [];

    let query: AnyObject = {
      created: { $lte: moment(endDate).toDate() }
    }

    if (startDate) {
      query.created['$gte'] = moment(startDate).toDate();
    }
    if (endDate) {
      query.created['$lte'] = moment(endDate).toDate();
    }

    if (!this.jobRepository.dataSource.connected) {
      await this.jobRepository.dataSource.connect();
    }
    const jobCollection = (this.jobRepository.dataSource.connector as any).collection('Job');

    return await Promise.all([
      jobCollection.aggregate([
        { $match: query },
        { $sort: { created: -1 } },
        {
          $group: {
            _id: {
              day: { $dayOfMonth: '$created' },
              month: { $month: '$created' },
              year: { $year: '$created' },
            },
            count: { $sum: 1 }
          }
        }
      ]).get(),
    ]).then((res: any) => {

      if (res && res[0] && res[0].length) {
        _.forEach(res[0], function (val) {
          result.push([moment(`${val._id.year}-${val._id.month}-${val._id.day}`, 'YYYY-MM-DD').valueOf(), val.count]);
        })
        result = _.sortBy(result, function (v) { return v[0]; }).reverse();
      }

      return result;
    }).catch((err: any) => {
      throw new HttpErrors.InternalServerError('Something went wrong!')
    });
  }

  @get('/charts/proposal', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Proposal model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async proposalChart(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.string('startDate') startDate?: string,
    @param.query.string('endDate') endDate?: string
  ): Promise<any> {
    let result: Array<AnyObject> = [];

    let query: AnyObject = {
      created: { $lte: moment(endDate).toDate() }
    }

    if (startDate) {
      query.created['$gte'] = moment(startDate).toDate();
    }
    if (endDate) {
      query.created['$lte'] = moment(endDate).toDate();
    }

    if (!this.proposalRepository.dataSource.connected) {
      await this.proposalRepository.dataSource.connect();
    }
    const proposalCollection = (this.proposalRepository.dataSource.connector as any).collection('Proposal');

    return await Promise.all([
      proposalCollection.aggregate([
        { $match: query },
        { $sort: { created: -1 } },
        {
          $group: {
            _id: {
              day: { $dayOfMonth: '$created' },
              month: { $month: '$created' },
              year: { $year: '$created' },
            },
            count: { $sum: 1 }
          }
        }
      ]).get(),
    ]).then((res: any) => {

      if (res && res[0] && res[0].length) {
        _.forEach(res[0], function (val) {
          result.push([moment(`${val._id.year}-${val._id.month}-${val._id.day}`, 'YYYY-MM-DD').valueOf(), val.count]);
        })
        result = _.sortBy(result, function (v) { return v[0]; }).reverse();
      }

      return result;
    }).catch((err: any) => {
      throw new HttpErrors.InternalServerError('Something went wrong!')
    });
  }

  @get('/charts/contract', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Contract model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async contractChart(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.string('startDate') startDate?: string,
    @param.query.string('endDate') endDate?: string
  ): Promise<any> {
    let result: Array<AnyObject> = [];

    let query: AnyObject = {
      created: { $lte: moment(endDate).toDate() }
    }

    if (startDate) {
      query.created['$gte'] = moment(startDate).toDate();
    }
    if (endDate) {
      query.created['$lte'] = moment(endDate).toDate();
    }

    if (!this.contractRepository.dataSource.connected) {
      await this.contractRepository.dataSource.connect();
    }
    const contractCollection = (this.contractRepository.dataSource.connector as any).collection('Contract');

    return await Promise.all([
      contractCollection.aggregate([
        { $match: query },
        { $sort: { created: -1 } },
        {
          $group: {
            _id: {
              day: { $dayOfMonth: '$created' },
              month: { $month: '$created' },
              year: { $year: '$created' },
            },
            count: { $sum: 1 }
          }
        }
      ]).get(),
    ]).then((res: any) => {

      if (res && res[0] && res[0].length) {
        _.forEach(res[0], function (val) {
          result.push([moment(`${val._id.year}-${val._id.month}-${val._id.day}`, 'YYYY-MM-DD').valueOf(), val.count]);
        })
        result = _.sortBy(result, function (v) { return v[0]; }).reverse();
      }

      return result;
    }).catch((err: any) => {
      throw new HttpErrors.InternalServerError('Something went wrong!')
    });
  }

  @get('/charts/payments', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of payments model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async paymentsChart(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.string('startDate') startDate?: string,
    @param.query.string('endDate') endDate?: string
  ): Promise<any> {
    let result: Array<AnyObject> = [];

    let query: AnyObject = {
      modified: { $lte: moment(endDate).toDate() }
    }
    query.state = "success";
    if (startDate) {
      query.modified['$gte'] = moment(startDate).toDate();
    }
    if (endDate) {
      query.modified['$lte'] = moment(endDate).toDate();
    }

    if (!this.paymentRepository.dataSource.connected) {
      await this.paymentRepository.dataSource.connect();
    }
    const paymentCollection = (this.paymentRepository.dataSource.connector as any).collection('Payment');
    return await Promise.all([
      paymentCollection.aggregate([
        { $match: query },
        { $sort: { modified: -1 } },
        {
          $group: {
            _id: {
              day: { $dayOfMonth: '$modified' },
              month: { $month: '$modified' },
              year: { $year: '$modified' },
            },
            count: { $sum: 1 }
          }
        }
      ]).get(),
    ]).then((res: any) => {

      if (res && res[0] && res[0].length) {
        _.forEach(res[0], function (val) {
          result.push([moment(`${val._id.year}-${val._id.month}-${val._id.day}`, 'YYYY-MM-DD').valueOf(), val.count]);
        })
        result = _.sortBy(result, function (v) { return v[0]; }).reverse();
      }
      return result;
    }).catch((err: any) => {
      throw new HttpErrors.InternalServerError('Something went wrong!')
    });
  }


  @get('/charts/escrowPayment', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of escrow model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async escrowPaymentChart(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.string('startDate') startDate?: string,
    @param.query.string('endDate') endDate?: string
  ): Promise<any> {
    let result: Array<AnyObject> = [];

    let query: AnyObject = {
      created: { $lte: moment(endDate).toDate() }
    }
    query.state = "success";
    if (startDate) {
      query.created['$gte'] = moment(startDate).toDate();
    }
    if (endDate) {
      query.created['$lte'] = moment(endDate).toDate();
    }

    if (!this.escrowRepository.dataSource.connected) {
      await this.escrowRepository.dataSource.connect();
    }
    const paymentCollection = (this.escrowRepository.dataSource.connector as any).collection('Escrow');
    return await Promise.all([
      paymentCollection.aggregate([
        { $match: query },
        { $sort: { created: -1 } },
        {
          $group: {
            _id: {
              day: { $dayOfMonth: '$created' },
              month: { $month: '$created' },
              year: { $year: '$created' },
            },
            count: { $sum: 1 }
          }
        }
      ]).get(),
    ]).then((res: any) => {

      if (res && res[0] && res[0].length) {
        _.forEach(res[0], function (val) {
          result.push([moment(`${val._id.year}-${val._id.month}-${val._id.day}`, 'YYYY-MM-DD').valueOf(), val.count]);
        })
        result = _.sortBy(result, function (v) { return v[0]; }).reverse();
      }

      return result;
    }).catch((err: any) => {
      throw new HttpErrors.InternalServerError('Something went wrong!')
    });
  }
}
