import { Count, CountSchema, Filter, repository, Where, AnyObject, relation, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Proposal, Invitation } from '../models';
import { ProposalRepository, MemberRepository, JobRepository, InvitationRepository, ContractRepository } from '../repositories';
import { inject } from '@loopback/core';
import { SecurityBindings, UserProfile, securityId } from '@loopback/security';
import { authenticate } from '@loopback/authentication';
import * as _ from 'lodash';
import { ObjectID } from 'mongodb';
import * as moment from 'moment';
import * as common from '../services/common';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { FCMServiceBindings } from '../keys';
import { FCMService } from '../services';
import { NotificationRepository } from '../repositories/notification.repository';

export class ProposalController {
  constructor(
    @repository(ProposalRepository) public proposalRepository: ProposalRepository,
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @repository(JobRepository) public jobRepository: JobRepository,
    @repository(InvitationRepository) public invitationRepository: InvitationRepository,
    @repository(ContractRepository) public contractRepository: ContractRepository,
    @inject(FCMServiceBindings.FCM_SERVICE) public fcmService: FCMService,
    @repository(NotificationRepository) public notificationRepository: NotificationRepository,
  ) { }

  @post('/proposals', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Proposal model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Proposal) } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Proposal, { exclude: ['id'] }),
        },
      },
    })
    proposal: Omit<Proposal, 'id'>,
  ): Promise<any> {
    //console.log(currentUser)
    //return
    const checkProposal = await this.proposalRepository.count({
      jobId: proposal.jobId,
      memberId: proposal.memberId,
      state: { neq: 'withdraw' }
    })
    if (checkProposal && checkProposal.count) {
      let cErr = new HttpErrors.UnprocessableEntity("You have already submitted a proposal for this job. Would you like to edit your proposed terms?")
      cErr.name = 'ERROR';
      throw cErr;
    } else {
      return await Promise.all([
        this.memberRepository.findOne({
          where: {
            id: currentUser[securityId]
          }, fields: { id: true, name: true, fcmToken: true },
        }),
        this.proposalRepository.create(proposal),
        this.jobRepository.findOne({
          where: {
            id: proposal.jobId
          },
          include: [
            {
              relation: "member"
            }
          ],
          fields: { id: true, memberId: true },
        }),

      ]).then(async (res: any) => {
        let loginMember = res && res[0] || null;
        let proposal = res && res[1] || null
        let inviteMember = res && res[2] || null
        if (inviteMember && inviteMember.member && inviteMember.member.id) {
          var message = {
            // to: inviteMember.member.fcmToken && ,
            data: {
              name: loginMember.name,
              memberId: loginMember.id,
              jobId: proposal.jobId,
              type: 'proposalDetails',
              userType: (inviteMember && inviteMember.member && inviteMember.member.jobSeeker && inviteMember.member.jobSeeker.title) ? "jobSeeker" : "jobManager",
              notificationObject: { jobId: proposal.jobId }
            },
            notification: {
              title: 'Hoobie',
              body: "You have received a new proposal",
              priority: "high",
              sound: "default",
              vibrate: true,
            }
          };
          //console.log(message)
          await this.fcmService.sendNotification({ message: message })

          let notification = {
            title: "You have received a new proposal",
            message: proposal.message,
            memberId: inviteMember.member && inviteMember.member.id,
            type: "proposalDetails",
            userType: (inviteMember && inviteMember.member && inviteMember.member.jobSeeker && inviteMember.member.jobSeeker.title) ? "jobSeeker" : "jobManager",
            notificationObject: { jobId: proposal.jobId }
          }
          await this.notificationRepository.create(notification)
        }
        return proposal;
      })
    }

  }

  @get('/proposals/count', {
    responses: {
      '200': {
        description: 'Proposal model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Proposal)) where?: Where<Proposal>,
  ): Promise<Count> {
    return this.proposalRepository.count(where);
  }

  @get('/proposals', {
    responses: {
      '200': {
        description: 'Array of Proposal model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Proposal) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Proposal)) filter?: Filter<Proposal>,
  ): Promise<Proposal[]> {
    return this.proposalRepository.find(filter);
  }

  @patch('/proposals', {
    responses: {
      '200': {
        description: 'Proposal PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Proposal, { partial: true }),
        },
      },
    })
    proposal: Proposal,
    @param.query.object('where', getWhereSchemaFor(Proposal)) where?: Where<Proposal>,
  ): Promise<Count> {
    return this.proposalRepository.updateAll(proposal, where);
  }

  @get('/proposals/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Proposal model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  @authenticate('jwt')
  async findById(@param.path.string('id') id: string): Promise<AnyObject> {
    const proposal = await this.proposalRepository.findById(id, {
      fields: { id: true, jobId: true, amount: true, time: true, message: true, created: true, memberId: true, files: true, images: true }
    });

    let resProposal: AnyObject = Object.assign({}, proposal, { job: {} });

    if (proposal && proposal.jobId) {
      const job = await this.jobRepository.findOne({
        where: {
          id: proposal.jobId
        },
        include: [
          { relation: "member" }
        ],
        /*  fields: { id: true, description: true, budget: true, time: true, title: true, memberId: true } */
      });

      resProposal.job = _.cloneDeep(job);
    }

    return resProposal;
  }

  @patch('/proposals/{id}', {
    responses: {
      '204': {
        description: 'Proposal PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Proposal, { partial: true }),
        },
      },
    })
    proposal: Proposal,
  ): Promise<any> {
    await this.proposalRepository.updateById(id, proposal);

    const proposalDetail = await this.proposalRepository.findById(id, {
      fields: { id: true, jobId: true, memberId: true, invitationId: true },
      include: [
        { relation: "job" },
        { relation: "member" }
      ]
    })

    if (proposal && proposal.state && proposal.state === 'decline') {
      if (proposalDetail && proposalDetail.id) {
        let memId = proposalDetail && proposalDetail.memberId || proposalDetail.invitationId
        const member = await this.memberRepository.findOne({
          where: {
            id: memId
          }, fields: { id: true, name: true, fcmToken: true, jobManager: true, jobSeeker: true }
        });
        if (member && member.id) {
          var message = {
            // to: member && member.fcmToken || "",
            data: {
              name: member.name,
              memberId: member.id,
              jobId: proposalDetail.jobId,
              type: 'proposalDetails',
              userType: (member && member.jobSeeker && member.jobSeeker.title) ? "jobSeeker" : "jobManager",
              notificationObject: { jobId: proposal.jobId }
            },
            notification: {
              title: 'Hoobie',
              body: "Sorry! Your proposal has been declined for " + (proposalDetail && proposalDetail.job && proposalDetail.job.title || "") + " You have received a message from " + (member && member.name || ""),
              priority: "high",
              sound: "default",
              vibrate: true,
            }
          };
          await this.fcmService.sendNotification({ message: message })
          let notification = {
            title: "proposal has  declined",
            message: "Sorry! Your proposal has been declined for " + (proposalDetail && proposalDetail.job && proposalDetail.job.title || "") + " You have received a message from " + (member && member.name || ""),
            memberId: member.id,
            userType: (member && member.jobSeeker && member.jobSeeker.title) ? "jobSeeker" : "jobManager",
            type: "proposalDetails",
            notificationObject: { jobId: proposal.jobId }
          }
          await this.notificationRepository.create(notification)
        }
      } else {
        throw new HttpErrors.no
      }
    }
    if (proposal && proposal.state && proposal.state === 'withdraw') {
      const jobDetails = await this.jobRepository.findOne({
        where: { id: proposalDetail.jobId },
        include: [
          { relation: "member" }
        ]
      })
      if (jobDetails && jobDetails.member && jobDetails.member.id) {

        var messages = {
          // to: jobDetails && jobDetails.member && jobDetails.member.fcmToken || "",
          data: {
            name: jobDetails && jobDetails.member && jobDetails.member.name || "",
            memberId: jobDetails && jobDetails.member && jobDetails.member.id || "",
            jobId: proposalDetail.jobId,
            type: 'proposalDetails',
            userType: "jobManager",
            notificationObject: { jobId: proposal.jobId }
          },
          notification: {
            title: 'Hoobie',
            body: (proposalDetail && proposalDetail.member && proposalDetail.member.name) + " has withdrawn their proposal for the job " + jobDetails.title,
            priority: "high",
            sound: "default",
            vibrate: true,
          }
        };
        await this.fcmService.sendNotification({ message: messages })
        let notification = {
          title: (proposalDetail && proposalDetail.member && proposalDetail.member.name) + " has withdrawn their proposal",
          message: (proposalDetail && proposalDetail.member && proposalDetail.member.name) + " has withdrawn their proposal for the job " + jobDetails.title,
          memberId: jobDetails && jobDetails.member && jobDetails.member.id || "",
          userType: "jobManager",
          type: "proposalDetails",
          notificationObject: { jobId: proposal.jobId }
        }

        await this.notificationRepository.create(notification)
        //}
      }
    }
  }

  @put('/proposals/{id}', {
    responses: {
      '204': {
        description: 'Proposal PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() proposal: Proposal,
  ): Promise<void> {
    await this.proposalRepository.replaceById(id, proposal);
  }

  @del('/proposals/{id}', {
    responses: {
      '204': {
        description: 'Proposal DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.proposalRepository.deleteById(id);
  }

  @get('/proposals/job-proposal/{jobId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Proposal model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async jobProposal(
    @param.path.string('jobId') id: string,
    @inject(SecurityBindings.USER) currentUser: UserProfile
  ): Promise<any> {
    let resProposal: Array<AnyObject> = [];

    const proposals = await this.proposalRepository.find({
      where: {
        jobId: id,
        state: { neq: "withdraw" }
      },
      fields: { id: true, time: true, amount: true, memberId: true, message: true }
    });

    if (proposals && proposals.length) {
      const proposalMemberIds: Array<any> = _.map(proposals, v => new ObjectID(v.memberId)!);

      const members = await this.memberRepository.find({
        where: {
          id: { inq: proposalMemberIds }
        },
        fields: { id: true, name: true, jobSeeker: true, location: true, image: true, isVerified: true }
      })
      const contracts = await this.contractRepository.find({
        where: {
          resourceId: { inq: proposalMemberIds }
        }
      })
      const completeContracts = _.filter(contracts, function (val: any) { return val.state === 'complete' })
      const memberGroup = members && members.length && _.groupBy(members, function (v) { return v.id; });
      const contractList = contracts && contracts.length && _.groupBy(contracts, function (v) { return v.resourceId; });
      const contractComplete = completeContracts && completeContracts.length && _.groupBy(completeContracts, function (v) { return v.resourceId; });

      _.forEach(proposals, function (val: any) {
        let obj: AnyObject = Object.assign({}, val);
        let completeCont = contractComplete && contractComplete[val.memberId] && contractComplete[val.memberId].length || 0;
        let Conttract = contractList && contractList[val.memberId] && contractList[val.memberId].length || 0;
        let sucessRate = (completeCont) * 100 / (Conttract)
        obj.successRate = sucessRate && sucessRate !== null && sucessRate || 0
        obj.member = memberGroup && memberGroup[val.memberId] && memberGroup[val.memberId][0] || {};

        resProposal.push(obj);
      });

      return resProposal;
    } else {
      return [];
    }
  }

  @get('/proposals/submitted', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Proposal model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async submittedProposals(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.string('state') state?: string
  ): Promise<any> {
    let resProposal: Array<AnyObject> = [];
    const siteOption = common.getSiteOptions();
    const archiveDate = moment().subtract(siteOption.archiveDays, 'd').toDate();

    let query: AnyObject = {
      status: 1,
      memberId: currentUser[securityId],
      state: 'submitted'
    }

    if (state && state === 'archive') {
      query.created = { lte: archiveDate }
    } else {
      query.created = { gte: archiveDate }
    }

    const proposals = await this.proposalRepository.find({
      where: query,
      order: ['created DESC'],
      fields: { id: true, time: true, amount: true, memberId: true, jobId: true, created: true }
    });

    if (proposals && proposals.length) {
      const jobIds: Array<any> = _.map(proposals, v => v.jobId!);

      if (!this.proposalRepository.dataSource.connected) {
        await this.proposalRepository.dataSource.connect();
      }
      const proposalCollection = (this.proposalRepository.dataSource.connector as any).collection('Proposal');

      return await Promise.all([
        this.jobRepository.find({
          where: {
            id: { inq: _.cloneDeep(jobIds) }
          },
          /*  fields: { id: true, title: true, budget: true, time: true, } */
        }).then(res => res),
        proposalCollection.aggregate([
          {
            $match: {
              jobId: { $in: _.cloneDeep(jobIds) },
            }
          },
          {
            $group: {
              _id: '$jobId',
              totalBid: { $sum: 1 }
            }
          }
        ]).get(),
      ]).then((res: any) => {
        const jobGroup = res && res[0] && res[0].length && _.groupBy(res[0], function (v) { return v.id; });
        const proposalGroup = res && res[1] && res[1].length && _.groupBy(res[1], function (v) { return v._id; });

        _.forEach(proposals, function (val: any) {
          let obj: AnyObject = Object.assign({}, val);
          obj.job = jobGroup && jobGroup[val.jobId] && jobGroup[val.jobId][0] || {};
          obj.bids = proposalGroup && proposalGroup[val.jobId] && proposalGroup[val.jobId][0] && proposalGroup[val.jobId][0].totalBid || 0;

          resProposal.push(obj);
        });

        return resProposal;
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

  @get('/proposals/in-discussion', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Proposal model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async inDiscussionProposals(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.string('state') state?: string
  ): Promise<any> {
    let resProposal: Array<AnyObject> = [];
    const siteOption = common.getSiteOptions();
    const archiveDate = moment().subtract(siteOption.archiveDays, 'd').toDate();

    let query: AnyObject = {
      status: 1,
      memberId: currentUser[securityId],
      state: 'discussion'
    }

    if (state && state === 'archive') {
      query.created = { lte: archiveDate }
    } else {
      query.created = { gte: archiveDate }
    }

    const proposals = await this.proposalRepository.find({
      where: query,
      order: ['created DESC'],
      fields: { id: true, time: true, amount: true, memberId: true, jobId: true, created: true, state: true }
    });

    if (proposals && proposals.length) {
      const jobIds: Array<any> = _.map(proposals, v => v.jobId!);

      if (!this.proposalRepository.dataSource.connected) {
        await this.proposalRepository.dataSource.connect();
      }
      const proposalCollection = (this.proposalRepository.dataSource.connector as any).collection('Proposal');

      return await Promise.all([
        this.jobRepository.find({
          where: {
            id: { inq: _.cloneDeep(jobIds) }
          },
/*           fields: { id: true, title: true, budget: true, time: true, memberId: true },
 */          include: [{
            relation: "member"
          }
          ],
        }).then(res => res),
        proposalCollection.aggregate([
          {
            $match: {
              jobId: { $in: _.cloneDeep(jobIds) },
            }
          },
          {
            $group: {
              _id: '$jobId',
              totalBid: { $sum: 1 }
            }
          }
        ]).get(),
      ]).then((res: any) => {
        const jobGroup = res && res[0] && res[0].length && _.groupBy(res[0], function (v) { return v.id; });
        const proposalGroup = res && res[1] && res[1].length && _.groupBy(res[1], function (v) { return v._id; });

        _.forEach(proposals, function (val: any) {
          let obj: AnyObject = Object.assign({}, val);
          obj.job = jobGroup && jobGroup[val.jobId] && jobGroup[val.jobId][0] || {};
          obj.bids = proposalGroup && proposalGroup[val.jobId] && proposalGroup[val.jobId][0] && proposalGroup[val.jobId][0].totalBid || 0;

          resProposal.push(obj);
        });

        return resProposal;
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

  @post('/proposals/createByInvitation', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Proposal model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Proposal) } },
      },
    },
  })
  @authenticate('jwt')
  async proposalCreateByInvitation(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Proposal, { exclude: ['id'] }),
        },
      },
    })
    proposal: Omit<Proposal, 'id'>,
  ): Promise<Proposal> {
    const checkProposal = await this.proposalRepository.count({
      jobId: proposal.jobId,
      memberId: proposal.memberId
    })
    if (checkProposal && checkProposal.count) {
      let cErr = new HttpErrors.UnprocessableEntity("You have already submitted a proposal for this job. Would you like to edit your proposed terms?")
      cErr.name = 'ERROR';
      throw cErr;
    } else {
      const resProposal = await this.proposalRepository.create(proposal);
      if (resProposal && resProposal.id && resProposal.invitationId) {
        let invitationObj: Partial<Invitation> = {
          state: 'accept'
        }
        const memberDetails = await this.memberRepository.findOne({
          where: {
            id: proposal.memberId
          }, fields: { id: true, name: true, fcmToken: true }
        })
        if (memberDetails && memberDetails.id) {
          var message = {
            // to: memberDetails && memberDetails.fcmToken || "",
            data: {
              name: memberDetails.name,
              memberId: memberDetails.id,
              type: 'proposalDetail'
            },
            notification: {
              title: 'welcome Hoobie',
              body: "Your has been accepted sucessfully!",
              priority: "high",
              sound: "default",
              vibrate: true,
            }
          };
          await this.fcmService.sendNotification({ message: message })
        }
        await this.invitationRepository.updateById(resProposal.invitationId, invitationObj);
      }

      return resProposal;
    }

  }

  @get('/proposals/proposalDetailsForClient/{proposalId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'A Proposal model instances',
        content: {
          'application/json': {
            schema: { type: 'object' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async proposalDetailsForClient(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.path.string('proposalId') proposalId: string
  ): Promise<any> {
    let resProposal: AnyObject = {};

    const proposal = await this.proposalRepository.findById(proposalId, {
      fields: { id: true, time: true, amount: true, memberId: true, message: true, files: true, jobId: true, images: true },
      include: [{
        relation: 'member',
      }, {
        relation: 'job',
      }]
    });

    if (proposal && proposal.id) {
      // const member = await this.memberRepository.findById(proposal.memberId, {
      //   fields: { id: true, name: true, jobSeeker: true, location: true, image: true, isVerified: true }
      // });

      const contracts = await this.contractRepository.find({
        where: {
          resourceId: proposal.memberId
        }
      })
      const completedJobs = _.filter(contracts, function (val: any) {
        return val.state === 'complete'
      })
      let sucessRate = (completedJobs && completedJobs.length) * 100 / (contracts && contracts.length)
      // console.log(sucessRate, "sucessRate")
      // console.log(completedJobs.length, "completedJobs")
      // console.log(contracts.length, "contracts")
      resProposal = Object.assign({}, proposal);
      resProposal.successRate = sucessRate && sucessRate !== null && sucessRate || 0
      // resProposal.member = member || {};
      resProposal.completedJobs = completedJobs && completedJobs.length || 0;

      return resProposal;
    } else {
      throw new HttpErrors.NotFound('No proposal detials found for given id');
    }
  }

  @get('/proposals/proposalDetailsForResource/{proposalId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'A Proposal model instances',
        content: {
          'application/json': {
            schema: { type: 'object' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async proposalDetailsForResource(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.path.string('proposalId') proposalId: string
  ): Promise<Proposal> {

    return await this.proposalRepository.findById(proposalId, {
      fields: { id: true, time: true, amount: true, memberId: true, message: true, jobId: true },
      include: [{
        relation: 'job',
      }]
    });
  }

  @post('/proposals/searchproposals', {
    responses: {
      '200': {
        description: 'Array of proposals model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Proposal) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async searchproposals(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    let resproposals: Array<AnyObject> = [];
    let resproposalsData: AnyObject = {}
    let jobIds: Array<any> = [];
    let memberId: Array<any> = [];
    let query: AnyObject = { and: [] }
    let jobQuery: AnyObject = {}
    query.and.push({
      status: { inq: [0, 1] }
    })
    if (data) {
      if (data && data.title) {
        jobQuery = {
          title: new RegExp('.*' + data.title + '.*', 'i')
        }
        const jobs = await this.jobRepository.find({
          where: jobQuery
        });

        jobIds = _.map(jobs, (v) => { return String(v.id); });
        if (jobIds && jobIds.length) {
          query.and.push({
            jobId: { inq: jobIds }
          })
        }
      }

      if (data && data.name) {
        jobQuery = {
          name: new RegExp('.*' + data.name + '.*', 'i')
        }
        const members = await this.memberRepository.find({
          where: jobQuery
        });
        memberId = _.map(members, (w) => { return String(w.id) })
        if (memberId && memberId.length > 0) {
          query.and.push({
            memberId: { inq: memberId }
          })
        }
      }
    }

    if (data && data.state) {
      query.and.push({
        state: new RegExp('.*' + data.state + '.*', 'i')
      })
    }
    const proposal = await this.proposalRepository.find({
      where: query,
      order: [data.order],
    });

    if (proposal && proposal.length) {
      let cJobIds = _.map(proposal, v => { return v.jobId; });
      let cMemberIds = _.map(proposal, v => { return v && v.memberId != '' && v.memberId || v.invitationId; });
      return await Promise.all([
        this.jobRepository.find({
          where: {
            id: { inq: cJobIds }
          },
          fields: { id: true, title: true },
          order: [data.order]
        }).then(res => res),
        this.memberRepository.find({
          where: {
            id: { inq: cMemberIds }
          },
          order: [data.order],
          fields: { id: true, name: true, image: true }
        }).then(res => res)
      ]).then((res: any) => {
        const jobGroup = res && res[0] && res[0].length && _.groupBy(res[0], function (v) { return v.id; });
        const memberGroup = res && res[1] && res[1].length && _.groupBy(res[1], function (v) { return v.id; });
        _.forEach(proposal, (val: Proposal) => {
          let obj: AnyObject = Object.assign({}, val);
          obj.job = jobGroup && jobGroup[val.jobId] && jobGroup[val.jobId][0] || {};
          obj.member = memberGroup && memberGroup[val.memberId] && memberGroup[val.memberId][0] || memberGroup && memberGroup[val.invitationId] && memberGroup[val.invitationId][0] || {}

          resproposals.push(obj);
        });
        let totalCount = resproposals && resproposals.length || 0
        let start = data && data.skip || 0

        if (data && data.sorting === 'title') {
          resproposals = _.orderBy(resproposals, function (e) { return _.lowerCase(e.job.title) }, [data.sortBy]);
          resproposals = _.slice(resproposals, start, start + data.limit)
          resproposalsData = {
            totalProposalCount: totalCount,
            data: resproposals
          }
          return resproposalsData;
        }
        else if (data && data.sorting === 'name') {
          resproposals = _.orderBy(resproposals, function (e) { return _.lowerCase(e.member.name) }, [data.sortBy]);
          resproposals = _.slice(resproposals, start, start + data.limit)
          resproposalsData = {
            totalProposalCount: totalCount,
            data: resproposals
          }
          return resproposalsData;
        } else {
          resproposals = _.slice(resproposals, start, start + data.limit)
          resproposalsData = {
            totalProposalCount: totalCount,
            data: resproposals
          }
          return resproposalsData;
        }


      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

  @post('/proposals/proposalsInDiscussion', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Proposal model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async proposalsInDiscussion(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject
  ): Promise<any> {
    if (data && data.jobId && data.memberId) {
      const proposalData = await this.proposalRepository.findOne({
        where: {
          jobId: data.jobId,
          memberId: data.memberId,
          state: { inq: ["active", "submitted"] }
        }
      });
      // console.log(proposalData, '-------------------')
      if (proposalData && proposalData.id) {
        const updates = await this.proposalRepository.updateById(proposalData.id, { state: "discussion" });
        return updates;
      } else {
        throw new HttpErrors.NotFound("Proposal Not Found");
      }
    } else {
      throw new HttpErrors.NotFound("Request data not found");
    }
  }
}
