import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Payment } from '../models';
import { PaymentRepository, MemberRepository, EscrowRepository, ContractRepository, JobRepository, DisputeRepository, CronLogsRepository, JobWorksRepository, NotificationRepository } from '../repositories';
import { inject } from '@loopback/core';
import { SecurityBindings, UserProfile, securityId } from '@loopback/security';
import * as Stripe from 'stripe';
import * as common from '../services/common';
import * as _ from 'lodash';
import * as async from 'async'
import * as moment from 'moment'
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate } from '@loopback/authentication';
import { FCMServiceBindings } from '../keys';
import { FCMService } from '../services';


export class PaymentController {
  constructor(
    @repository(PaymentRepository) public paymentRepository: PaymentRepository,
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @repository(EscrowRepository) public escroRepository: EscrowRepository,
    @repository(ContractRepository) public contractRepository: ContractRepository,
    @repository(JobRepository) public jobRepository: JobRepository,
    @repository(DisputeRepository) public disputeRepository: DisputeRepository,
    @repository(CronLogsRepository) public cronlogRepository: CronLogsRepository,
    @repository(JobWorksRepository) public jobworkRepository: JobWorksRepository,
    @inject(FCMServiceBindings.FCM_SERVICE) public fcmServices: FCMService,
    @repository(NotificationRepository) public notificationRepository: NotificationRepository
  ) { }

  @post('/payments', {
    responses: {
      '200': {
        description: 'Payment model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Payment) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Payment, {
            title: 'NewPayment',
            exclude: ['id'],
          }),
        },
      },
    })
    payment: Omit<Payment, 'id'>,
  ): Promise<any> {
    const escroAmounts = await this.escroRepository.find({
      where: {
        jobId: payment.jobId
      }
    });
    const payments = await this.paymentRepository.find({
      where: {
        jobId: payment.jobId,
        state: "pending"
      }
    })
    let paymentAmount = _.sumBy(payments, v => v.amount);
    let totalPayments = paymentAmount + payment.amount

    let totalPendingAmount = _.sumBy(escroAmounts, vl => vl.amount);

    if (totalPendingAmount < totalPayments) {
      throw new HttpErrors.NotFound("No sufficient amount")
    }
    else {
      let pTransferDate = moment(payment.created).add(+3, 'days').endOf('day').toDate();
      payment.paymentTransferDate = String(pTransferDate);
      return await Promise.all([
        this.paymentRepository.create(payment),
        this.escroRepository.findOne({
          where: {
            memberId: payment.memberId,
            jobId: payment.jobId
          }
        }),
        this.memberRepository.findOne({
          where: {
            id: payment.memberId
          }, fields: { id: true, name: true, fcmToken: true }
        })
      ]).then(async (res: any) => {
        let ecsrowData = res && res[1] || {}
        let memberDetail = res && res[2] || {}
        if (memberDetail && memberDetail.fcmToken) {
          var message = {
            to: memberDetail.fcmToken,
            data: {
              name: memberDetail.name,
              memberId: memberDetail.id,
              type: 'paymentDetail'
            },
            notification: {
              title: 'welcome Hoobie',
              body: "Congratulations! Payment will be credited successfully to your account within the next 72 hours",
              priority: "high",
              sound: "default",
              vibrate: true,
            }
          };
          await this.fcmServices.sendNotification({ message: message })
          let notification = {
            title: "Payment has been successful!",
            message: "Congratulations payment has been credited to your account",
            memberId: memberDetail && memberDetail.id,
            type: "paymentDetail",
            userType: "jobSeeker",
            notificationObject: { memberId: memberDetail.id }
          }
          await this.notificationRepository.create(notification)
          if (ecsrowData && ecsrowData.id) {
            await this.escroRepository.updateById(ecsrowData.id, { isPending: true });
            return res[0];
          } else {
            return res[0];
          }
        } else {
          return res[0];
        }
      })
    }
  }

  @get('/payments/count', {
    responses: {
      '200': {
        description: 'Payment model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Payment)) where?: Where<Payment>,
  ): Promise<Count> {
    return this.paymentRepository.count(where);
  }

  @get('/payments', {
    responses: {
      '200': {
        description: 'Array of Payment model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Payment) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Payment)) filter?: Filter<Payment>,
  ): Promise<Payment[]> {
    return this.paymentRepository.find(filter);
  }

  @patch('/payments', {
    responses: {
      '200': {
        description: 'Payment PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Payment, { partial: true }),
        },
      },
    })
    payment: Payment,
    @param.query.object('where', getWhereSchemaFor(Payment)) where?: Where<Payment>,
  ): Promise<Count> {
    return this.paymentRepository.updateAll(payment, where);
  }

  @get('/payments/{id}', {
    responses: {
      '200': {
        description: 'Payment model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Payment) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Payment> {
    return this.paymentRepository.findById(id);
  }

  @patch('/payments/{id}', {
    responses: {
      '204': {
        description: 'Payment PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Payment, { partial: true }),
        },
      },
    })
    payment: Payment,
  ): Promise<void> {
    await this.paymentRepository.updateById(id, payment);
  }

  @put('/payments/{id}', {
    responses: {
      '204': {
        description: 'Payment PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() payment: Payment,
  ): Promise<void> {
    await this.paymentRepository.replaceById(id, payment);
  }

  @del('/payments/{id}', {
    responses: {
      '204': {
        description: 'Payment DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.paymentRepository.deleteById(id);
  }

  @get('/payments/authenticateConnectAccount', {
    responses: {
      '200': {
        description: 'payments model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Payment) } },
      },
    },
  })
  async authenticateConnectAccount(
    @param.query.string('state') state?: string,
    @param.query.string('code') code?: string,
  ): Promise<any> {
    //const siteOption = common.getSiteOptions();
    //const stripe = new Stripe(siteOption.stripe.secretKey);
    const stripe = require('stripe')('sk_live_cSaPv1OPMmXYyeUOkYyQG4oT00PrCn9hFk');
    if (state && code) {
      const memberDetails = await this.memberRepository.findOne({
        where: {
          id: state
        }, fields: { id: true, stripeCustomerId: true, stripeAccount: true },
      })
      if (memberDetails && memberDetails.id) {
        return await Promise.all([
          await stripe.oauth.token({
            grant_type: 'authorization_code',
            code: code,
          })
        ]).then(async (res: any) => {
          var oathAccount = res && res[0] || null
          if (oathAccount && oathAccount.stripe_user_id) {
            const stripeUser = {
              stripeAccount: {
                stripeUserId: oathAccount.stripe_user_id
              }
            }
            const updateMember = await this.memberRepository.updateById(memberDetails.id, stripeUser)
            return updateMember
          } else {
            throw new HttpErrors.NotFound("stripe user id is required")
          }
        })
      } else {
        throw new HttpErrors.NotFound("Member not found")
      }

    } else {
      throw new HttpErrors.NotFound("code and member id is required!")
    }
  }
  @post('/payments/paymentTransfer', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'payments model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Payment) } },
      },
    },
  })
  @authenticate('jwt')
  async paymentTransfer(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    const siteOption = common.getSiteOptions();
    const stripe = new Stripe(siteOption.stripe.secretKey);
    const charges = siteOption.charges.resourceCharge;

    if (data && data.amount && data.contractId) {
      if (data && data.amount && data.amount > 0) {
        const contractDetails = await this.contractRepository.findOne({
          where: {
            id: data.contractId
          }, fields: { id: true, resourceId: true, jobId: true, clientId: true },
        })

        if (contractDetails && contractDetails.resourceId) {
          return await Promise.all([
            await this.memberRepository.findOne({
              where: {
                id: contractDetails.resourceId
              }, fields: { id: true, stripeAccount: true }
            }),
            await this.escroRepository.findOne({
              where: {
                memberId: currentUser[securityId],
                jobId: contractDetails.jobId
              }
            })
          ]).then(async (res: any) => {
            var memberDetails = res && res[0] || null
            var escroDetails = res && res[1] || null
            if (memberDetails && memberDetails.stripeAccount && memberDetails.stripeAccount.stripeUserId) {
              let taxAmount = (data.amount * charges) / 100;
              let amount = (data.amount - taxAmount);
              let chargeAmount = Math.round(amount * 100);
              const tranferDetails = await stripe.transfers.create({
                amount: chargeAmount,
                currency: 'aud',
                destination: memberDetails.stripeAccount.stripeUserId,
                source_transaction: escroDetails.chargeId,
                transfer_group: "ORDER_95"
              })

              if (tranferDetails && tranferDetails.id) {
                let paymentObject = {
                  amount: data.amount,
                  transferAmount: amount,
                  textAmount: taxAmount,
                  byMemberId: contractDetails.clientId,
                  memberId: memberDetails.id,
                  jobId: escroDetails.jobId,
                  contractId: data.contractId,
                  transactionId: tranferDetails.id,
                  state: "success",
                }
                await this.escroRepository.updateById(escroDetails.id, { isReleased: true });
                const payment = await this.paymentRepository.create(paymentObject);
                return payment;
              } else {
                throw new HttpErrors.NotFound("Trasication failed")
              }
            } else {
              throw new HttpErrors.NotFound("stripe user id is required")
            }
          })
        } else {
          throw new HttpErrors.NotFound("cahrgeId is requred!")
        }
      } else {
        throw new HttpErrors.NotFound('amount should be greater then zero.');
      }
    } else {
      throw new HttpErrors.NotFound("Request data is missing")
    }
  }

  @get('/payments/paymentReportsByResource', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'payments model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Payment) } },
      },
    },
  })
  @authenticate('jwt')
  async paymentReportsByResource(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
  ): Promise<any> {
    let refundPayment: Array<AnyObject> = [];
    let payJobWork: Array<AnyObject> = [];
    let onProgressContract: Array<AnyObject> = [];
    const payments = await this.paymentRepository.find({
      where: {
        memberId: currentUser[securityId],
        state: { inq: ["success", "refunded"] }
      }, fields: { id: true, memberId: true, jobId: true, contractId: true, amount: true, created: true, state: true, refunded: true, jobWorkId: true },
      order: ['created DESC'],
      include: [
        { relation: "job" },
        { relation: "contract" }
      ]
    });
    let jobWorkIds = _.map(payments, v => v.jobWorkId)
    return await Promise.all([
      this.paymentRepository.find({
        where: {
          memberId: currentUser[securityId],
          state: "pending"
        },
        order: ['created DESC'],
        include: [
          { relation: "job" }
        ]
      }),
      this.jobworkRepository.find({
        where: {
          id: { inq: jobWorkIds }
        }
      })
    ]).then(async (res: any) => {

      let contractProgress = res && res[0] || [];
      let pendingJobWorkIds = _.map(contractProgress, v => v.jobWorkId)

      let jobworks = res && res[1] && res[1].length && _.groupBy(res[1], function (v: any) { return v.id })

      _.forEach(payments, function (val: any) {
        if (val && val.state && val.state === 'refunded') {
          refundPayment.push(val)
        }
        let obj: AnyObject = Object.assign({}, val);
        obj.jobWorkDetails = jobworks && jobworks[val.jobWorkId!] && Object.assign({}, jobworks[val.jobWorkId!][0]) || {};
        payJobWork.push(obj)
      })

      let pendingJobWork = await this.jobworkRepository.find({
        where: {
          id: { inq: pendingJobWorkIds }
        }
      })

      let jobworksDetail = pendingJobWork && pendingJobWork && pendingJobWork.length && _.groupBy(pendingJobWork, function (v: any) { return v.id })
      _.forEach(contractProgress, function (val: any) {
        let obj: AnyObject = Object.assign({}, val);
        obj.jobWorkDetails = jobworksDetail && jobworksDetail[val.jobWorkId!] && Object.assign({}, jobworksDetail[val.jobWorkId!][0]) || {};
        onProgressContract.push(obj)
      })
      let earnpayment = _.sumBy(payments, v => v.amount);
      let refund = _.sumBy(refundPayment, v => v.amount);

      let paymentData = {
        totalEarnPayment: (earnpayment - refund),
        contractProgressCount: contractProgress && contractProgress.length || 0,
        availableBlance: (earnpayment - refund),
        earnPaymentData: payJobWork && payJobWork.length && payJobWork || [],
        contractProgressData: onProgressContract && onProgressContract.length && onProgressContract || []
      }
      return paymentData;
    })
  }

  @get('/payments/paymentTransferByClient', {
    responses: {
      '200': {
        description: 'payments model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Payment) } },
      },
    },
  })
  async paymentTransferByClient(
  ): Promise<any> {
    let reportDetails: Array<AnyObject> = [];
    const siteOption = common.getSiteOptions();
    const stripe = new Stripe(siteOption.stripe.secretKey);
    const charges = siteOption.charges.resourceCharge;

    const paymentDetails = await this.paymentRepository.find({
      where: {
        state: "pending"
      }
    });

    if (paymentDetails && paymentDetails.length) {

      const contractId = _.map(paymentDetails, v => v.contractId);
      const jobIds = _.map(paymentDetails, w => w.jobId);
      return await Promise.all([
        this.contractRepository.find({
          where: {
            id: { inq: contractId }
          }
        })
      ]).then(async (res: any) => {

        let contractData = res && res[0] || []
        if (contractData && contractData.length) {

          const disputeContractsId = _.map(contractData, v => v.id)
          const resourceId = _.map(contractData, v => v.resourceId)
          return await Promise.all([
            this.disputeRepository.find({
              where: {
                contractId: { inq: disputeContractsId },
                state: "active"
              }
            }),
            this.memberRepository.find({
              where: {
                id: { inq: resourceId }
              }, fields: { id: true, stripeAccount: true, fcmToken: true }
            }),
            this.escroRepository.find({
              where: {
                jobId: { inq: jobIds },
                state: "success"
              }
            })
          ]).then(async (ress: any) => {

            let contracts = contractData && contractData.length && _.groupBy(contractData, w => w.id);
            let memberData = ress && ress[1] && ress[1].length && _.groupBy(ress[1], v => v.id);
            let escrowData = ress && ress[2] && ress[2].length && _.groupBy(ress[2], w => w.jobId)
            let disputeData = ress && ress[0] && ress[0].length && _.groupBy(ress[0], v => v.contractId);
            if (ress && ress[0] && ress[0].length > 0) {

              _.forEach(paymentDetails, function (val: any) {
                let obj: AnyObject = Object.assign({}, val);
                obj.contractDetails = contracts && contracts[val.contractId!] && Object.assign({}, contracts[val.contractId!][0]) || {};
                obj.memberDetails = memberData && memberData[val.memberId!] && Object.assign({}, memberData[val.memberId!][0]) || {};
                obj.escrowDetails = escrowData && escrowData[val.jobId!] && Object.assign({}, escrowData[val.jobId!][0]) || {};
                obj.disputeDetails = disputeData && disputeData[val.contractId!] && Object.assign({}, disputeData[val.contractId!][0]) || {};
                reportDetails.push(obj);
              })
              let _this = this;
              _.forEach(reportDetails, async function (val: any) {
                if (val && val.disputeDetails && val.disputeDetails.state === "active") {

                  let paymentDate = moment().unix();
                  let pTransferDate = moment(val.created).add(+14, 'days').unix();
                  if (paymentDate > pTransferDate) {
                    let data = {
                      status: 1,
                      jobId: val && val.disputeDetails && val.disputeDetails.jobId || "",
                      message: "Dispute autoClose after 14 Day's complete"
                    }
                    await _this.cronlogRepository.create(data);
                    await _this.disputeRepository.updateById(val.disputeDetails.id, { state: "autoClose" });
                    return null;
                  } else {
                    let data = {
                      status: 1,
                      jobId: val && val.disputeDetails && val.disputeDetails.jobId || "",
                      message: "Payment can't transfer because dispute is active"
                    }
                    await _this.cronlogRepository.create(data);
                  }
                } else {
                  let paymentDate = moment().unix();
                  let pTransferDate = moment(val.created).add(+3, 'days').unix();

                  if (paymentDate >= pTransferDate) {
                    // if (paymentDate <= pTransferDate) {
                    if (val && val.memberDetails && val.memberDetails.stripeAccount && val.memberDetails.stripeAccount.stripeUserId) {
                      if (val && val.escrowDetails && val.escrowDetails.chargeId) {

                        let taxAmount = (val.amount * charges) / 100;
                        let amount = (val.amount - taxAmount);
                        let chargeAmount = Math.round(amount * 100);

                        const tranferDetails = await stripe.transfers.create({
                          amount: chargeAmount,
                          currency: 'aud',
                          destination: val && val.memberDetails && val.memberDetails.stripeAccount && val.memberDetails.stripeAccount.stripeUserId || "",
                          source_transaction: val && val.escrowDetails && val.escrowDetails.chargeId || "",
                          transfer_group: "ORDER_95"
                        })

                        if (tranferDetails && tranferDetails.id) {
                          let paymentObject = {
                            transferAmount: amount,
                            textAmount: taxAmount,
                            byMemberId: val.contractDetails.clientId,
                            memberId: val.contractDetails.resourceId,
                            jobId: val.jobId,
                            contractId: val.contractId,
                            transactionId: tranferDetails.id,
                            state: "success",
                          }
                          //await _this.escroRepository.updateById(val.escroDetails.id, { isReleased: true });
                          if (val && val.memberDetails && val.memberDetails.fcmToken) {
                            var message = {
                              to: val.memberDetails.fcmToken,
                              data: {
                                name: val.memberDetails.name,
                                memberId: val.memberDetails.id,
                                type: 'paymentDetail'
                              },
                              notification: {
                                title: 'welcome Hoobie',
                                body: "You have recieved Payment!",
                                priority: "high",
                                sound: "default",
                                vibrate: true,
                              }
                            };
                            await _this.fcmServices.sendNotification({ message: message })
                          }
                          await _this.paymentRepository.updateById(val.id, paymentObject);
                          let data = {
                            status: 1,
                            jobId: val.jobId,
                            message: "Payment Transfer Sucessfully"
                          }
                          await _this.cronlogRepository.create(data);
                          return paymentObject;
                        } else {
                          throw new HttpErrors.NotFound("Transition failed")
                        }
                      }
                    }
                  } else {
                    let data = {
                      status: 1,
                      jobId: val && val.contractDetails && val.contractDetails.jobId || "",
                      message: "Payment Transfer failed"
                    }
                    await _this.cronlogRepository.create(data);
                  }
                }
              })

            } else {

              _.forEach(paymentDetails, function (val: any) {
                let obj: AnyObject = Object.assign({}, val);
                obj.contractDetails = contracts && contracts[val.contractId!] && Object.assign({}, contracts[val.contractId!][0]) || {};
                obj.memberDetails = memberData && memberData[val.memberId!] && Object.assign({}, memberData[val.memberId!][0]) || {};
                obj.escrowDetails = escrowData && escrowData[val.jobId!] && Object.assign({}, escrowData[val.jobId!][0]) || {};
                reportDetails.push(obj);
              })
              if (reportDetails && reportDetails.length > 0) {
                let _this = this;
                _.forEach(reportDetails, async function (val: any) {

                  let paymentDate = moment().unix();
                  let pTransferDate = moment(val.created).add(+3, 'days').unix();
                  if (paymentDate >= pTransferDate) {
                    // if (paymentDate <= pTransferDate) {

                    if (val && val.memberDetails && val.memberDetails.stripeAccount && val.memberDetails.stripeAccount.stripeUserId) {

                      if (val && val.escrowDetails && val.escrowDetails.chargeId) {

                        let taxAmount = (Number(val.amount) * Number(charges)) / 100;
                        let amount = (val.amount - taxAmount);
                        let chargeAmount = Math.round(amount * 100);

                        const tranferDetails = await stripe.transfers.create({
                          amount: chargeAmount,
                          currency: 'aud',
                          destination: val.memberDetails.stripeAccount.stripeUserId,
                          source_transaction: val.escrowDetails.chargeId,
                          transfer_group: "ORDER_95"
                        })

                        if (tranferDetails && tranferDetails.id) {
                          let paymentObject = {
                            transferAmount: amount,
                            textAmount: taxAmount,
                            byMemberId: val.contractDetails.clientId,
                            memberId: val.contractDetails.resourceId,
                            jobId: val.jobId,
                            contractId: val.contractId,
                            transactionId: tranferDetails.id,
                            state: "success",
                          }
                          //await _this.escroRepository.updateById(val.escrowDetails.id, { isReleased: true });
                          if (val && val.memberDetails && val.memberDetails.fcmToken) {
                            var message = {
                              to: val.memberDetails.fcmToken,
                              data: {
                                name: val.memberDetails.name,
                                memberId: val.memberDetails.id,
                                type: 'paymentDetail'
                              },
                              notification: {
                                title: 'welcome Hoobie',
                                body: "You have recieved Payment!",
                                priority: "high",
                                sound: "default",
                                vibrate: true,
                              }
                            };
                            await _this.fcmServices.sendNotification({ message: message })
                          }
                          const payment = await _this.paymentRepository.updateById(val.id, paymentObject);
                          let data = {
                            status: 1,
                            jobId: val.jobId,
                            message: "Payment Transfer Sucessfully"
                          }
                          await _this.cronlogRepository.create(data);
                          return payment;
                        } else {
                          throw new HttpErrors.NotFound("Trasication failed")
                        }
                      } else {
                        console.log("client charge id not valid");
                      }
                    } else {
                      console.log("stripe user account not valid");
                    }
                  } else {
                    let data = {
                      status: 1,
                      jobId: val.jobId,
                      message: "Payment Transfer failed"
                    }
                    await _this.cronlogRepository.create(data);
                  }
                })
              }
            }
          }).catch((err: any) => {
            throw new HttpErrors.InternalServerError('Something went wrong!')
          });
        } else {
          throw new HttpErrors.NotFound("Contracts not found")
        }
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      throw new HttpErrors.NotFound("Payment not on hold")
    }
  }
  @post('/payments/paymentRefundByResource', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'payments model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Payment) } },
      },
    },
  })
  @authenticate('jwt')
  async paymentRefundByResource(
    @requestBody() data: AnyObject
  ): Promise<any> {
    if (data && data.jobId && data.contractId) {
      const payments = await this.paymentRepository.findOne({
        where: {
          jobId: data.jobId,
          contractId: data.contractId,
          state: "pending"
        }
      });
      if (payments && payments.id) {

        return await Promise.all([
          this.paymentRepository.updateById(payments.id, { state: "refund" }),
          this.escroRepository.findOne({
            where: {
              memberId: payments.memberId,
              jobId: payments.jobId
            }
          })
        ]).then(async (res: any) => {
          let escroData = res && res[1] || {}
          if (escroData && escroData.id) {
            await this.escroRepository.updateById(escroData.id, { isReleased: true, isPending: false });
            return res[0]
          } else {
            return res[0]
          }
        })
      } else {
        throw new HttpErrors.NotFound("Payment not found");
      }
    } else {
      throw new HttpErrors.NotFound("Invalid requred data")
    }
  }

  @post('/payments/calculatePayments', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'payments model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Payment) } },
      },
    },
  })
  @authenticate('jwt')
  async calculatePayments(
    @requestBody() data: AnyObject
  ): Promise<any> {
    let resPayments: Array<AnyObject> = [];
    let query: AnyObject = { and: [] }
    let searchQuery: AnyObject = {}
    let jobId: Array<any> = [];
    let memberId: Array<any> = [];

    query.and.push({
      state: String(data.state)
    })
    if (data && data.name) {
      searchQuery = {
        name: new RegExp('.*' + data.name + '.*', 'i')
      }
      const members = await this.memberRepository.find({
        where: searchQuery
      });
      memberId = _.map(members, (w) => { return String(w.id) })
      if (memberId && memberId.length > 0) {
        query.and.push({
          memberId: { inq: memberId }
        })
      }
    }
    if (data && data.title) {
      searchQuery = {
        title: new RegExp('.*' + data.title + '.*', 'i')
      }
      const jobs = await this.jobRepository.find({
        where: searchQuery
      });
      jobId = _.map(jobs, (v) => { return String(v.id); });
      if (jobId && jobId.length) {
        query.and.push({
          jobId: { inq: jobId }
        })
      }
    }
    const payments = await this.paymentRepository.find({
      where: query,
      order: [data.order]
    });
    let memberIds = _.map(payments, v => v.memberId);
    let jobIds = _.map(payments, v => v.jobId);
    let contractIds = _.map(payments, v => v.contractId);
    let byMemberIds = _.map(payments, v => v.byMemberId);

    return await Promise.all([
      this.memberRepository.find({
        where: {
          id: { inq: memberIds }
        }, fields: { id: true, name: true }
      }),
      this.jobRepository.find({
        where: {
          id: { inq: jobIds }
        }, fields: { id: true, title: true }
      }),
      this.contractRepository.find({
        where: {
          id: { inq: contractIds }
        }
      }),
      this.memberRepository.find({
        where: {
          id: { inq: byMemberIds }
        }, fields: { id: true, name: true }
      })
    ]).then((res: any) => {
      if (payments && payments.length) {

        let toMembers = res && res[0] && res[0].length && _.groupBy(res[0], v => v.id)
        let contract = res && res[2] && res[2].length && _.groupBy(res[2], v => v.id)
        let jobs = res && res[1] && res[1].length && _.groupBy(res[1], v => v.id)
        let byMembers = res && res[3] && res[3].length && _.groupBy(res[3], v => v.id)

        _.forEach(payments, function (val: any) {
          var obj = Object.assign({}, val)
          obj.member = toMembers && toMembers[val.memberId] && toMembers[val.memberId][0] || {};
          obj.contract = contract && contract[val.contractId] && contract[val.contractId][0] || {};
          obj.job = jobs && jobs[val.jobId] && jobs[val.jobId][0] || {};
          obj.byMember = byMembers && byMembers[val.byMemberId] && byMembers[val.byMemberId][0] || {};
          resPayments.push(obj)
        })

        let totalCount = resPayments && resPayments.length || 0
        let totalTaxPayment = _.sumBy(resPayments, v => v.textAmount);
        let totalTransferAmount = _.sumBy(resPayments, w => w.transferAmount);
        let totalActuallAmount = _.sumBy(resPayments, v => v.amount);
        let start = data && data.skip || 0

        if (data && data.sorting === 'title') {
          resPayments = _.orderBy(resPayments, function (e) { return _.lowerCase(e.job.title) }, [data.sortBy]);
          resPayments = _.slice(resPayments, start, start + data.limit)

          let paymentsData = {
            totalCount: totalCount,
            totalTaxPayment: totalTaxPayment,
            totalTransferAmount: totalTransferAmount,
            totalActuallAmount: totalActuallAmount,
            data: resPayments
          }
          return paymentsData
        } else if (data && data.sorting === 'cname') {
          resPayments = _.orderBy(resPayments, function (e) { return _.lowerCase(e.member.name) }, [data.sortBy]);
          resPayments = _.slice(resPayments, start, start + data.limit)

          let paymentsData = {
            totalCount: totalCount,
            totalTaxPayment: totalTaxPayment,
            totalTransferAmount: totalTransferAmount,
            totalActuallAmount: totalActuallAmount,
            data: resPayments
          }
          return paymentsData
        } else {
          resPayments = _.slice(resPayments, start, start + data.limit)
          let paymentsData = {
            totalCount: totalCount,
            totalTaxPayment: totalTaxPayment,
            totalTransferAmount: totalTransferAmount,
            totalActuallAmount: totalActuallAmount,
            data: resPayments
          }
          return paymentsData
        }

      } else {
        throw new HttpErrors.NotFound("Payments not found")
      }
    }).catch(err => {
      throw new HttpErrors.Unauthorized("Something went wrong!")
    })
  }
  @get('/payments/getConnectAccountDetails', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'payments model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Payment) } },
      },
    },
  })
  @authenticate('jwt')
  async getConnectAccountDetails(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
  ): Promise<any> {
    const siteOption = common.getSiteOptions();
    const stripe = new Stripe(siteOption.stripe.secretKey);
    const memberDetails = await this.memberRepository.findOne({
      where: {
        id: currentUser[securityId]
      }, fields: { id: true, stripeCustomerId: true, stripeAccount: true },
    })
    if (memberDetails && memberDetails.stripeAccount && memberDetails.stripeAccount.stripeUserId) {
      const stripeConnectedDetails = await stripe.accounts.retrieve(
        memberDetails.stripeAccount.stripeUserId)
      if (stripeConnectedDetails) {
        let links = await stripe.accounts.createLoginLink(
          memberDetails.stripeAccount.stripeUserId
        );
        let stripeDetails = {
          loginLink: links,
          stripeAccountDetails: stripeConnectedDetails
        }
        return stripeDetails;
      } else {
        throw new HttpErrors.NotFound("Connected account not found")
      }
    } else {
      throw new HttpErrors.NotFound("Connected account not found")
    }
  }
}
