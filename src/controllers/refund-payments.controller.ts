import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { RefundPayments } from '../models';
import { RefundPaymentsRepository, MemberRepository, ContractRepository, EscrowRepository, PaymentRepository } from '../repositories';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate } from '@loopback/authentication';
import { SecurityBindings, UserProfile, securityId } from '@loopback/security';
import { inject } from '@loopback/core';
import * as Stripe from 'stripe';
import * as common from '../services/common';
import * as _ from 'lodash';
import * as  moment from 'moment'
export class RefundPaymentsController {
  constructor(
    @repository(RefundPaymentsRepository) public refundPaymentsRepository: RefundPaymentsRepository,
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @repository(ContractRepository) public contractRepository: ContractRepository,
    @repository(EscrowRepository) public escroRepository: EscrowRepository,
    @repository(PaymentRepository) public paymentRepository: PaymentRepository

  ) { }

  @post('/refund-payments', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'RefundPayments model instance',
        content: { 'application/json': { schema: getModelSchemaRef(RefundPayments) } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RefundPayments, { exclude: ['id'] }),
        },
      },
    })
    refundPayments: Omit<RefundPayments, 'id'>,
  ): Promise<RefundPayments> {
    return this.refundPaymentsRepository.create(refundPayments);
  }

  @get('/refund-payments/count', {
    responses: {
      '200': {
        description: 'RefundPayments model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(RefundPayments)) where?: Where<RefundPayments>,
  ): Promise<Count> {
    return this.refundPaymentsRepository.count(where);
  }

  @get('/refund-payments', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of RefundPayments model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(RefundPayments) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async find(
    @param.query.object('filter', getFilterSchemaFor(RefundPayments)) filter?: Filter<RefundPayments>,
  ): Promise<RefundPayments[]> {
    return this.refundPaymentsRepository.find(filter);
  }

  @patch('/refund-payments', {
    responses: {
      '200': {
        description: 'RefundPayments PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RefundPayments, { partial: true }),
        },
      },
    })
    refundPayments: RefundPayments,
    @param.query.object('where', getWhereSchemaFor(RefundPayments)) where?: Where<RefundPayments>,
  ): Promise<Count> {
    return this.refundPaymentsRepository.updateAll(refundPayments, where);
  }

  @get('/refund-payments/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'RefundPayments model instance',
        content: { 'application/json': { schema: getModelSchemaRef(RefundPayments) } },
      },
    },
  })
  @authenticate('jwt')
  async findById(@param.path.string('id') id: string): Promise<RefundPayments> {
    return this.refundPaymentsRepository.findById(id);
  }

  @patch('/refund-payments/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'RefundPayments PATCH success',
      },
    },
  })
  @authenticate('jwt')
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RefundPayments, { partial: true }),
        },
      },
    })
    refundPayments: RefundPayments,
  ): Promise<void> {
    await this.refundPaymentsRepository.updateById(id, refundPayments);
  }

  @put('/refund-payments/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'RefundPayments PUT success',
      },
    },
  })
  @authenticate('jwt')
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() refundPayments: RefundPayments,
  ): Promise<void> {
    await this.refundPaymentsRepository.replaceById(id, refundPayments);
  }

  /* @del('/refund-payments/{id}', {
    responses: {
      '204': {
        description: 'RefundPayments DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.refundPaymentsRepository.deleteById(id);
  } */
  @post('/refund-payments/paymentRefund', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'payments model instance',
        content: { 'application/json': { schema: getModelSchemaRef(RefundPayments) } },
      },
    },
  })
  @authenticate('jwt')
  async paymentRefund(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    const siteOption = common.getSiteOptions();
    const stripe = new Stripe(siteOption.stripe.secretKey);

    if (data && data.contractId) {
      if (data && data.amount && data.amount > 0) {
        const contractDetails = await this.contractRepository.findOne({
          where: {
            id: data.contractId
          }, fields: { id: true, resourceId: true, jobId: true, clientId: true, amount: true },
        })
        // console.log(contractDetails)
        if (contractDetails && contractDetails.clientId) {
          return await Promise.all([
            await this.memberRepository.findOne({
              where: {
                id: contractDetails.clientId
              }, fields: { id: true, stripeAccount: true }
            }),
            await this.escroRepository.findOne({
              where: {
                jobId: contractDetails.jobId
              }
            }),
            this.paymentRepository.findOne({
              where: {
                contractId: contractDetails.id
              }
            })
          ]).then(async (res: any) => {
            var memberDetails = res && res[0] || null
            var escroDetails = res && res[1] && res[1] || null
            var paymentDetails = res && res[2] || null
            if (memberDetails && memberDetails.id) {

              let chargeAmount = Math.round(data.amount * 100);

              const refundPayment = await stripe.refunds.create({
                amount: chargeAmount,
                charge: escroDetails.chargeId
              })

              if (refundPayment && refundPayment.id) {
                let paymentObject = {
                  amount: data.amount,
                  byMemberId: memberDetails.resourceId,
                  toMemberId: contractDetails.clientId,
                  jobId: escroDetails.jobId,
                  contractId: data.contractId,
                  transactionId: refundPayment.id,
                  state: refundPayment.status,
                  chargeId: escroDetails.chargeId
                }
                let refundDates = moment().toString();
                this.paymentRepository.updateById(paymentDetails.id, { state: "refunded", refunded: refundDates });
                this.escroRepository.updateById(escroDetails.id, { state: "refunded", refunded: refundDates });
                const refundPaymentData = await this.refundPaymentsRepository.create(paymentObject);
                return refundPaymentData;
              } else {
                throw new HttpErrors.NotFound("Trasication failed")
              }
            } else {
              throw new HttpErrors.NotFound("Member id is not found")
            }
          })
        } else {
          throw new HttpErrors.NotFound("Client id not found!")
        }
      } else {
        throw new HttpErrors.NotFound('amount should be greater then zero.');
      }
    } else {
      throw new HttpErrors.NotFound("Request data is missing")
    }
  }

}
