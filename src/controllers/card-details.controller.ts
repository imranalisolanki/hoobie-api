import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { CardDetails } from '../models';
import { CardDetailsRepository, MemberRepository } from '../repositories';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate } from '@loopback/authentication';
import { inject } from '@loopback/core';
import { SecurityBindings, UserProfile, securityId } from '@loopback/security';
import * as Stripe from 'stripe';
import * as common from '../services/common';
import * as _ from 'lodash';
import { ObjectID } from 'mongodb';

export class CardDetailsController {
  constructor(
    @repository(CardDetailsRepository) public cardDetailsRepository: CardDetailsRepository,
    @repository(MemberRepository) public memberRepository: MemberRepository,
  ) { }

  @post('/card-details', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'CardDetails model instance',
        content: { 'application/json': { schema: getModelSchemaRef(CardDetails) } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() card: AnyObject,
  ): Promise<CardDetails> {
    const siteOption = common.getSiteOptions();
    const stripe = new Stripe(siteOption.stripe.secretKey);
    let stripeCustomerId = null;

    const member = await this.memberRepository.findOne({
      where: {
        id: currentUser[securityId],
        status: 1
      },
      fields: { id: true, stripeCustomerId: true, email: true },
    });

    if (member && member.email) {
      stripeCustomerId = member.stripeCustomerId || null;
      const cardDetail: AnyObject = await stripe.tokens.retrieve(card.tokenId);

      if (cardDetail && cardDetail.card && cardDetail.card.id) {
        try {
          if (stripeCustomerId) {
            const stripeCustomer = await stripe.customers.retrieve(stripeCustomerId);

            if (stripeCustomer && stripeCustomer.id) {
              /* await stripe.customers.update(stripeCustomer.id, {
                source: card.tokenId
              }); */
              await stripe.customers.createSource(stripeCustomer.id, {
                source: card.tokenId
              });
            } else {
              const customer = await stripe.customers.create({
                source: card.tokenId,
                email: member.email
              });

              stripeCustomerId = customer && customer.id;
            }
          } else {
            const customer = await stripe.customers.create({
              source: card.tokenId,
              email: member.email
            });

            stripeCustomerId = customer && customer.id;
          }

          await this.memberRepository.updateById(currentUser[securityId], {
            stripeCustomerId: stripeCustomerId
          });

          let cardObj: Partial<CardDetails> = {
            memberId: currentUser[securityId],
            lastFour: cardDetail.card.last4,
            expMonth: cardDetail.card.exp_month,
            expYear: cardDetail.card.exp_year,
            cardToken: card.tokenId,
            cardId: cardDetail.card.id,
            cardType: cardDetail.card.brand,
          }

          return this.cardDetailsRepository.create(cardObj);
        } catch (err) {
          throw err;
        }
      } else {
        throw new HttpErrors.NotFound('No card details found for given token.');
      }
    } else {
      throw new HttpErrors.Unauthorized('Member not found');
    }
  }

  @get('/card-details/count', {
    responses: {
      '200': {
        description: 'CardDetails model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(CardDetails)) where?: Where<CardDetails>,
  ): Promise<Count> {
    return this.cardDetailsRepository.count(where);
  }

  @get('/card-details', {
    responses: {
      '200': {
        description: 'Array of CardDetails model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(CardDetails) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(CardDetails)) filter?: Filter<CardDetails>,
  ): Promise<CardDetails[]> {
    return this.cardDetailsRepository.find(filter);
  }

  @patch('/card-details', {
    responses: {
      '200': {
        description: 'CardDetails PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CardDetails, { partial: true }),
        },
      },
    })
    cardDetails: CardDetails,
    @param.query.object('where', getWhereSchemaFor(CardDetails)) where?: Where<CardDetails>,
  ): Promise<Count> {
    return this.cardDetailsRepository.updateAll(cardDetails, where);
  }


  @patch('/card-details/updateCard/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'CardDetails PATCH success',
      },
    },
  })

  @authenticate('jwt')
  async updateCard(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CardDetails, { partial: true }),
        },
      },
    })
    cardDetails: CardDetails,
  ): Promise<void> {
    const siteOption = common.getSiteOptions();
    const stripe = new Stripe(siteOption.stripe.secretKey);

    return await Promise.all([
      this.memberRepository.findOne({
        where: {
          id: currentUser[securityId],
          status: 1
        },
        fields: { id: true, stripeCustomerId: true, email: true },
      }),
      this.cardDetailsRepository.findById(id, {
        fields: { id: true, cardId: true, cardToken: true, isDefault: true }
      })
    ]).then(async (res: any) => {
      const member = res && res[0] || null;
      const card = res && res[1] || null;

      if (member && member.email && member.stripeCustomerId) {
        if (card && card.cardId) {
          try {
            await stripe.customers.updateSource(
              member.stripeCustomerId,
              card.cardId,
              {
                exp_month: Number(cardDetails.expMonth),
                exp_year: Number(cardDetails.expYear)
              }
            );
            Promise.all([
              this.cardDetailsRepository.updateById(card.id, {
                expMonth: cardDetails.expMonth,
                expYear: cardDetails.expYear
              })
            ]);
          } catch (err) {
            throw err;
          }
        } else {
          throw new HttpErrors.NotFound('No card details found for given id.');
        }
      } else {
        throw new HttpErrors.Unauthorized('Member not found');
      }
    }).catch((err: any) => {
      console.log(err);
      throw new HttpErrors.InternalServerError('Something went wrong!')
    });
  }

  @get('/card-details/{id}', {
    responses: {
      '200': {
        description: 'CardDetails model instance',
        content: { 'application/json': { schema: getModelSchemaRef(CardDetails) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<CardDetails> {
    return this.cardDetailsRepository.findById(id);
  }

  @patch('/card-details/{id}', {
    responses: {
      '204': {
        description: 'CardDetails PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CardDetails, { partial: true }),
        },
      },
    })
    cardDetails: CardDetails,
  ): Promise<void> {
    await this.cardDetailsRepository.updateById(id, cardDetails);
  }

  @put('/card-details/{id}', {
    responses: {
      '204': {
        description: 'CardDetails PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() cardDetails: CardDetails,
  ): Promise<void> {
    await this.cardDetailsRepository.replaceById(id, cardDetails);
  }

  @del('/card-details/{id}', {
    responses: {
      '204': {
        description: 'CardDetails DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.cardDetailsRepository.deleteById(id);
  }

  @patch('/set-default/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'CardDetails PATCH success',
      },
    },
  })
  @authenticate('jwt')
  async setDefault(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.path.string('id') id: string,
  ): Promise<void> {
    const siteOption = common.getSiteOptions();
    const stripe = new Stripe(siteOption.stripe.secretKey);

    return await Promise.all([
      this.memberRepository.findOne({
        where: {
          id: currentUser[securityId],
          status: 1
        },
        fields: { id: true, stripeCustomerId: true, email: true },
      }),
      this.cardDetailsRepository.findById(id, {
        fields: { id: true, cardId: true, cardToken: true, isDefault: true }
      })
    ]).then(async (res: any) => {
      const member = res && res[0] || null;
      const card = res && res[1] || null;

      if (member && member.email && member.stripeCustomerId) {
        if (card && card.cardId) {
          try {
            await stripe.customers.update(member.stripeCustomerId, {
              default_source: card.cardId
            });

            let cardCond: AnyObject = {
              memberId: currentUser[securityId],
              id: { neq: new ObjectID(card.id) }
            }

            Promise.all([
              this.cardDetailsRepository.updateAll({
                isDefault: false
              }, cardCond),
              this.cardDetailsRepository.updateById(card.id, {
                isDefault: true
              })
            ]);
          } catch (err) {
            throw err;
          }
        } else {
          throw new HttpErrors.NotFound('No card details found for given id.');
        }
      } else {
        throw new HttpErrors.Unauthorized('Member not found');
      }
    }).catch((err: any) => {
      console.log(err);
      throw new HttpErrors.InternalServerError('Something went wrong!')
    });
  }
}
