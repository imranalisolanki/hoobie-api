import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Review } from '../models';
import { ReviewRepository, ContractRepository, MemberRepository, JobRepository } from '../repositories';
import { inject } from '@loopback/core';
import { ControllerServiceBindings } from '../keys';
import { ControllerService } from '../services';
import * as _ from 'lodash';
import { authenticate } from '@loopback/authentication';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';

export class ReviewController {
  constructor(
    @repository(ReviewRepository) public reviewRepository: ReviewRepository,
    @repository(ContractRepository) public contractRepository: ContractRepository,
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @repository(JobRepository) public jobRepository: JobRepository,
    @inject(ControllerServiceBindings.CONTROLLER_SERVICE) public controllerService: ControllerService
  ) { }

  @post('/reviews', {
    responses: {
      '200': {
        description: 'Review model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Review) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Review, { exclude: ['id'] }),
        },
      },
    })
    review: Omit<Review, 'id'>,
  ): Promise<Review> {
    const reviewObj = await this.reviewRepository.create(review);

    if (reviewObj && reviewObj.toMemberId && reviewObj.memberType) {
      let dataObj: any = {
        memberId: reviewObj.toMemberId,
        memberType: reviewObj.memberType
      }
      this.controllerService.refreshAverageRating(dataObj);
    }

    return reviewObj;
  }

  @get('/reviews/count', {
    responses: {
      '200': {
        description: 'Review model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Review)) where?: Where<Review>,
  ): Promise<Count> {
    return this.reviewRepository.count(where);
  }

  @get('/reviews', {
    responses: {
      '200': {
        description: 'Array of Review model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Review) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Review)) filter?: Filter<Review>,
  ): Promise<Review[]> {
    return this.reviewRepository.find(filter);
  }

  @patch('/reviews', {
    responses: {
      '200': {
        description: 'Review PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Review, { partial: true }),
        },
      },
    })
    review: Review,
    @param.query.object('where', getWhereSchemaFor(Review)) where?: Where<Review>,
  ): Promise<Count> {
    return this.reviewRepository.updateAll(review, where);
  }

  @get('/reviews/{id}', {
    responses: {
      '200': {
        description: 'Review model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Review) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Review> {
    return this.reviewRepository.findById(id);
  }

  @patch('/reviews/{id}', {
    responses: {
      '204': {
        description: 'Review PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Review, { partial: true }),
        },
      },
    })
    review: Review,
  ): Promise<void> {
    await this.reviewRepository.updateById(id, review);

    const reviewObj = await this.reviewRepository.findById(id);

    if (reviewObj && reviewObj.toMemberId && reviewObj.memberType) {
      let dataObj: any = {
        memberId: reviewObj.toMemberId,
        memberType: reviewObj.memberType
      }
      this.controllerService.refreshAverageRating(dataObj);
    }
  }

  @put('/reviews/{id}', {
    responses: {
      '204': {
        description: 'Review PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() review: Review,
  ): Promise<void> {
    await this.reviewRepository.replaceById(id, review);
  }

  @del('/reviews/{id}', {
    responses: {
      '204': {
        description: 'Review DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    const reviewObj = await this.reviewRepository.findById(id);
    await this.reviewRepository.deleteById(id);

    if (reviewObj && reviewObj.toMemberId && reviewObj.memberType) {
      let dataObj: any = {
        memberId: reviewObj.toMemberId,
        memberType: reviewObj.memberType
      }
      this.controllerService.refreshAverageRating(dataObj);
    }
  }

  @get('/reviews/forJob/{jobId}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Review model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { type: 'object' } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async reviewsForJob(
    @param.path.string('jobId') jobId: string,
  ): Promise<any> {
    let resReviews: Array<AnyObject> = [];

    const contract = await this.contractRepository.findOne({
      where: {
        jobId: jobId
      },
      fields: { id: true, clientId: true, resourceId: true }
    });

    if (contract && contract.id) {
      return await Promise.all([
        this.reviewRepository.find({
          where: {
            contractId: contract.id
          },
          fields: { modified: false, contractId: false, jobId: false, status: false }
        }),
        this.memberRepository.find({
          where: {
            id: { inq: [contract.clientId, contract.resourceId] }
          },
          fields: { id: true, name: true, image: true }
        })
      ]).then((res: any) => {
        const memberGroup = res && res[1] && res[1].length && _.groupBy(res[1], function (v) { return v.id; });

        if (res && res[0] && res[0].length) {
          _.forEach(res[0], (val: Review) => {
            let obj: AnyObject = Object.assign({}, val);
            obj.member = memberGroup && memberGroup[val.byMemberId] && memberGroup[val.byMemberId][0] || {};
            resReviews.push(obj);
          });

          return resReviews;
        } else {
          throw new HttpErrors.NotFound('No reviews found for given job id.')
        }
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!');
      });
    } else {
      throw new HttpErrors.NotFound('No contract found for given job id.');
    }
  }

  @post('/reviews/reviewListingDetails', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of reviews model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Review) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async reviewListingDetails(
    @requestBody() data: AnyObject,
  ): Promise<any> {
    let resproposals: Array<AnyObject> = [];
    let toMemberId: Array<any> = [];
    let byMemberId: Array<any> = [];
    let contractId: Array<any> = [];
    let jobId: Array<any> = [];

    const reviewData = await this.reviewRepository.find({
      where: {
        status: 1
      },
      order: [data.order]
    })

    toMemberId = _.map(reviewData, v => v.toMemberId)
    byMemberId = _.map(reviewData, v => v.byMemberId)
    contractId = _.map(reviewData, v => v.contractId)
    jobId = _.map(reviewData, v => v.jobId)

    return await Promise.all([
      this.memberRepository.find({
        where: {
          id: { inq: toMemberId }
        }, fields: { id: true, name: true }
      }),
      this.memberRepository.find({
        where: {
          id: { inq: byMemberId }
        }, fields: { id: true, name: true }
      }),
      this.contractRepository.find({
        where: {
          id: { inq: contractId }
        }, fields: { id: true }
      }),
      this.jobRepository.find({
        where: {
          id: { inq: jobId }
        }, fields: { id: true, title: true }
      })
    ]).then((res: any) => {

      let tomember = res && res[0] && res[0].length && _.groupBy(res[0], v => v.id);
      let bymember = res && res[1] && res[1].length && _.groupBy(res[1], v => v.id);
      let contracts = res && res[2] && res[2].length && _.groupBy(res[2], v => v.id);
      let jobs = res && res[3] && res[3].length && _.groupBy(res[3], v => v.id);

      _.forEach(reviewData, function (val: any) {
        let obj = Object.assign({}, val);
        obj.job = jobs && jobs[val.jobId] && jobs[val.jobId][0] || {};
        obj.byMember = bymember && bymember[val.byMemberId] && bymember[val.byMemberId][0] || {};
        obj.toMember = tomember && tomember[val.toMemberId] && tomember[val.toMemberId][0] || {};
        obj.contract = contracts && contracts[val.contractId] && contracts[val.contractId][0] || {};
        resproposals.push(obj)
      })
      let start = data && data.skip || 0
      if (data && data.sorting === 'title') {
        resproposals = _.orderBy(resproposals, function (e) { return _.lowerCase(e.job.title) }, [data.sortBy]);
        resproposals = _.slice(resproposals, start, start + data.limit)
        return resproposals
      }
      else if (data && data.sorting === 'reviewer') {
        resproposals = _.orderBy(resproposals, function (e) { return _.lowerCase(e.toMember.name) }, [data.sortBy]);
        resproposals = _.slice(resproposals, start, start + data.limit)
        return resproposals;
      }
      else if (data && data.sorting === 'reviewBy') {
        resproposals = _.orderBy(resproposals, function (e) { return _.lowerCase(e.byMember.name) }, [data.sortBy]);
        resproposals = _.slice(resproposals, start, start + data.limit)
        return resproposals;
      } else {
        resproposals = _.slice(resproposals, start, start + data.limit)
        return resproposals
      }
      // return resproposals;
    }).catch(err => {
      throw new HttpErrors.Unauthorized("Something went wrong!")
    })
  }
}
