import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {CronLogs} from '../models';
import {CronLogsRepository} from '../repositories';

export class CronLogsController {
  constructor(
    @repository(CronLogsRepository)
    public cronLogsRepository : CronLogsRepository,
  ) {}

  @post('/cron-logs', {
    responses: {
      '200': {
        description: 'CronLogs model instance',
        content: {'application/json': {schema: getModelSchemaRef(CronLogs)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CronLogs, {exclude: ['id']}),
        },
      },
    })
    cronLogs: Omit<CronLogs, 'id'>,
  ): Promise<CronLogs> {
    return this.cronLogsRepository.create(cronLogs);
  }

  @get('/cron-logs/count', {
    responses: {
      '200': {
        description: 'CronLogs model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(CronLogs)) where?: Where<CronLogs>,
  ): Promise<Count> {
    return this.cronLogsRepository.count(where);
  }

  @get('/cron-logs', {
    responses: {
      '200': {
        description: 'Array of CronLogs model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(CronLogs)},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(CronLogs)) filter?: Filter<CronLogs>,
  ): Promise<CronLogs[]> {
    return this.cronLogsRepository.find(filter);
  }

  @patch('/cron-logs', {
    responses: {
      '200': {
        description: 'CronLogs PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CronLogs, {partial: true}),
        },
      },
    })
    cronLogs: CronLogs,
    @param.query.object('where', getWhereSchemaFor(CronLogs)) where?: Where<CronLogs>,
  ): Promise<Count> {
    return this.cronLogsRepository.updateAll(cronLogs, where);
  }

  @get('/cron-logs/{id}', {
    responses: {
      '200': {
        description: 'CronLogs model instance',
        content: {'application/json': {schema: getModelSchemaRef(CronLogs)}},
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<CronLogs> {
    return this.cronLogsRepository.findById(id);
  }

  @patch('/cron-logs/{id}', {
    responses: {
      '204': {
        description: 'CronLogs PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CronLogs, {partial: true}),
        },
      },
    })
    cronLogs: CronLogs,
  ): Promise<void> {
    await this.cronLogsRepository.updateById(id, cronLogs);
  }

  @put('/cron-logs/{id}', {
    responses: {
      '204': {
        description: 'CronLogs PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() cronLogs: CronLogs,
  ): Promise<void> {
    await this.cronLogsRepository.replaceById(id, cronLogs);
  }

  @del('/cron-logs/{id}', {
    responses: {
      '204': {
        description: 'CronLogs DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.cronLogsRepository.deleteById(id);
  }
}
