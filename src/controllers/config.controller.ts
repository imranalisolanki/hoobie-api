import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Config } from '../models';
import { ConfigRepository } from '../repositories';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate } from '@loopback/authentication';
import { securityId } from '@loopback/security';

export class ConfigController {
  constructor(
    @repository(ConfigRepository) public configRepository: ConfigRepository,
  ) { }

  @post('/configs', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Config model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Config) } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Config, { exclude: ['id'] }),
        },
      },
    })
    config: Omit<Config, 'id'>,
  ): Promise<Config> {
    return this.configRepository.create(config);
  }

  @get('/configs/count', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Config model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  @authenticate('jwt')
  async count(
    @param.query.object('where', getWhereSchemaFor(Config)) where?: Where<Config>,
  ): Promise<Count> {
    return this.configRepository.count(where);
  }

  @get('/configs', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Config model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Config) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async find(
    @param.query.object('filter', getFilterSchemaFor(Config)) filter?: Filter<Config>,
  ): Promise<Config[]> {
    return this.configRepository.find(filter);
  }

  /* @patch('/configs', {
    responses: {
      '200': {
        description: 'Config PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Config, {partial: true}),
        },
      },
    })
    config: Config,
    @param.query.object('where', getWhereSchemaFor(Config)) where?: Where<Config>,
  ): Promise<Count> {
    return this.configRepository.updateAll(config, where);
  } */

  /* @get('/configs/{id}', {
    responses: {
      '200': {
        description: 'Config model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Config) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Config> {
    return this.configRepository.findById(id);
  } */

  @patch('/configs/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Config PATCH success',
      },
    },
  })
  @authenticate('jwt')
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Config, { partial: true }),
        },
      },
    })
    config: Config,
  ): Promise<void> {
    await this.configRepository.updateById(id, config);
  }

  /* @put('/configs/{id}', {
    responses: {
      '204': {
        description: 'Config PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() config: Config,
  ): Promise<void> {
    await this.configRepository.replaceById(id, config);
  } */

  @del('/configs/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Config DELETE success',
      },
    },
  })
  @authenticate('jwt')
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.configRepository.deleteById(id);
  }
}
