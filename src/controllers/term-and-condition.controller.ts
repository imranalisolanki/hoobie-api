import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {TermAndCondition} from '../models';
import {TermAndConditionRepository} from '../repositories';

export class TermAndConditionController {
  constructor(
    @repository(TermAndConditionRepository)
    public termAndConditionRepository : TermAndConditionRepository,
  ) {}

  @post('/term-and-conditions', {
    responses: {
      '200': {
        description: 'TermAndCondition model instance',
        content: {'application/json': {schema: getModelSchemaRef(TermAndCondition)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TermAndCondition, {exclude: ['id']}),
        },
      },
    })
    termAndCondition: Omit<TermAndCondition, 'id'>,
  ): Promise<TermAndCondition> {
    return this.termAndConditionRepository.create(termAndCondition);
  }

  @get('/term-and-conditions/count', {
    responses: {
      '200': {
        description: 'TermAndCondition model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(TermAndCondition)) where?: Where<TermAndCondition>,
  ): Promise<Count> {
    return this.termAndConditionRepository.count(where);
  }

  @get('/term-and-conditions', {
    responses: {
      '200': {
        description: 'Array of TermAndCondition model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(TermAndCondition)},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(TermAndCondition)) filter?: Filter<TermAndCondition>,
  ): Promise<TermAndCondition[]> {
    return this.termAndConditionRepository.find(filter);
  }

  @patch('/term-and-conditions', {
    responses: {
      '200': {
        description: 'TermAndCondition PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TermAndCondition, {partial: true}),
        },
      },
    })
    termAndCondition: TermAndCondition,
    @param.query.object('where', getWhereSchemaFor(TermAndCondition)) where?: Where<TermAndCondition>,
  ): Promise<Count> {
    return this.termAndConditionRepository.updateAll(termAndCondition, where);
  }

  @get('/term-and-conditions/{id}', {
    responses: {
      '200': {
        description: 'TermAndCondition model instance',
        content: {'application/json': {schema: getModelSchemaRef(TermAndCondition)}},
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<TermAndCondition> {
    return this.termAndConditionRepository.findById(id);
  }

  @patch('/term-and-conditions/{id}', {
    responses: {
      '204': {
        description: 'TermAndCondition PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TermAndCondition, {partial: true}),
        },
      },
    })
    termAndCondition: TermAndCondition,
  ): Promise<void> {
    await this.termAndConditionRepository.updateById(id, termAndCondition);
  }

  @put('/term-and-conditions/{id}', {
    responses: {
      '204': {
        description: 'TermAndCondition PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() termAndCondition: TermAndCondition,
  ): Promise<void> {
    await this.termAndConditionRepository.replaceById(id, termAndCondition);
  }

  @del('/term-and-conditions/{id}', {
    responses: {
      '204': {
        description: 'TermAndCondition DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.termAndConditionRepository.deleteById(id);
  }
}
