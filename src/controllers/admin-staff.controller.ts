import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, RestBindings, } from '@loopback/rest';
import { AdminStaffs, Member } from '../models';
import { AdminStaffsRepository } from '../repositories';
import { CredentialsRequestBody, Credentials } from '../type-schema';
import { inject } from '@loopback/core';
import { securityId, SecurityBindings, UserProfile } from '@loopback/security';
import { UserServiceBindings, TokenServiceBindings, PasswordHasherBindings } from '../keys';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate, UserService } from '@loopback/authentication';
import { JWTService } from '../services/jwt-service';
import { PasswordHasher } from '../services/hash.password.bcryptjs';
import { validateCredentials, validatePassword } from '../services/validator';
import * as _ from 'lodash';

export class AdminStaffController {
  constructor(
    @repository(AdminStaffsRepository) public adminStaffsRepository: AdminStaffsRepository,
    @inject(UserServiceBindings.USER_SERVICE) public userService: UserService<Member, Credentials>,
    @inject(TokenServiceBindings.TOKEN_SERVICE) public jwtService: JWTService,
    @inject(PasswordHasherBindings.PASSWORD_HASHER) public passwordHasher: PasswordHasher,
  ) { }

  @post('/admin-staffs', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'AdminStaffs model instance',
        content: { 'application/json': { schema: getModelSchemaRef(AdminStaffs) } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AdminStaffs, { exclude: ['id'] }),
        },
      },
    })
    adminStaffs: Omit<AdminStaffs, 'id'>,
  ): Promise<AdminStaffs> {
    validateCredentials(_.pick(adminStaffs, ['email', 'password']));
    adminStaffs.password = await this.passwordHasher.hashPassword(adminStaffs.password || '123456');
    return this.adminStaffsRepository.create(adminStaffs);
  }

  @patch('/admin-staffs/change-password', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'admin-staffs Password change success',
      },
    },
  })
  @authenticate('jwt')
  async changePassword(
    @requestBody() data: AnyObject,
    @inject(SecurityBindings.USER) currentUser: UserProfile
  ): Promise<any> {
    if (!(data && data.currentPassword)) {
      throw new HttpErrors.UnprocessableEntity('Current Password is required!');
    } else if (!(data && data.password)) {
      throw new HttpErrors.UnprocessableEntity('New Password is required!');
    }

    const staff = await this.adminStaffsRepository.findById(currentUser[securityId]);

    if (staff && staff.id) {
      const isMatched = await this.passwordHasher.comparePassword(data.currentPassword, staff.password!);
      if (isMatched) {
        staff.password = await this.passwordHasher.hashPassword(data.password);

        return await this.adminStaffsRepository.updateById(staff.id, staff).then(async (rs: any) => {
          // this.response.status(200)
          return rs;
        }).catch((er: any) => {
          throw new HttpErrors.UnprocessableEntity(`Error in changing staff password`);
        });
      } else {
        throw new HttpErrors.Unauthorized('Current password doesn\'t matched.');
      }
    } else {
      throw new HttpErrors.NotFound(`User not found for Email ${currentUser.email}`);
    }
  }


  @post('/admin-staffs/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: { type: 'object', properties: { token: { type: 'string' } } }
          }
        }
      }
    }
  })
  async login(@requestBody(CredentialsRequestBody) credentials: Credentials): Promise<{ token: string }> {
    const invalidCredentialsError = 'Invalid email or password.';

    const getMember = await this.adminStaffsRepository.findOne({
      where: {
        email: credentials.email
      }
    });
    if (!getMember) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }
    const passwordMatched = await this.passwordHasher.comparePassword(credentials.password, getMember.password || '');

    if (!passwordMatched) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }
    const userProfile = {
      [securityId]: getMember.id || '',
      name: getMember.name || '',
      email: getMember.email || '',
    }
    const token = await this.jwtService.generateToken(userProfile);

    return { token };
  }


  @get('/admin-staffs/count', {
    responses: {
      '200': {
        description: 'AdminStaffs model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(AdminStaffs)) where?: Where<AdminStaffs>,
  ): Promise<Count> {
    return this.adminStaffsRepository.count(where);
  }

  @get('/admin-staffs', {
    responses: {
      '200': {
        description: 'Array of AdminStaffs model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(AdminStaffs) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(AdminStaffs)) filter?: Filter<AdminStaffs>,
  ): Promise<AdminStaffs[]> {
    return this.adminStaffsRepository.find(filter);
  }

  @patch('/admin-staffs', {
    responses: {
      '200': {
        description: 'AdminStaffs PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AdminStaffs, { partial: true }),
        },
      },
    })
    adminStaffs: AdminStaffs,
    @param.query.object('where', getWhereSchemaFor(AdminStaffs)) where?: Where<AdminStaffs>,
  ): Promise<Count> {
    return this.adminStaffsRepository.updateAll(adminStaffs, where);
  }

  @get('/admin-staffs/{id}', {
    responses: {
      '200': {
        description: 'AdminStaffs model instance',
        content: { 'application/json': { schema: getModelSchemaRef(AdminStaffs) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<AdminStaffs> {
    return this.adminStaffsRepository.findById(id);
  }

  @patch('/admin-staffs/{id}', {
    responses: {
      '204': {
        description: 'AdminStaffs PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AdminStaffs, { partial: true }),
        },
      },
    })
    adminStaffs: AdminStaffs,
  ): Promise<void> {
    await this.adminStaffsRepository.updateById(id, adminStaffs);
  }

  @put('/admin-staffs/{id}', {
    responses: {
      '204': {
        description: 'AdminStaffs PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() adminStaffs: AdminStaffs,
  ): Promise<void> {
    await this.adminStaffsRepository.replaceById(id, adminStaffs);
  }

  @del('/admin-staffs/{id}', {
    responses: {
      '204': {
        description: 'AdminStaffs DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.adminStaffsRepository.deleteById(id);
  }
}
