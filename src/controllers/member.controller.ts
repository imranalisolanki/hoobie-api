import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, RestBindings, Response, } from '@loopback/rest';
import { Member } from '../models';
import { MemberRepository, CategoryRepository, ReviewRepository, ContractRepository, PaymentRepository } from '../repositories';
import { authenticate, UserService } from '@loopback/authentication';
import { UserProfile, securityId, SecurityBindings } from '@loopback/security';
import { CredentialsRequestBody, Credentials } from '../type-schema';
import { inject } from '@loopback/core';
import { UserServiceBindings, TokenServiceBindings, EmailServiceBindings, PasswordHasherBindings, FCMServiceBindings } from '../keys';
import { EmailService, FCMService } from '../services';
import { validateCredentials, validatePassword } from '../services/validator';
import * as _ from 'lodash';
import * as assert from 'assert';
import { PasswordHasher } from '../services/hash.password.bcryptjs';
import { TokenService } from '../services/jwt-service';
import { ObjectID } from 'mongodb';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import * as common from '../services/common';
const siteOption = common.getSiteOptions();
var twilio = require('twilio')(siteOption.twilio.twilioAccountSid, siteOption.twilio.authToken);

export class MemberController {
  constructor(
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @repository(CategoryRepository) public categoryRepository: CategoryRepository,
    @repository(ReviewRepository) public reviewRepository: ReviewRepository,
    @repository(ContractRepository) public contractRepository: ContractRepository,
    @inject(UserServiceBindings.USER_SERVICE) public userService: UserService<Member, Credentials>,
    @inject(TokenServiceBindings.TOKEN_SERVICE) public jwtService: TokenService,
    @inject(EmailServiceBindings.MAIL_SERVICE) public emailService: EmailService,
    @inject(PasswordHasherBindings.PASSWORD_HASHER) public passwordHasher: PasswordHasher,
    @inject(RestBindings.Http.RESPONSE) protected response: Response,
    @repository(PaymentRepository) public paymentRepository: PaymentRepository,
    @inject(FCMServiceBindings.FCM_SERVICE) public fcmServices: FCMService
  ) { }

  @post('/members', {
    responses: {
      '200': {
        description: 'Member model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Member, { exclude: ['id'] }),
        },
      },
    })
    member: Omit<Member, 'id'>,
  ): Promise<Member> {
    validateCredentials(_.pick(member, ['email', 'password']));

    member.password = await this.passwordHasher.hashPassword(member.password || '123456');

    try {
      const savedUser = await this.memberRepository.create(member);

      let userProfile: UserProfile = Object.assign(
        { [securityId]: '', name: '', email: '' },
        { [securityId]: savedUser.id, name: savedUser.name, email: savedUser.email }
      );

      let resUser: any = Object.assign({}, savedUser);
      resUser.token = await this.jwtService.generateToken(userProfile);

      //  send push notification to create member
      if (savedUser && savedUser.fcmToken) {
        var message = {
          to: savedUser.fcmToken,
          data: {
            name: savedUser.name,
            memberId: savedUser.id,
            type: 'memberDetail'
          },
          notification: {
            title: 'welcome Hoobie',
            body: "Congratulations you have successfully registered!",
            priority: "high",
            sound: "default",
            vibrate: true,
          }
        };
        await this.fcmServices.sendNotification({ message: message })
      }
      return resUser;
    } catch (err) {
      if (err.code === 11000 && err.errmsg.includes('index: uniqueEmail')) {
        throw new HttpErrors.Conflict('Email already exist');
      } else {
        throw err;
      }
    }
  }

  @get('/members/count', {
    responses: {
      '200': {
        description: 'Member model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(@param.query.object('where', getWhereSchemaFor(Member)) where?: Where<Member>): Promise<Count> {
    return this.memberRepository.count(where);
  }

  @get('/members', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Member model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Member) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async find(@param.query.object('filter', getFilterSchemaFor(Member)) filter?: Filter<Member>): Promise<Member[]> {
    return this.memberRepository.find(filter);
  }

  @get('/members/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Member model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Member) } },
      },
    },
  })
  @authenticate('jwt')
  async findById(@param.path.string('id') id: string): Promise<Member> {
    return this.memberRepository.findById(id);
  }

  @patch('/members/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Member PATCH success',
      },
    },
  })
  @authenticate('jwt')
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Member, { partial: true }),
        },
      },
    })
    member: Member,
  ): Promise<void> {
    await this.memberRepository.updateById(id, member);
    const memberDetails = await this.memberRepository.findOne({
      where: {
        id: id
      }, fields: { id: true, name: true, fcmToken: true, isProfileComplete: true }
    });
    if (memberDetails && memberDetails.fcmToken) {
      var message = {
        to: memberDetails.fcmToken,
        data: {
          name: memberDetails.name,
          memberId: memberDetails.id,
          type: 'memberDetail'
        },
        notification: {
          body: (memberDetails && memberDetails.isProfileComplete) ? "Congratulations your profile is complete!" : "Your profile is incomplete, complete it to start finding new jobs! / new connections / new employees",
          priority: "high",
          sound: "default",
          vibrate: true,
        }
      };
      await this.fcmServices.sendNotification({ message: message })
    }
  }

  /* @del('/members/{id}', {
    responses: {
      '204': {
        description: 'Member DELETE success',
      },
    },
  })
  @authenticate('jwt')
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.memberRepository.deleteById(id);
  } */

  @get('/members/me', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'The current user profile',
        content: {
          'application/json': { schema: { type: 'AnyObject' } }
        },
      },
    }
  })
  @authenticate('jwt')
  async printCurrentUser(@inject(SecurityBindings.USER) currentUser: UserProfile): Promise<AnyObject | null> {
    const member = await this.memberRepository.findOne({
      where: {
        id: currentUser[securityId],
        status: 1
      },
      fields: { created: false, modified: false, verificationToken: false, status: false, password: false }
    });

    let resMember: AnyObject = Object.assign({}, member, { interests: [] });

    if (member && member.interestIds && member.interestIds.length) {
      const categoryList = await this.categoryRepository.find({
        where: {
          id: { inq: member.interestIds }
        },
        fields: { id: true, name: true, icon: true }
      });

      resMember.interests = _.cloneDeep(categoryList);
    }
    return resMember;
  }

  @post('/members/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: { type: 'object', properties: { token: { type: 'string' } } }
          }
        }
      }
    }
  })
  async login(@requestBody(CredentialsRequestBody) credentials: Credentials): Promise<{ token: string }> {
    const user = await this.userService.verifyCredentials(credentials);
    const userProfile = this.userService.convertToUserProfile(user);
    const token = await this.jwtService.generateToken(userProfile);

    return { token };
  }

  @post('/members/sendOTP', {
    responses: {
      '200': {
        description: 'Email send success',
        content: {
          'application/json': {
            schema: { type: 'object' }
          }
        }
      }
    }
  })
  async sendOtp(@requestBody() data: any): Promise<any> {
    if (data && data.email && data.otp) {
      const mailOptions = {
        to: data.email,
        slug: 'registration-otp',
        message: {
          otp: data.otp
        }
      };
      var body = data.otp + '  is the OTP your Hoobie account NEVER SHARE YOUR OTP WITH ANYONE Hoobie will never call or messsage to ask for the OTP.'
      Promise.all([
        this.emailService.sendMail(mailOptions),

        await twilio.messages.create({
          from: siteOption.twilio.TWILIO_PHONE_NO,
          to: data.mobile,
          body: body
        })
      ]).then(res => {
        if (res && res[0]) {
          this.response.status(200);
          return { message: `Successfully sent verification Token to ${data.email}` };
        } else if (res && res[1]) {
          this.response.status(200);
          return { message: `Successfully sent verification Token to ${data.mobile}` };
        } else {
          throw new HttpErrors.UnprocessableEntity(`Error in sending E-mail to ${data.email}`);
        }
      })

      // await this.emailService.sendMail(mailOptions).then((res: any) => {
      //   this.response.status(200);
      //   return { message: `Successfully sent verification Token to ${data.email}` };
      // }).catch(function (err: any) {
      //   throw new HttpErrors.UnprocessableEntity(`Error in sending E-mail to ${data.email}`);
      // });

      // if (data && data.mobile) {
      //   var body = data.otp + '  is the OTP your Hoobie account NEVER SHARE YOUR OTP WITH ANYONE Hoobie will never call or messsage to ask for the OTP.'
      //   await twilio.messages.create({
      //     from: siteOption.twilio.TWILIO_PHONE_NO,
      //     to: data.mobile,
      //     body: body
      //   });
      // }

    } else {
      throw new HttpErrors.UnprocessableEntity('Request parameters are missing.');
    }
  }

  @post('/members/socialLogin', {
    responses: {
      '200': {
        description: 'Member model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  async socialLogin(@requestBody() data: any): Promise<any> {
    if (!(data && (data.facebookId || data.twitterId || data.googleId))) {
      throw new HttpErrors.UnprocessableEntity('Social Id is required!');
    }

    let resUser: any;
    let memberObj: any;
    let isNewRegistration: boolean = false;
    let query: AnyObject = {}
    if (data && data.facebookId) {
      query = {
        'social.facebookId': data.facebookId
      }
      const memberDetails = await this.memberRepository.findOne({
        where: query
      })
      if (memberDetails && memberDetails.id) {
        memberObj = Object.assign({}, memberDetails);
      }
    } if (memberObj && memberObj.id) { }
    else {
      if (!(data && data.email)) {
        throw new HttpErrors.UnprocessableEntity('Email Id is required!');
      }
      const member = await this.memberRepository.findOne({
        where: {
          email: data.email,
          status: 1
        }
      });

      if (member && member.id) {
        if (member.social && (member.social.facebookId === data.facebookId || member.social.twitterId === data.twitterId || member.social.googleId === data.googleId)) {
          memberObj = Object.assign({}, member);
        } else {
          throw new HttpErrors.UnprocessableEntity(`Account already exists for Email ${data.email} without this Social enable`);
        }
      } else {
        let userObj = new Member();
        userObj.email = data.email;
        userObj.name = data.name;
        userObj.image = data.image;

        if (data.facebookId) {
          userObj.social = { facebookId: data.facebookId };
        } else if (data.twitterId) {
          userObj.social = { twitterId: data.twitterId };
        } else if (data.googleId) {
          userObj.social = { googleId: data.googleId };
        } else if (data.appleId) {
          userObj.social = { appleId: data.appleId };
        }
        memberObj = await this.memberRepository.create(userObj);
        isNewRegistration = true;
      }
    }
    let userProfile: UserProfile = Object.assign(
      { [securityId]: '', name: '', email: '' },
      { [securityId]: memberObj.id, name: memberObj.name, email: memberObj.email }
    );

    resUser = Object.assign({}, memberObj);
    resUser.token = await this.jwtService.generateToken(userProfile);
    resUser.isNewRegistration = isNewRegistration;

    return resUser;

  }

  @post('/members/reset', {
    responses: {
      '200': {
        description: 'Member model instance',
        content: {
          'application/json': {
            schema: { type: 'object', properties: { token: { type: 'string' } } }
          }
        },
      },
    },
  })
  async requestPasswordReset(@requestBody() data: AnyObject): Promise<any> {
    if (!(data && data.email)) {
      throw new HttpErrors.UnprocessableEntity('Email Id is required!');
    }

    const member = await this.memberRepository.findOne({
      where: {
        email: data.email,
        status: 1
      }
    });

    if (member && member.id) {
      const memberId: string = member.id;
      const otp = Math.floor(100000 + Math.random() * 900000);

      const mailOptions = {
        to: data.email,
        slug: 'verification-otp',
        message: {
          otp: data.otp || otp
        }
      };
      var body = data.otp + '  is the OTP your Hoobie account NEVER SHARE YOUR OTP WITH ANYONE Hoobie will never call or messsage to ask for the OTP.'

      return Promise.all([
        this.emailService.sendMail(mailOptions)
      ]).then(async (res: any) => {

        const messages = twilio.messages.create({
          from: siteOption.twilio.TWILIO_PHONE_NO,
          to: member && member.mobile || "123456",
          body: body
        })

        if (messages && messages === undefined && res && res[0]) {
          return await this.memberRepository.updateById(memberId, member).then(async (rs: any) => {
            let userProfile: UserProfile = Object.assign(
              { [securityId]: '', otp: '', email: '' },
              { [securityId]: member.id, otp: otp, email: member.email, id: member.id }
            );
            const token = await this.jwtService.generateResetToken(userProfile);
            return { token };
          }).catch((er: any) => {
            throw new HttpErrors.UnprocessableEntity(`Error in updating verification otp`);
          });
        } else {
          return await this.memberRepository.updateById(memberId, member).then(async (rs: any) => {
            let userProfile: UserProfile = Object.assign(
              { [securityId]: '', otp: '', email: '' },
              { [securityId]: member.id, otp: otp, email: member.email, id: member.id }
            );
            const token = await this.jwtService.generateResetToken(userProfile);
            return { token };
          }).catch((er: any) => {
            throw new HttpErrors.UnprocessableEntity(`Error in updating verification otp`);
          });
        }
      }).catch(err => {
        throw new HttpErrors.UnprocessableEntity("Something went wrong")
      })
    } else {
      throw new HttpErrors.NotFound(`User not found for Email ${data.email}`);
    }
  }

  @post('/members/reset-password', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Member model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  @authenticate('jwt')
  async resetPassword(
    @requestBody() data: AnyObject,
    @inject(SecurityBindings.USER) currentUser: UserProfile
  ): Promise<void> {
    if (!(data && data.password)) {
      throw new HttpErrors.UnprocessableEntity('Password is required!');
    }
    validatePassword(data.password);

    const member = await this.memberRepository.findOne({
      where: {
        email: currentUser.email
      }
    });

    if (member && member.id) {
      member.password = await this.passwordHasher.hashPassword(data.password);

      return await this.memberRepository.updateById(member.id, member).then(async (rs: any) => {
        this.response.status(200);
        return;
      }).catch((er: any) => {
        throw new HttpErrors.UnprocessableEntity(`Error in updating member password`);
      });
    } else {
      throw new HttpErrors.NotFound(`User not found for Email ${currentUser.email}`);
    }
  }

  @patch('/members/change-password', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Member Password change success',
      },
    },
  })
  @authenticate('jwt')
  async changePassword(
    @requestBody() data: AnyObject,
    @inject(SecurityBindings.USER) currentUser: UserProfile
  ): Promise<void> {
    if (!(data && data.currentPassword)) {
      throw new HttpErrors.UnprocessableEntity('Current Password is required!');
    } else if (!(data && data.password)) {
      throw new HttpErrors.UnprocessableEntity('New Password is required!');
    }

    const member = await this.memberRepository.findById(currentUser[securityId]);

    if (member && member.id) {
      const isMatched = await this.passwordHasher.comparePassword(data.currentPassword, member.password!);
      if (isMatched) {
        member.password = await this.passwordHasher.hashPassword(data.password);

        return await this.memberRepository.updateById(member.id, member).then(async (rs: any) => {
          this.response.status(200);
          return;
        }).catch((er: any) => {
          throw new HttpErrors.UnprocessableEntity(`Error in changing member password`);
        });
      } else {
        throw new HttpErrors.Unauthorized('Current password doesn\'t matched.');
      }
    } else {
      throw new HttpErrors.NotFound(`User not found for Email ${currentUser.email}`);
    }
  }

  @post('/members/resources', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Member model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async searchResources(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    let query: AnyObject = {
      status: 1,
      id: { nin: [currentUser[securityId]] }
    };

    if (data && data.text) {
      query.or = [
        { name: new RegExp('.*' + data.text + '.*', 'i') },
        { 'jobSeeker.title': new RegExp('.*' + data.text + '.*', 'i') },
        { 'jobSeeker.description': new RegExp('.*' + data.text + '.*', 'i') }
      ]
    }
    if (data && data.category) {
      query.interestIds = { in: [data.category] }
    }

    return await this.memberRepository.find({
      where: query,
      order: data.order || 'jobSeeker.rating DESC',
      skip: data.skip,
      limit: data.limit,
      fields: { id: true, name: true, jobSeeker: true, location: true, image: true, isVerified: true, isProfileComplete: true }
    });
  }

  @get('/members/public/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Member model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  @authenticate('jwt')
  async publicProfile(@param.path.string('id') id: string): Promise<AnyObject> {
    let refundPayment: Array<AnyObject> = [];
    const member = await this.memberRepository.findOne({
      where: {
        id: id,
        status: 1
      },
      fields: { id: true, name: true, jobSeeker: true, jobManager: true, image: true, interestIds: true, location: true, isVerified: true, created: true, address: true }
    });

    if (member && member.id) {
      let resMember: AnyObject = Object.assign({}, member, { interests: [] });

      if (!this.contractRepository.dataSource.connected) {
        await this.contractRepository.dataSource.connect();
      }
      const contractCollection = (this.contractRepository.dataSource.connector as any).collection('Contract');

      return await Promise.all([
        this.categoryRepository.find({
          where: {
            id: { inq: member.interestIds }
          },
          fields: { id: true, name: true, icon: true }
        }),
        contractCollection.aggregate([
          {
            $match: {
              resourceId: new ObjectID(String(member.id)),
              state: 'complete'
            }
          },
          {
            $group: {
              _id: '$resourceId',
              totalEarn: { $sum: "$amount" },
              totalComplete: { $sum: 1 }
            }
          }
        ]).get(),
        this.paymentRepository.find({
          where: {
            state: { inq: ["success", "refunded"] },
            memberId: member.id
          }
        }),
        this.contractRepository.find({
          where: {
            resourceId: member.id
          }
        })
      ]).then((res: any) => {

        _.forEach(res[2], function (val: any) {
          if (val && val.state && val.state === 'refunded') {
            refundPayment.push(val)
          }
        })

        const completeContract = _.filter(res[3], function (val: any) {
          return val.state === 'complete'
        })
        let sucessRate = (completeContract && completeContract.length) * 100 / (res && res[3].length)
        let refund = _.sumBy(refundPayment, v => v.amount);
        let earnPayment = _.sumBy(res[2], function (v: any) { return v.amount })
        let totalEarnAmount = (earnPayment - refund)
        resMember.successRate = sucessRate && sucessRate !== null && sucessRate || 0
        resMember.interests = res && res[0] && res[0].length && res[0] || [];
        resMember.totalEarning = (totalEarnAmount && totalEarnAmount > 0) ? totalEarnAmount : 0;
        resMember.jobsComplete = res && res[1] && res[1][0] && res[1][0].totalComplete || 0;

        return resMember;
      }).catch((err: any) => {
        console.debug(err);
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      throw new HttpErrors.NotFound('Member not found.');
    }
  }

  @get('/members/trusted-resources', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Member model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Member) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async trustedResouces(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
  ): Promise<Member[]> {

    let query: AnyObject = {
      status: 1,
      id: { nin: [currentUser[securityId]] },
      isVerified: true
    };

    let members: Member[] = await this.memberRepository.find({
      where: query,
      order: ['jobSeeker.rating DESC'],
      limit: 5,
      fields: { id: true, name: true, jobSeeker: true, image: true, location: true, isVerified: true }
    });

    return members;
  }


  @post('/members/getMembersList', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Member model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  @authenticate('jwt')
  async getMembersList(
    @requestBody() data: any)
    : Promise<any> {
    let query: AnyObject = { and: [] }
    query.and.push({
      status: { inq: [0, 1] }
    })
    if (data && data.name) {
      query.and.push({
        name: new RegExp('.*' + data.name + '.*', 'i')
      })
    }
    if (data && data.email) {
      query.and.push({
        email: new RegExp('.*' + data.email + '.*', 'i')
      })
    }
    if (data && data.type) {
      query.and.push({
        ['jobSeeker.title']: new RegExp('.*' + data.email + '.*', 'i')
      })
    }

    let memberList = await this.memberRepository.find({
      where: query,
      skip: data.skip,
      limit: data.limit,
      order: [data.order]
    })
    if (memberList && memberList.length) {
      return memberList;
    } else {
      throw new HttpErrors.NotFound("Members Not found!")
    }
  }
}
