import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Dispute } from '../models';
import { DisputeRepository, MemberRepository, SupportRepository, NotificationRepository } from '../repositories';
import * as _ from 'lodash';
import { inject } from '@loopback/core';
import { SecurityBindings, UserProfile, securityId } from '@loopback/security';
import { authenticate } from '@loopback/authentication';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { EmailService, FCMService } from '../services';
import { EmailServiceBindings, FCMServiceBindings } from '../keys';

export class DisputeController {
  constructor(
    @repository(DisputeRepository) public disputeRepository: DisputeRepository,
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @repository(SupportRepository) public supportRepository: SupportRepository,
    @repository(NotificationRepository) public notificationRepository: NotificationRepository,
    @inject(EmailServiceBindings.MAIL_SERVICE) public emailService: EmailService,
    @inject(FCMServiceBindings.FCM_SERVICE) public fcmServices: FCMService
  ) { }

  @post('/disputes', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Dispute model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Dispute) } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Dispute, { exclude: ['id'] }),
        },
      },
    })
    dispute: Omit<Dispute, 'id'>,
  ): Promise<any> {
    let memberIds: Array<any> = [];
    const disputeDetails = await this.disputeRepository.create(dispute);
    if (disputeDetails && disputeDetails.id) {
      if (disputeDetails && disputeDetails.memberType && disputeDetails.memberType === 'jobManager') {
        memberIds.push(disputeDetails.clientId, currentUser[securityId])
      } else if (disputeDetails && disputeDetails.memberType && disputeDetails.memberType === 'jobSeeker') {
        memberIds.push(disputeDetails.resourceId, currentUser[securityId])
      }

      // let memberId = (disputeDetails && disputeDetails.memberType && disputeDetails.memberType === 'jobManager') ? disputeDetails.resourceId : disputeDetails.clientId
      // memberIds.push(memberId, currentUser[securityId])
      const member = await this.memberRepository.find({
        where: {
          id: { inq: memberIds }
        }, fields: { id: true, name: true, fcmToken: true, jobSeeker: true, jobManager: true }
      });
      let _this = this;
      _.forEach(member, async function (val: any) {
        if (val && val.fcmToken) {
          var message = {
            to: val.fcmToken,
            data: {
              name: val.name,
              memberId: val.id,
              jobId: disputeDetails.jobId,
              type: 'disputeDetail',
              userType: (val && val.jobSeeker && val.jobSeeker.title) ? "jobSeeker" : "jobManager",
              notificationObject: { disputeid: disputeDetails.id }
            },
            notification: {
              title: 'Hoobie',
              body: (currentUser && currentUser[securityId] && currentUser[securityId] === val.id) ? "You have an open dispute request" : "you have received a dispute message",
              priority: "high",
              sound: "default",
              vibrate: true,
            }
          };

          await _this.fcmServices.sendNotification({ message: message });
          let notification = {
            title: (currentUser && currentUser[securityId] && currentUser[securityId] === val.id) ? "You have an open dispute request" : "you have received a dispute message",
            message: disputeDetails.description,
            memberId: val.id,
            type: "diputeDetail",
            userType: (val && val.jobSeeker && val.jobSeeker.title) ? "jobSeeker" : "jobManager",
            notificationObject: { disputeid: disputeDetails.id }
          }
          await _this.notificationRepository.create(notification)
        } else {
          throw new HttpErrors.NotFound("Invalid fcm Token");
        }
      })

      return disputeDetails;

    } else {
      throw new HttpErrors.InternalServerError("Internal server error")
    }
  }

  @get('/disputes/count', {
    responses: {
      '200': {
        description: 'Dispute model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Dispute)) where?: Where<Dispute>,
  ): Promise<Count> {
    return this.disputeRepository.count(where);
  }

  @get('/disputes', {
    responses: {
      '200': {
        description: 'Array of Dispute model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Dispute) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Dispute)) filter?: Filter<Dispute>,
  ): Promise<Dispute[]> {
    return await this.disputeRepository.find(filter);
  }

  @patch('/disputes', {
    responses: {
      '200': {
        description: 'Dispute PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Dispute, { partial: true }),
        },
      },
    })
    dispute: Dispute,
    @param.query.object('where', getWhereSchemaFor(Dispute)) where?: Where<Dispute>,
  ): Promise<Count> {
    return this.disputeRepository.updateAll(dispute, where);
  }

  @get('/disputes/{id}', {
    responses: {
      '200': {
        description: 'Dispute model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Dispute) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Dispute> {
    return this.disputeRepository.findById(id, {
      include: [
        { relation: 'resource' },
        { relation: 'client' },
      ]
    });
  }

  @patch('/disputes/{id}', {
    responses: {
      '204': {
        description: 'Dispute PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Dispute, { partial: true }),
        },
      },
    })
    dispute: Dispute,
  ): Promise<void> {
    await this.disputeRepository.updateById(id, dispute);
  }

  @put('/disputes/{id}', {
    responses: {
      '204': {
        description: 'Dispute PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() dispute: Dispute,
  ): Promise<void> {
    await this.disputeRepository.replaceById(id, dispute);
  }

  @del('/disputes/{id}', {
    responses: {
      '204': {
        description: 'Dispute DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.disputeRepository.deleteById(id);
  }

  @get('/disputes/list', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Dispute model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Dispute) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getDisputes(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.string('memberType') memberType?: string,
    @param.query.string('state') state?: string,
  ): Promise<any> {
    let resDesputes: Array<AnyObject> = [];

    let query: AnyObject = {
      state: state
    };

    if (memberType === 'jobManager') {
      query.clientId = currentUser[securityId]
    } else if (memberType === 'jobSeeker') {
      query.resourceId = currentUser[securityId]
    }

    const disputes = await this.disputeRepository.find({
      where: query,
      order: ['created DESC'],
      fields: { modified: false, status: false, }
    });

    if (disputes && disputes.length) {
      let memberIds: Array<any> = [];

      if (memberType === 'jobManager') {
        memberIds = _.map(disputes, v => v.resourceId);
      } else if (memberType === 'jobSeeker') {
        memberIds = _.map(disputes, v => v.clientId);
      }

      const members = await this.memberRepository.find({
        where: {
          id: { inq: memberIds }
        },
        fields: { id: true, name: true, image: true }
      });

      const memberGroup = members && members.length && _.groupBy(members, function (v) { return v.id; });

      _.forEach(disputes, (val: Dispute) => {
        let obj: AnyObject = Object.assign({}, val);
        let memberId = (memberType === 'jobManager') ? val.resourceId : val.clientId;
        obj.member = memberGroup && memberGroup[memberId] && memberGroup[memberId][0] || {};

        resDesputes.push(obj);
      });

      return resDesputes;
    } else {
      return [];
    }
  }

  @post('/disputes/SendMessageAndNotificatin', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of disputes model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Dispute) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async SendMessageAndNotificatin(
    @requestBody() data: AnyObject,
  ): Promise<any> {
    if (data && data.disputeId) {
      const disputeData = await this.disputeRepository.findOne({
        where: {
          id: data.disputeId
        }, fields: { id: true, clientId: true, resourceId: true }
      })

      if (disputeData && disputeData.id) {
        return await Promise.all([
          await this.memberRepository.findOne({
            where: {
              id: disputeData.resourceId
            }
          }),
          await this.memberRepository.findOne({
            where: {
              id: disputeData.clientId
            }
          })
        ]).then(async (res: any) => {
          let resourceMember = res && res[0] || {}
          let clientMember = res && res[1] || {}
          // if (resourceMember && resourceMember.fcmToken) {

          // }
          // if (clientMember && clientMember.fcmToken) {

          // }
          if (resourceMember && resourceMember.email) {
            const mailOptions = {
              to: resourceMember.email,
              slug: 'breeze-dispute',
              message: {
                message: data.message
              }
            };
            await this.emailService.sendMail(mailOptions)
          }
          if (clientMember && clientMember.email) {
            const mailOptions = {
              to: clientMember.email,
              slug: 'breeze-dispute',
              message: {
                message: data.message,
                name: clientMember.name
              }
            };
            await this.emailService.sendMail(mailOptions);
          }
          return disputeData;
        })
      } else {
        throw new HttpErrors.NotFound("Dispute not found!")
      }
    } else {
      throw new HttpErrors.NotFound("Dispute id Required!")

    }
  }

  @post('/disputes/disputeSupportList', {
    responses: {
      '200': {
        description: 'Array of disputes model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Dispute) },
          },
        },
      },
    },
  })
  async disputeSupportList(
    @requestBody() data: AnyObject,
  ): Promise<any> {
    let resNotification: Array<AnyObject> = [];
    let clientIds: Array<any> = []
    const disputes = await this.disputeRepository.find({
      order: [data.order]
    })
    let clientId = _.map(disputes, v => v.clientId);
    let resourceId = _.map(disputes, v => v.resourceId);
    clientId.concat(resourceId)
    // return false
    return await Promise.all([
      this.memberRepository.find({
        where: {
          id: { inq: clientId }
        }, fields: { id: true, name: true }
      })
    ]).then(async (res: any) => {

      let members = res && res[0] && res[0].length && _.groupBy(res[0], v => v.id);
      _.forEach(disputes, function (val: any) {
        resNotification.push({
          id: val.id,
          title: val.title,
          message: val.description,
          created: val.created,
          ticketNo: val.disputeId,
          member: members && members[val.clientId] && members[val.clientId][0] && members[val.clientId][0].name || "",
          type: 'Dispute'
        })
      })
      const supports = await this.supportRepository.find({
        order: [data.order],
        include: [
          { relation: "member" }
        ]
      })
      _.forEach(supports, function (val: any) {
        resNotification.push({
          id: val.id,
          title: val.title,
          message: val.description,
          created: val.created,
          ticketNo: val.ticketId,
          member: (val && val.member && val.member.name) ? val.member.name : "",
          type: 'Support'
        })
      })
      let start = data && data.skip || 0
      let disputeSupportCount = resNotification && resNotification.length || 0
      if (data && data.sorting === 'title') {
        resNotification = _.orderBy(resNotification, function (e) { return _.lowerCase(e.title) }, [data.sortBy]);
        resNotification = _.slice(resNotification, start, start + data.limit)

        let resDispute = {
          totalCount: disputeSupportCount,
          result: resNotification
        }
        return resDispute;
      } else if (data && data.sorting === 'type') {
        resNotification = _.orderBy(resNotification, function (e) { return _.lowerCase(e.type) }, [data.sortBy]);
        resNotification = _.slice(resNotification, start, start + data.limit)

        let resDispute = {
          totalCount: disputeSupportCount,
          result: resNotification
        }
        return resDispute;
      } else if (data && data.sorting === 'ticket') {
        resNotification = _.orderBy(resNotification, function (e) { return _.lowerCase(e.ticketNo) }, [data.sortBy]);
        resNotification = _.slice(resNotification, start, start + data.limit)

        let resDispute = {
          totalCount: disputeSupportCount,
          result: resNotification
        }
        return resDispute;
      } else if (data && data.sorting === 'message') {
        resNotification = _.orderBy(resNotification, function (e) { return _.lowerCase(e.message) }, [data.sortBy]);
        resNotification = _.slice(resNotification, start, start + data.limit)

        let resDispute = {
          totalCount: disputeSupportCount,
          result: resNotification
        }
        return resDispute;
      } else if (data && data.sorting === 'name') {
        resNotification = _.orderBy(resNotification, function (e) { return _.lowerCase(e.member) }, [data.sortBy]);
        resNotification = _.slice(resNotification, start, start + data.limit)

        let resDispute = {
          totalCount: disputeSupportCount,
          result: resNotification
        }
        return resDispute;
      } else if (data && data.sorting === 'created') {
        resNotification = _.orderBy(resNotification, function (e) { return _.lowerCase(e.created) }, [data.sortBy]);
        resNotification = _.slice(resNotification, start, start + data.limit)

        let resDispute = {
          totalCount: disputeSupportCount,
          result: resNotification
        }
        return resDispute;
      } else {
        resNotification = _.orderBy(resNotification, function (e) { return e.created }, ['desc']);
        resNotification = _.slice(resNotification, start, start + data.limit)

        let resDispute = {
          totalCount: disputeSupportCount,
          result: resNotification
        }
        return resDispute;
      }

    })

  }
}
