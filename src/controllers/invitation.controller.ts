import { Count, CountSchema, Filter, repository, Where, AnyObject, relation, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Invitation } from '../models';
import { InvitationRepository, ProposalRepository, JobRepository, MemberRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';
import { inject } from '@loopback/core';
import { SecurityBindings, UserProfile, securityId } from '@loopback/security';
import * as _ from 'lodash';
import { ObjectID } from 'mongodb';
import * as moment from 'moment';
import * as common from '../services/common';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { FCMServiceBindings } from '../keys';
import { FCMService } from '../services';
import { NotificationRepository } from '../repositories/notification.repository';
export class InvitationController {
  constructor(
    @repository(InvitationRepository) public invitationRepository: InvitationRepository,
    @repository(ProposalRepository) public proposalRepository: ProposalRepository,
    @repository(JobRepository) public jobRepository: JobRepository,
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @inject(FCMServiceBindings.FCM_SERVICE) public fcmServices: FCMService,
    @repository(NotificationRepository) public notificationRepository: NotificationRepository
  ) { }

  @post('/invitations', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Invitation model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Invitation) } },
      },
    },
  })
  @authenticate('jwt')
  async create(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Invitation, { exclude: ['id'] }),
        },
      },
    })
    invitation: Omit<Invitation, 'id'>,
  ): Promise<any> {
    const checkInvition = await this.invitationRepository.findOne({
      where: {
        jobId: invitation.jobId,
        resourceId: invitation.resourceId,
        clientId: invitation.clientId,
        state: { inq: ["decline", "active"] }
      },
      include: [
        { relation: "client" },
        { relation: "job" }
      ]
    })
    if (checkInvition && checkInvition.id) {
      if (checkInvition && checkInvition.state === "active") {
        const declineMember = await this.memberRepository.findOne({
          where: {
            id: invitation.resourceId
          }
        })
        if (declineMember && declineMember.fcmToken) {
          let memberName = checkInvition && checkInvition.client && checkInvition.client.name || ""
          let jobTitle = checkInvition && checkInvition.job && checkInvition.job.title || ""
          let message = {
            to: declineMember.fcmToken,
            data: {
              name: declineMember.name,
              memberId: declineMember.id,
              jobId: invitation.jobId,
              type: 'invitation',
              userType: (declineMember && declineMember.jobSeeker && declineMember.jobSeeker.title) ? "jobSeeker" : "jobManager",
              notificationObject: { invitationId: checkInvition.id }
            },
            notification: {
              title: 'Hoobie',
              body: memberName + " has recommended you for  " + jobTitle,
              priority: "high",
              sound: "default",
              vibrate: true,
            }
          };

          await this.fcmServices.sendNotification({ message: message });
        }
        let Error = new HttpErrors.UnprocessableEntity("You have already invited this person to this job!")
        Error.name = "ERROR"
        throw Error
      } else if (checkInvition && checkInvition.state === "decline") {

        let Err = new HttpErrors.UnprocessableEntity("Invitation could not be sent as this person has already declined your previous proposal for this job.")
        Err.name = "ERROR"
        throw Err;
      }
    } else {
      //return this.invitationRepository.create(invitation);
      return await Promise.all([
        await this.memberRepository.findOne({
          where: {
            id: currentUser[securityId]
          }
        }),
        this.invitationRepository.create(invitation)
      ]).then(async (res: any) => {

        let member = res && res[0] || null
        let invitation = res && res[1] || null
        if (member && member.id) {
          if (invitation && invitation.resourceId) {
            try {
              const jobDetail = await this.jobRepository.findOne({
                where: {
                  id: invitation.jobId
                }, fields: { title: true, id: true }
              })
              if (jobDetail && jobDetail.id) {
                const memberDetail = await this.memberRepository.findOne({
                  where: {
                    id: invitation.resourceId
                  }, fields: { name: true, fcmToken: true, id: true, jobSeeker: true, jobManager: true }
                });

                if (memberDetail && memberDetail.fcmToken) {

                  let message = {
                    to: memberDetail.fcmToken,
                    data: {
                      name: member.name,
                      memberId: member.id,
                      jobId: invitation.jobId,
                      type: 'invitation',
                      userType: (memberDetail && memberDetail.jobSeeker && memberDetail.jobSeeker.title) ? "jobSeeker" : "jobManager",
                      notificationObject: { invitationId: invitation.id }
                    },
                    notification: {
                      title: 'Hoobie',
                      body: member.name + " has recommended you for  " + jobDetail.title,
                      priority: "high",
                      sound: "default",
                      vibrate: true,
                    }
                  };
                  await this.fcmServices.sendNotification({ message: message });
                  let notification = {
                    title: "You have been invited for " + jobDetail.title,
                    message: "Job Invitation",
                    memberId: invitation.resourceId,
                    type: "invitation",
                    userType: (memberDetail && memberDetail.jobSeeker && memberDetail.jobSeeker.title) ? "jobSeeker" : "jobManager",
                    notificationObject: { invitationId: invitation.id }
                  }
                  await this.notificationRepository.create(notification)
                  return invitation;
                } else {
                  throw new HttpErrors.NotFound('fcmToken not found.');
                }
              } else {
                throw new HttpErrors.NotFound("JOb not found")
              }
            }
            catch (err) {
              console.log(err);
            }
          } else {
            throw new HttpErrors.NotFound('No member found for given id.');
          }
        } else {
          throw new HttpErrors.Unauthorized('Member not found');
        }
      }).catch((err: any) => {
        console.log(err);
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    }
  }

  @get('/invitations/count', {
    responses: {
      '200': {
        description: 'Invitation model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Invitation)) where?: Where<Invitation>,
  ): Promise<Count> {
    return this.invitationRepository.count(where);
  }

  @get('/invitations', {
    responses: {
      '200': {
        description: 'Array of Invitation model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Invitation) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Invitation)) filter?: Filter<Invitation>,
  ): Promise<Invitation[]> {
    return this.invitationRepository.find(filter);
  }

  @patch('/invitations', {
    responses: {
      '200': {
        description: 'Invitation PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Invitation, { partial: true }),
        },
      },
    })
    invitation: Invitation,
    @param.query.object('where', getWhereSchemaFor(Invitation)) where?: Where<Invitation>,
  ): Promise<Count> {
    return this.invitationRepository.updateAll(invitation, where);
  }

  @get('/invitations/{id}', {
    responses: {
      '200': {
        description: 'Invitation model instance',
        content: { 'application/json': { schema: { type: 'object' } } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<any> {
    const invitation = await this.invitationRepository.findById(id, {
      fields: { id: true, jobId: true, message: true, created: true, clientId: true, resourceId: true, state: true, status: true }
    });

    let resInvitation: AnyObject = Object.assign({}, invitation, { job: {}, member: {}, client: {} });

    if (invitation && invitation.jobId) {
      let memberIds: Array<any> = []
      memberIds.push(invitation.clientId, invitation.resourceId);
      // console.log(memberIds, '------------')
      return await Promise.all([
        this.jobRepository.findOne({
          where: {
            id: invitation.jobId
          },
          /* fields: { id: true, description: true, budget: true, time: true, title: true, created: true } */
        }),
        this.memberRepository.find({
          where: {
            id: { inq: memberIds }
          },
          fields: { id: true, name: true, image: true }
        }),
        this.proposalRepository.find({
          where: {
            jobId: invitation.jobId
          }, fields: { id: true, memberId: true }
        })
      ]).then((res: any) => {
        //console.log(res[1])
        resInvitation.job = res && res[0] ? _.cloneDeep(res[0]) : {};
        resInvitation.bids = res && res[2] ? _.cloneDeep(res[2].length) : 0;
        resInvitation.client = res && res[1] && res[1][0] ? _.cloneDeep(res[1][0]) : {};
        resInvitation.member = res && res[1] && res[1][1] ? _.cloneDeep(res[1][1]) : {};
        return resInvitation;
      }).catch((err: any) => {
        console.debug(err);
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else return resInvitation;
  }

  @patch('/invitations/{id}', {
    responses: {
      '204': {
        description: 'Invitation PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Invitation, { partial: true }),
        },
      },
    })
    invitation: Invitation,
  ): Promise<void> {
    await this.invitationRepository.updateById(id, invitation);
    if (invitation && invitation.state === 'decline') {
      const invitationDetail = await this.invitationRepository.findOne({
        where: {
          id: id
        }, include: [
          { relation: "client" }, { relation: "resource" }
        ]
      });
      if (invitationDetail && invitationDetail.client && invitationDetail.client.fcmToken) {
        let message = {
          to: invitationDetail.client && invitationDetail.client.fcmToken || "",
          data: {
            name: invitationDetail.resource && invitationDetail.resource.name,
            memberId: invitationDetail.resource && invitationDetail.resource.id,
            jobId: invitationDetail.jobId,
            type: "jobDetails",
            userType: "jobManager",
            notificationObject: { jobId: invitationDetail.jobId }
          },
          notification: {
            body: invitationDetail.resource && invitationDetail.resource.name + " has declined your Invitation!",
            priority: "high",
            sound: "default",
            vibrate: true,
          }
        };
        // console.log(message)
        await this.fcmServices.sendNotification({ message: message })
        let notification = {
          title: invitationDetail.resource && invitationDetail.resource.name + " has declined your Invitation!",
          message: "Invitation has been declined",
          memberId: invitationDetail.clientId,
          type: "jobDetails",
          userType: "jobManager",
          notificationObject: { jobId: invitationDetail.jobId }
        }
        await this.notificationRepository.create(notification)
      }
    }

  }

  @put('/invitations/{id}', {
    responses: {
      '204': {
        description: 'Invitation PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() invitation: Invitation,
  ): Promise<void> {
    await this.invitationRepository.replaceById(id, invitation);
  }

  @del('/invitations/{id}', {
    responses: {
      '204': {
        description: 'Invitation DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.invitationRepository.deleteById(id);
  }

  @get('/invitations/resource-invitation', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Invitation model instances',
        content: {
          'application/json': {
            schema: { type: 'array' },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async resourceInvitation(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.string('state') state?: string
  ): Promise<any> {
    let resInvitation: Array<AnyObject> = [];
    const siteOption = common.getSiteOptions();
    const archiveDate = moment().subtract(siteOption.archiveDays, 'd').toDate();

    let query: AnyObject = {
      status: 1,
      resourceId: currentUser[securityId],
      state: 'active'
    }

    if (state && state === 'archive') {
      query.created = { lte: archiveDate }
    } else {
      query.created = { gte: archiveDate }
    }

    const invitations = await this.invitationRepository.find({
      where: query,
      order: ['created DESC'],
      fields: { id: true, clientId: true, jobId: true, created: true }
    });

    if (invitations && invitations.length) {
      const jobIds: Array<any> = _.map(invitations, v => v.jobId!);
      const clientIds: Array<any> = _.map(invitations, v => v.clientId!);

      if (!this.proposalRepository.dataSource.connected) {
        await this.proposalRepository.dataSource.connect();
      }
      const proposalCollection = (this.proposalRepository.dataSource.connector as any).collection('Proposal');

      return await Promise.all([
        this.jobRepository.find({
          where: {
            id: { inq: _.cloneDeep(jobIds) }
          },
          /*  fields: { id: true, title: true, budget: true, time: true, } */
        }).then((res: any) => res),
        proposalCollection.aggregate([
          {
            $match: {
              jobId: { $in: _.cloneDeep(jobIds) },
            }
          },
          {
            $group: {
              _id: '$jobId',
              totalBid: { $sum: 1 }
            }
          }
        ]).get(),
        this.memberRepository.find({
          where: {
            id: { inq: _.cloneDeep(clientIds) }
          },
          fields: { id: true, name: true, image: true }
        }).then(res => res),
      ]).then((res: any) => {
        const jobGroup = res && res[0] && res[0].length && _.groupBy(res[0], function (v) { return v.id; });
        const proposalGroup = res && res[1] && res[1].length && _.groupBy(res[1], function (v) { return v._id; });
        const memberGroup = res && res[2] && res[2].length && _.groupBy(res[2], function (v) { return v.id; });

        _.forEach(invitations, function (val: any) {
          let obj: AnyObject = Object.assign({}, val);
          obj.job = jobGroup && jobGroup[val.jobId] && jobGroup[val.jobId][0] || {};
          obj.bids = proposalGroup && proposalGroup[val.jobId] && proposalGroup[val.jobId][0] && proposalGroup[val.jobId][0].totalBid || 0;
          obj.client = memberGroup && memberGroup[val.clientId] && memberGroup[val.clientId][0] || {};

          resInvitation.push(obj);
        });

        return resInvitation;
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

  @post('/invitations/searchInvitations', {
    responses: {
      '200': {
        description: 'Array of Invitations model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Invitation) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async searchInvitations(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    let resInvitation: Array<AnyObject> = [];
    let resInvitationsData: AnyObject = {}
    let jobIds: Array<any> = [];
    let memberId: Array<any> = [];
    let query: AnyObject = { and: [] }
    let jobQuery: AnyObject = {}
    query.and.push({
      status: 1
    })
    if (data) {
      if (data && data.title) {
        jobQuery = {
          title: new RegExp('.*' + data.title + '.*', 'i')
        }
        const jobs = await this.jobRepository.find({
          where: jobQuery
        });
        jobIds = _.map(jobs, (v) => { return String(v.id); });
        if (jobIds && jobIds.length) {
          query.and.push({
            jobId: { inq: jobIds }
          })
        }
      }
      if (data && data.name) {
        jobQuery = {
          name: new RegExp('.*' + data.name + '.*', 'i')
        }
        const members = await this.memberRepository.find({
          where: jobQuery
        });
        memberId = _.map(members, (w) => { return String(w.id) })
        if (memberId && memberId.length > 0) {
          query.and.push({
            resourceId: { inq: memberId }
          })
        }
      }
    }
    const Invitations = await this.invitationRepository.find({
      where: query,
      order: [data.order]
    });
    if (Invitations && Invitations.length) {
      let cJobIds = _.map(Invitations, v => { return v.jobId; });
      let cMemberIds = _.map(Invitations, v => { return v.resourceId; });
      let cClientIds = _.map(Invitations, v => { return v.clientId; });

      return await Promise.all([
        this.jobRepository.find({
          where: {
            id: { inq: cJobIds }
          }
        }).then(res => res),
        this.memberRepository.find({
          where: {
            id: { inq: cMemberIds }
          },
          fields: { id: true, name: true, image: true }
        }).then(res => res),
        this.memberRepository.find({
          where: {
            id: { inq: cClientIds }
          },
          fields: { id: true, name: true, image: true }
        }).then(res => res)
      ]).then((res: any) => {
        const jobGroup = res && res[0] && res[0].length && _.groupBy(res[0], function (v) { return v.id; });
        const memberGroup = res && res[1] && res[1].length && _.groupBy(res[1], function (v) { return v.id; });
        const clientGroup = res && res[2] && res[2].length && _.groupBy(res[2], function (v) { return v.id; });

        _.forEach(Invitations, (val: Invitation) => {
          let obj: AnyObject = Object.assign({}, val);
          obj.job = jobGroup && jobGroup[val.jobId] && jobGroup[val.jobId][0] || {};
          obj.resource = memberGroup && memberGroup[val.resourceId] && memberGroup[val.resourceId][0] || {};
          obj.client = clientGroup && clientGroup[val.clientId] && clientGroup[val.clientId][0] || {};

          resInvitation.push(obj);
        });
        let totalInvitationCount = resInvitation && resInvitation.length || 0
        let start = data && data.skip || 0
        if (data && data.sorting === 'title') {
          resInvitation = _.orderBy(resInvitation, function (e) { return _.lowerCase(e.job.title) }, [data.sortBy]);
          resInvitation = _.slice(resInvitation, start, start + data.limit)

          resInvitationsData = {
            totalCount: totalInvitationCount,
            resInvitation
          }
          return resInvitationsData;
        }
        else if (data && data.sorting === 'time') {
          resInvitation = _.orderBy(resInvitation, function (e) { return e.job.time }, [data.sortBy]);
          resInvitation = _.slice(resInvitation, start, start + data.limit)

          resInvitationsData = {
            totalCount: totalInvitationCount,
            resInvitation
          }
          return resInvitationsData;
        }
        else if (data && data.sorting === 'resourceName') {
          resInvitation = _.orderBy(resInvitation, function (e) { return _.lowerCase(e.resource.name) }, [data.sortBy]);
          resInvitation = _.slice(resInvitation, start, start + data.limit)
          resInvitationsData = {
            totalCount: totalInvitationCount,
            resInvitation
          }
          return resInvitationsData;
        }
        else if (data && data.sorting === 'clientName') {
          resInvitation = _.orderBy(resInvitation, function (e) { return _.lowerCase(e.client.name) }, [data.sortBy]);
          resInvitation = _.slice(resInvitation, start, start + data.limit)
          resInvitationsData = {
            totalCount: totalInvitationCount,
            resInvitation
          }
          return resInvitationsData;
        }
        else if (data && data.sorting === 'budget') {
          resInvitation = _.orderBy(resInvitation, function (e) { return e.job.budget }, [data.sortBy]);
          resInvitation = _.slice(resInvitation, start, start + data.limit)
          resInvitationsData = {
            totalCount: totalInvitationCount,
            resInvitation
          }
          return resInvitationsData;
        } else {
          resInvitation = _.slice(resInvitation, start, start + data.limit)
          resInvitationsData = {
            totalCount: totalInvitationCount,
            resInvitation
          }
          return resInvitationsData;
        }
        //return resInvitation;
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

}
