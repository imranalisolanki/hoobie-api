import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Contract, Review } from '../models';
import { ContractRepository, JobRepository, MemberRepository, ReviewRepository, ProposalRepository, PaymentRepository, NotificationRepository, EscrowRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';
import { inject } from '@loopback/core';
import { SecurityBindings, UserProfile, securityId } from '@loopback/security';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ControllerServiceBindings, FCMServiceBindings } from '../keys';
import { ControllerService, FCMService } from '../services';
import { promises } from 'dns';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';

export class ContractController {
  constructor(
    @repository(ContractRepository) public contractRepository: ContractRepository,
    @repository(JobRepository) public jobRepository: JobRepository,
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @repository(ReviewRepository) public reviewRepository: ReviewRepository,
    @repository(ProposalRepository) public proposalRepository: ProposalRepository,
    @inject(ControllerServiceBindings.CONTROLLER_SERVICE) public controllerService: ControllerService,
    @inject(FCMServiceBindings.FCM_SERVICE) public fcmServices: FCMService,
    @repository(PaymentRepository) public paymentRepository: PaymentRepository,
    @repository(EscrowRepository) public escroRepository: EscrowRepository,
    @repository(NotificationRepository) public notificationRepository: NotificationRepository
  ) { }

  @post('/contracts', {
    responses: {
      '200': {
        description: 'Contract model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Contract) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Contract, { exclude: ['id'] }),
        },
      },
    })
    contract: Omit<Contract, 'id'>,
  ): Promise<Contract> {
    return this.contractRepository.create(contract);
  }

  @get('/contracts/count', {
    responses: {
      '200': {
        description: 'Contract model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Contract)) where?: Where<Contract>,
  ): Promise<Count> {
    return this.contractRepository.count(where);
  }

  @get('/contracts', {
    responses: {
      '200': {
        description: 'Array of Contract model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Contract) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Contract)) filter?: Filter<Contract>,
  ): Promise<Contract[]> {
    return this.contractRepository.find(filter);
  }

  @patch('/contracts', {
    responses: {
      '200': {
        description: 'Contract PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Contract, { partial: true }),
        },
      },
    })
    contract: Contract,
    @param.query.object('where', getWhereSchemaFor(Contract)) where?: Where<Contract>,
  ): Promise<Count> {
    return this.contractRepository.updateAll(contract, where);
  }

  @get('/contracts/{id}', {
    responses: {
      '200': {
        description: 'Contract model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Contract) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<any> {
    return await this.contractRepository.findById(id);
  }

  @patch('/contracts/{id}', {
    responses: {
      '204': {
        description: 'Contract PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Contract, { partial: true }),
        },
      },
    })
    contract: Contract,
  ): Promise<void> {
    await this.contractRepository.updateById(id, contract);

    const contractDetail = await this.contractRepository.findById(id, {
      fields: { id: true, jobId: true, clientId: true, resourceId: true },
      include: [
        { relation: "job" }
      ]
    });

    if (contract && contract.resourceReleaseStatus && contract.resourceReleaseStatus === true) {

      return await Promise.all([
        this.memberRepository.findOne({
          where: {
            id: contractDetail.clientId
          }, fields: { id: true, name: true, fcmToken: true, jobSeeker: true, jobManager: true }
        })
      ]).then(async (res: any) => {
        let member = res && res[0] || {}
        if (member && member.fcmToken) {
          var message = {
            to: member.fcmToken,
            data: {
              name: member.name,
              memberId: member.id,
              jobId: contractDetail.jobId,
              type: 'Payment',
              userType: (member && member.jobSeeker && member.jobSeeker.title) ? "jobSeeker" : "jobManager",
              notificationObject: { contractId: contractDetail.id }
            },
            notification: {
              title: 'Hoobie',
              body: "You have received a payment request",
              priority: "high",
              sound: "default",
              vibrate: true,
            }
          };
          await this.fcmServices.sendNotification({ message: message })
          let notification = {
            title: "Received payment request",
            message: "You have received a payment request",
            memberId: member.id,
            type: "Payment",
            userType: "jobManager",
            notificationObject: { contractId: contractDetail.id }
          }
          await this.notificationRepository.create(notification)
        }
      })
    }

    if (contract && contract.state) {
      let stage = (contract && contract.state === 'complete') ? 'completed' : (contract && contract.state === 'cancel') ? 'canceled' : contract && contract.state
      return await Promise.all([
        this.memberRepository.findOne({
          where: {
            id: contractDetail.resourceId
          }, fields: { id: true, name: true, fcmToken: true, jobSeeker: true, jobManager: true }
        })
      ]).then(async (res: any) => {
        let member = res && res[0] || {}
        if (member && member.fcmToken) {
          var message = {
            to: member.fcmToken,
            data: {
              name: member.name,
              memberId: member.id,
              jobId: contractDetail.jobId,
              type: 'contractDetails',
              userType: "jobSeeker",
              notificationObject: { contractId: contractDetail.id }
            },
            notification: {
              title: 'Welcome Hoobie',
              body: (contractDetail && contractDetail.job && contractDetail.job.title) + " contract has been " + (stage),
              priority: "high",
              sound: "default",
              vibrate: true,
            }
          };
          await this.fcmServices.sendNotification({ message: message })
          let notification = {
            title: "Contract ha been " + (stage),
            message: (contractDetail && contractDetail.job && contractDetail.job.title) + " contract has been " + (stage),
            memberId: member.id,
            type: "contractDetails",
            userType: "jobSeeker",
            notificationObject: { contractId: contractDetail.id }
          }
          console.log(notification)
          await this.notificationRepository.create(notification)
        }
      })
    }
  }

  @get('/contracts/client-contracts', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Contract model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Contract) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async clientContractList(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.string('search') searchText?: string
  ): Promise<any[]> {
    let resContract: Array<AnyObject> = [];
    let jobIds: Array<any> = [];
    let query: AnyObject = { clientId: currentUser[securityId] }

    if (searchText) {
      let jobQuery: AnyObject = {
        memberId: currentUser[securityId],
        title: new RegExp('.*' + searchText + '.*', 'i')
      };

      const jobs = await this.jobRepository.find({
        where: jobQuery,
        fields: { id: true, title: true, budget: true }
      });

      jobIds = _.map(jobs, (v) => { return v.id; });
      query.jobId = { inq: jobIds }
    }

    const contracts = await this.contractRepository.find({
      where: query,
      order: ['created DESC'],
      fields: { id: true, jobId: true, resourceId: true, amount: true, time: true, state: true }
    });

    if (contracts && contracts.length) {
      let cJobIds = _.map(contracts, v => { return v.jobId; });
      let cMemberIds = _.map(contracts, v => { return v.resourceId; });

      return await Promise.all([
        this.jobRepository.find({
          where: {
            id: { inq: cJobIds }
          },
          fields: { id: true, title: true }
        }).then(res => res),
        this.memberRepository.find({
          where: {
            id: { inq: cMemberIds }
          },
          fields: { id: true, name: true, image: true, jobManager: true, jobSeeker: true, fcmToken: true }
        }).then(res => res)
      ]).then((res: any) => {
        const jobGroup = res && res[0] && res[0].length && _.groupBy(res[0], function (v) { return v.id; });
        const memberGroup = res && res[1] && res[1].length && _.groupBy(res[1], function (v) { return v.id; });

        _.forEach(contracts, (val: Contract) => {
          let obj: AnyObject = Object.assign({}, val);
          obj.job = jobGroup && jobGroup[val.jobId] && jobGroup[val.jobId][0] || {};
          obj.resource = memberGroup && memberGroup[val.resourceId] && memberGroup[val.resourceId][0] || {};

          resContract.push(obj);
        });

        return resContract;
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

  @get('/contracts/resource-contracts', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Array of Contract model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Contract) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async resourceContractList(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.query.string('search') searchText?: string
  ): Promise<any[]> {
    let resContract: Array<AnyObject> = [];
    let query: AnyObject = { resourceId: currentUser[securityId] }

    const contracts = await this.contractRepository.find({
      where: query,
      fields: { id: true, jobId: true, clientId: true, amount: true, time: true, state: true }
    });

    if (contracts && contracts.length) {
      let jobIds = _.map(contracts, (v) => { return v.jobId; });

      let jobQuery: AnyObject = {
        id: { inq: jobIds },
      };

      if (searchText) {
        jobQuery.title = new RegExp('.*' + searchText + '.*', 'i');
      }

      let memberIds = _.map(contracts, v => { return v.clientId; });

      return await Promise.all([
        this.jobRepository.find({
          where: jobQuery,
          fields: { id: true, title: true }
        }),
        this.memberRepository.find({
          where: {
            id: { inq: memberIds }
          },
          fields: { id: true, name: true, image: true }
        })
      ]).then((res: any) => {
        const jobGroup = res && res[0] && res[0].length && _.groupBy(res[0], function (v) { return v.id; });
        const memberGroup = res && res[1] && res[1].length && _.groupBy(res[1], function (v) { return v.id; });

        _.forEach(contracts, (val: Contract) => {
          let obj: AnyObject = Object.assign({}, val);
          obj.job = jobGroup && jobGroup[val.jobId] && jobGroup[val.jobId][0] || {};
          obj.client = memberGroup && memberGroup[val.clientId] && memberGroup[val.clientId][0] || {};

          if (jobGroup && jobGroup[val.jobId] && jobGroup[val.jobId][0]) {
            resContract.push(obj);
          }
        });

        return resContract;
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

  /* @post('/contracts/jobCompleteByResource', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Review model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Review) } },
      },
    },
  })
  @authenticate('jwt')
  async jobCompleteByResource(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Review, { exclude: ['id'] }),
        },
      },
    })
    review: Omit<Review, 'id'>,
  ): Promise<any> {
    const reviewObj = await this.reviewRepository.create(review);

    let contractObj: Partial<Contract> = {
      state: 'complete',
      ended: moment().toDate()
    }

    if (reviewObj && reviewObj.contractId) {
      const contract = await this.contractRepository.updateById(review.contractId, contractObj);

      let dataObj: any = {
        memberId: reviewObj.toMemberId,
        memberType: reviewObj.memberType
      }
      this.controllerService.refreshAverageRating(dataObj);

      return reviewObj;
    } else {
      throw new HttpErrors.UnprocessableEntity('unable to mark this job as completed.');
    }
  } */

  /* @post('/contracts/jobCompleteByClient', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Review model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Review) } },
      },
    },
  })
  @authenticate('jwt')
  async jobCompleteByClient(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Review, { exclude: ['id'] }),
        },
      },
    })
    review: Omit<Review, 'id'>,
  ): Promise<any> {
    const reviewObj = await this.reviewRepository.create(review);

    let contractObj: Partial<Contract> = {
      state: 'complete',
      ended: moment().toDate()
    }

    if (reviewObj && reviewObj.contractId) {
      const contract = await this.contractRepository.updateById(review.contractId, contractObj);

      let dataObj: any = {
        memberId: reviewObj.toMemberId,
        memberType: reviewObj.memberType
      }
      this.controllerService.refreshAverageRating(dataObj);

      return reviewObj;
    } else {
      throw new HttpErrors.UnprocessableEntity('unable to mark this job as completed.');
    }
  } */

  @patch('/contracts/end/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Review model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Review) } },
      },
    },
  })
  @authenticate('jwt')
  async endContract(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @param.path.string('id') id: string,
  ): Promise<any> {
    let contractObj: Partial<Contract> = {
      state: 'complete',
      ended: moment().toDate(),
      endById: currentUser[securityId]
    }
    const contractDetails = await this.contractRepository.findOne({
      where: { id: id },
      include: [
        { relation: "job" }
      ]
    })
    if (contractDetails && contractDetails.id) {
      const memberId = (contractDetails.resourceId === currentUser[securityId]) ? contractDetails.clientId : contractDetails.resourceId

      const memberDetails = await this.memberRepository.findOne({
        where: {
          id: contractDetails.clientId
        }, fields: { id: true, name: true, fcmToken: true }
      })

      if (memberDetails && memberDetails.fcmToken) {
        var message = {
          to: memberDetails.fcmToken,
          data: {
            name: memberDetails.name,
            memberId: memberDetails.id,
            type: 'contractDetail'
          },
          notification: {
            title: 'welcome Hoobie',
            body: "Your job " + (contractDetails && contractDetails.job && contractDetails.job.title) + " contract has been completed!",
            priority: "high",
            sound: "default",
            vibrate: true,
          }
        };

        await this.fcmServices.sendNotification({ message: message })
        let notification = {
          title: "Contract completed",
          message: "Your job " + (contractDetails && contractDetails.job && contractDetails.job.title) + " contract has been completed!",
          memberId: memberDetails && memberDetails.id,
          type: "contractDetails",
          userType: "jobManager",
          notificationObject: { contractId: contractDetails.id }
        }
        await this.notificationRepository.create(notification)
      }
      return await this.contractRepository.updateById(id, contractObj);

    }
  }

  @get('/contracts/resourceContractDetails/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Contract model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Contract) } },
      },
    },
  })
  @authenticate('jwt')
  async resourceContractDetails(@param.path.string('id') id: string): Promise<any> {
    const contract = await this.contractRepository.findById(id, {});

    if (contract && contract.id) {
      let resContract: AnyObject = Object.assign({}, contract, { job: {}, client: {} });
      return await Promise.all([
        await this.jobRepository.findById(contract.jobId, {
          /* fields: { id: true, title: true, location: true, description: true } */
        }),
        await this.memberRepository.findById(contract.clientId, {
          fields: { id: true, name: true, image: true, jobSeeker: true, location: true, isVerified: true, fcmToken: true }
        }),
        this.escroRepository.find({
          where: {
            jobId: contract.jobId
          }, fields: { amount: true }
        }),
        this.paymentRepository.find({
          where: {
            jobId: contract.jobId,
            state: { inq: ["success", 'pending'] }
          }, fields: { amount: true }
        })
      ]).then((res: any) => {
        let escroAmount = _.sumBy(res && res[2].length && res[2], function (vl: any) { return vl.amount })
        let paymentAmount = _.sumBy(res && res[3].length && res[3], function (vl: any) { return vl.amount })

        resContract.job = res && res[0] && _.cloneDeep(res[0]) || {};
        resContract.client = res && res[1] && _.cloneDeep(res[1]) || {};
        resContract.reminingEscroAmount = escroAmount - paymentAmount;
        return resContract;
      })

    } else {
      throw new HttpErrors.NotFound('No contract details found for given id.')
    }
  }

  @get('/contracts/clientContractDetails/{id}', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Contract model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Contract) } },
      },
    },
  })
  @authenticate('jwt')
  async clientContractDetails(@param.path.string('id') id: string): Promise<any> {
    const contract = await this.contractRepository.findById(id);

    if (contract && contract.id) {
      let resContract: AnyObject = Object.assign({}, contract, { job: {}, resource: {} });

      return await Promise.all([
        this.jobRepository.findById(contract.jobId, {
          /* fields: { id: true, title: true, location: true, description: true } */
        }),
        this.memberRepository.findById(contract.resourceId, {
          fields: { id: true, name: true, image: true, jobSeeker: true, location: true, isVerified: true, fcmToken: true }
        }),
        this.escroRepository.find({
          where: {
            jobId: contract.jobId
          }, fields: { amount: true }
        }),
        this.paymentRepository.find({
          where: {
            jobId: contract.jobId,
            state: { inq: ["success", 'pending'] }
          }, fields: { amount: true }
        })
      ]).then((res: any) => {
        let escroAmount = _.sumBy(res && res[2].length && res[2], function (vl: any) { return vl.amount })
        let paymentAmount = _.sumBy(res && res[3].length && res[3], function (vl: any) { return vl.amount })

        resContract.job = res && res[0] && _.cloneDeep(res[0]) || {};
        resContract.resource = res && res[1] && _.cloneDeep(res[1]) || {};
        resContract.reminingEscroAmount = escroAmount - paymentAmount;

        return resContract;
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      throw new HttpErrors.NotFound('No contract details found for given id.')
    }
  }

  /*   create contract API and close proposal API */
  @post('/contracts/createByProposal', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Contract model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Contract) } },
      },
    },
  })
  @authenticate('jwt')
  async createByProposal(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    const proposalData = await this.proposalRepository.findOne({
      where: { id: data.proposalId },
      include: [
        { relation: "job" }
      ]
    })
    if (proposalData && proposalData.id) {
      const contractObject = {
        amount: proposalData.amount,
        time: proposalData.time,
        resourceId: proposalData.memberId,
        clientId: currentUser[securityId],
        jobId: proposalData.jobId,
        proposalId: proposalData.id
      }
      const contractData = await this.contractRepository.findOne({
        where: {
          jobId: proposalData.jobId
        }
      })
      if (contractData && contractData.id) {
        throw new HttpErrors.NotFound("Contract Already Created")
      } else {
        const contResData = await this.contractRepository.create(contractObject)
        if (contResData && contResData.id) {
          let jobsStatus = {
            state: "close"
          }
          let updateProposalState = {
            state: "close"
          }
          Promise.all([
            this.jobRepository.updateById(proposalData.jobId, jobsStatus).then(res => res),
            this.proposalRepository.updateById(proposalData.id, updateProposalState).then(ress => ress)
          ])
          const member = await this.memberRepository.findOne({
            where: {
              id: proposalData.memberId
            }, fields: { id: true, name: true, fcmToken: true, jobManager: true, jobSeeker: true }
          });
          if (member && member.fcmToken) {
            var message = {
              to: member.fcmToken,
              data: {
                name: member.name,
                memberId: member.id,
                jobId: contResData.jobId,
                contractId: contResData.id,
                type: 'ContractDetails',
                userType: (member && member.jobSeeker && member.jobSeeker.title) ? "jobSeeker" : "jobManager",
                notificationObject: { contractId: contResData.id }
              },
              notification: {
                title: 'Hoobie',
                body: 'Congratulations! Your proposal has been selected for  ' + (proposalData && proposalData.job && proposalData.job.title || ''),
                priority: "high",
                sound: "default",
                vibrate: true,
              }
            };
            await this.fcmServices.sendNotification({ message: message })
            let notification = {
              title: "Congratulations Your proposal has been selected ",
              message: 'Congratulations! Your proposal has been selected for  ' + (proposalData && proposalData.job && proposalData.job.title || ''),
              memberId: proposalData.memberId,
              type: "ContractDetails",
              userType: (member && member.jobSeeker && member.jobSeeker.title) ? "jobSeeker" : "jobManager",
              notificationObject: { contractId: contResData.id }
            }
            await this.notificationRepository.create(notification)
            await this.controllerService.rejectMembersProposalNotification(proposalData)
          } else {
            throw new HttpErrors.NotFound("Fcm token not found!")
          }
          return contResData
        }
      }

    } else {
      throw new HttpErrors.NotFound("Proposal Not Found")

    }
  }

  @post('/contracts/searchContract', {
    responses: {
      '200': {
        description: 'Array of Contract model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Contract) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async searchContract(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    let resContract: Array<AnyObject> = [];
    let resContractData: AnyObject = {}
    let jobIds: Array<any> = [];
    let memberId: Array<any> = [];
    let query: AnyObject = { and: [] }
    let jobQuery: AnyObject = {}
    query.and.push({
      state: { inq: ['active', 'close', 'complete', 'suspend', 'hold'] }
    })
    if (data) {
      if (data && data.title) {
        jobQuery = {
          title: new RegExp('.*' + data.title + '.*', 'i')
        };

        const jobs = await this.jobRepository.find({
          where: jobQuery
        });
        jobIds = _.map(jobs, (v) => { return String(v.id); });
        if (jobIds && jobIds.length) {
          query.and.push({
            jobId: { inq: jobIds }
          })
        }
      }
      if (data && data.name) {
        jobQuery = {
          name: new RegExp('.*' + data.name + '.*', 'i')
        };
        const members = await this.memberRepository.find({
          where: jobQuery
        });
        memberId = _.map(members, (w) => { return String(w.id) })
        if (memberId && memberId.length) {
          query.and.push({
            clientId: { inq: memberId }
          })
        }
      }

      if (data && data.state) {
        query.and.push({
          state: new RegExp('.*' + data.state + '.*', 'i')
        })
      }
    }
    const contracts = await this.contractRepository.find({
      where: query,
      order: [data.order],
      fields: { id: true, jobId: true, resourceId: true, amount: true, clientId: true, created: true, time: true, state: true }
    });
    if (contracts && contracts.length) {
      let cJobIds = _.map(contracts, v => { return v.jobId; });
      let cMemberIds = _.map(contracts, v => { return v.resourceId; });
      let cClientIds = _.map(contracts, v => { return v.clientId; });

      return await Promise.all([
        this.jobRepository.find({
          where: {
            id: { inq: cJobIds }
          },
          fields: { id: true, title: true }
        }).then(res => res),
        this.memberRepository.find({
          where: {
            id: { inq: cMemberIds }
          },
          fields: { id: true, name: true, image: true }
        }).then(res => res),
        this.memberRepository.find({
          where: {
            id: { inq: cClientIds }
          },
          fields: { id: true, name: true, image: true }
        }).then(res => res)
      ]).then((res: any) => {

        const jobGroup = res && res[0] && res[0].length && _.groupBy(res[0], function (v) { return v.id; });
        const memberGroup = res && res[1] && res[1].length && _.groupBy(res[1], function (v) { return v.id; });
        const clientGroup = res && res[2] && res[2].length && _.groupBy(res[2], function (v) { return v.id; });

        _.forEach(contracts, (val: Contract) => {
          let obj: AnyObject = Object.assign({}, val);
          obj.job = jobGroup && jobGroup[val.jobId] && jobGroup[val.jobId][0] || {};
          obj.resource = memberGroup && memberGroup[val.resourceId] && memberGroup[val.resourceId][0] || {};
          obj.client = clientGroup && clientGroup[val.clientId] && clientGroup[val.clientId][0] || {};

          resContract.push(obj);
        });
        let totalCount = resContract && resContract.length || 0
        let start = data && data.skip || 0

        if (data && data.sorting === 'title') {
          resContract = _.orderBy(resContract, function (e) { return _.lowerCase(e.job.title) }, [data.sortBy]);
          resContract = _.slice(resContract, start, start + data.limit)
          resContractData = {
            contractCount: totalCount,
            data: resContract
          }
          return resContractData;
        } else if (data && data.sorting === 'pTitle') {
          resContract = _.orderBy(resContract, function (e) { return _.lowerCase(e.resource.name) }, [data.sortBy]);
          resContract = _.slice(resContract, start, start + data.limit)
          resContractData = {
            contractCount: totalCount,
            data: resContract
          }
          return resContractData;
        } else if (data && data.sorting === 'mName') {
          resContract = _.orderBy(resContract, function (e) { return _.lowerCase(e.client.name) }, [data.sortBy]);
          resContract = _.slice(resContract, start, start + data.limit)
          resContractData = {
            contractCount: totalCount,
            data: resContract
          }
          return resContractData;
        } else {
          resContract = _.slice(resContract, start, start + data.limit)
          resContractData = {
            contractCount: totalCount,
            data: resContract
          }
          return resContractData;
        }
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

  @post('/contracts/getContractList', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Contract model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Contract) } },
      },
    },
  })
  @authenticate('jwt')
  async getContractList(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    let query: AnyObject = {}
    if (data && data.memberType === 'jobManager') {
      query = {
        state: { neq: "complete" },
        clientId: currentUser[securityId]
      }
    } else if (data && data.memberType === 'jobSeeker') {
      query = {
        state: { neq: "complete" },
        resourceId: currentUser[securityId]
      }
    }
    const constratc = await this.contractRepository.find({
      where: query,
      order: ['created DESC'],
      include: [
        { relation: "job" }
      ]
    });
    return constratc
  }
}
