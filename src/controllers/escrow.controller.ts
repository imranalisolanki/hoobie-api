import { Count, CountSchema, Filter, repository, Where, AnyObject, } from '@loopback/repository';
import { post, param, get, getFilterSchemaFor, getModelSchemaRef, getWhereSchemaFor, patch, put, del, requestBody, HttpErrors, } from '@loopback/rest';
import { Escrow } from '../models';
import { EscrowRepository, MemberRepository, JobRepository, PaymentRepository, ContractRepository, JobWorksRepository, NotificationRepository } from '../repositories';
import { OPERATION_SECURITY_SPEC } from '../utils/security-spec';
import { authenticate } from '@loopback/authentication';
import { inject } from '@loopback/core';
import { SecurityBindings, UserProfile, securityId } from '@loopback/security';
import * as Stripe from 'stripe';
import * as common from '../services/common';
import * as _ from 'lodash';
import { ObjectID } from 'mongodb';
import { throws } from 'assert';
import { assign } from 'nodemailer/lib/shared';
import { FCMServiceBindings } from '../keys';
import { FCMService } from '../services';
import moment = require('moment');

export class EscrowController {

  constructor(
    @repository(EscrowRepository) public escrowRepository: EscrowRepository,
    @repository(MemberRepository) public memberRepository: MemberRepository,
    @repository(JobRepository) public jobRepository: JobRepository,
    @repository(PaymentRepository) public paymentRepository: PaymentRepository,
    @repository(ContractRepository) public contractRepository: ContractRepository,
    @repository(JobWorksRepository) public jobworkRepository: JobWorksRepository,
    @inject(FCMServiceBindings.FCM_SERVICE) public fcmServices: FCMService,
    @repository(NotificationRepository) public notificationRepository: NotificationRepository
  ) {

  }

  @post('/escrows', {
    responses: {
      '200': {
        description: 'Escrow model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Escrow) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Escrow, {
            title: 'NewEscrow',
            exclude: ['id'],
          }),
        },
      },
    })
    escrow: Omit<Escrow, 'id'>,
  ): Promise<Escrow> {
    return this.escrowRepository.create(escrow);
  }

  @get('/escrows/count', {
    responses: {
      '200': {
        description: 'Escrow model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Escrow)) where?: Where<Escrow>,
  ): Promise<Count> {
    return this.escrowRepository.count(where);
  }

  @get('/escrows', {
    responses: {
      '200': {
        description: 'Array of Escrow model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Escrow) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Escrow)) filter?: Filter<Escrow>,
  ): Promise<Escrow[]> {
    return this.escrowRepository.find(filter);
  }

  @patch('/escrows', {
    responses: {
      '200': {
        description: 'Escrow PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Escrow, { partial: true }),
        },
      },
    })
    escrow: Escrow,
    @param.query.object('where', getWhereSchemaFor(Escrow)) where?: Where<Escrow>,
  ): Promise<Count> {
    return this.escrowRepository.updateAll(escrow, where);
  }

  @get('/escrows/{id}', {
    responses: {
      '200': {
        description: 'Escrow model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Escrow) } },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Escrow> {
    return this.escrowRepository.findById(id);
  }

  @patch('/escrows/{id}', {
    responses: {
      '204': {
        description: 'Escrow PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Escrow, { partial: true }),
        },
      },
    })
    escrow: Escrow,
  ): Promise<void> {
    await this.escrowRepository.updateById(id, escrow);
  }

  @put('/escrows/{id}', {
    responses: {
      '204': {
        description: 'Escrow PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() escrow: Escrow,
  ): Promise<void> {
    await this.escrowRepository.replaceById(id, escrow);
  }

  @del('/escrows/{id}', {
    responses: {
      '204': {
        description: 'Escrow DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.escrowRepository.deleteById(id);
  }

  @post('/escrows/charge', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Escrow model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Escrow) } },
      },
    },
  })
  @authenticate('jwt')
  async charge(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<Escrow> {
    const siteOption = common.getSiteOptions();
    const stripe = new Stripe(siteOption.stripe.secretKey);
    const clientCharge = siteOption.charges.clientCharge;

    const member = await this.memberRepository.findOne({
      where: {
        id: currentUser[securityId],
        status: 1
      },
      fields: { id: true, stripeCustomerId: true, email: true, fcmToken: true },
    });

    if (member && member.stripeCustomerId) {
      if (data && data.amount && data.amount > 0) {
        let taxAmount = (data.amount * clientCharge) / 100;
        let amount = (data.amount + taxAmount);
        // const stripeAmount = common.calculateStripeFee(amount)
        // let totalClientAmount = stripeAmount.total;
        let chargeAmount = Math.round(amount * 100);
        try {
          const stripeCharge = await stripe.charges.create({
            amount: chargeAmount,
            currency: 'aud',
            customer: member.stripeCustomerId,
            metadata: {
              contractId: data.contractId,
              jobId: data.jobId
            }
          });

          if (stripeCharge && stripeCharge.id && stripeCharge.status === 'succeeded') {
            let escrowObj: Partial<Escrow> = {
              memberId: currentUser[securityId],
              amount: data.amount,
              jobId: data.jobId,
              chargeId: stripeCharge.id,
              stripeCharges: 0,
              breezeCharge: taxAmount,
              totalPaidamount: amount,
              escroCreatedAt: moment().add(+7, 'days').toISOString(),
              state: 'success'
            }
            if (member && member.fcmToken) {
              var message = {
                to: member.fcmToken,
                data: {
                  name: member.name,
                  memberId: member.id,
                  type: 'paymentCredit'
                },
                notification: {
                  title: 'welcome Hoobie',
                  body: "Your escrow debit sucessfully!",
                  priority: "high",
                  sound: "default",
                  vibrate: true,
                }
              };
              await this.fcmServices.sendNotification({ message: message })
            }
            return this.escrowRepository.create(escrowObj);
          } else {
            throw new HttpErrors.UnprocessableEntity('Error in processing stripe charge.')
          }
        } catch (err) {
          /* switch (err.type) {
            case 'StripeCardError':
              // A declined card error
              err.message; // => e.g. "Your card's expiration year is invalid."
              break;
            case 'StripeRateLimitError':
              // Too many requests made to the API too quickly
              break;
            case 'StripeInvalidRequestError':
              // Invalid parameters were supplied to Stripe's API
              break;
            case 'StripeAPIError':
              // An error occurred internally with Stripe's API
              break;
            case 'StripeConnectionError':
              // Some kind of error occurred during the HTTPS communication
              break;
            case 'StripeAuthenticationError':
              // You probably used an incorrect API key
              break;
            default:
              // Handle any other types of unexpected errors
              break;
          } */
          throw err;
        }
      } else {
        throw new HttpErrors.NotFound('amount should be greater then zero.');
      }
    } else {
      throw new HttpErrors.Unauthorized('Member not found');
    }
  }

  @get('/escrows/escrowsReports', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Escrow model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Escrow) } },
      },
    },
  })
  @authenticate('jwt')
  async escrowsReports(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
  ): Promise<any> {
    let reportDetails: Array<AnyObject> = [];
    let refundPayment: Array<AnyObject> = [];
    let paidPayments: Array<AnyObject> = [];

    const escrowDetails = await this.escrowRepository.find({
      where: {
        memberId: currentUser[securityId],
        state: "success"
      }, fields: { id: true, memberId: true, jobId: true, created: true, amount: true, isReleased: true, refunded: true, state: true },
      order: ['created DESC'],
      include: [
        { relation: "job" }
      ]
    })

    if (escrowDetails && escrowDetails.length) {
      let jobId = _.map(escrowDetails, v => v.jobId)

      return await Promise.all([
        this.paymentRepository.find({
          where: {
            byMemberId: currentUser[securityId],
            state: { inq: ["success", "refunded"] }
          }, fields: { id: true, amount: true, memberId: true, jobWorkId: true, jobId: true, created: true, modified: true, state: true, refunded: true },
          order: ['created DESC'],
          include: [
            { relation: "job" },
            { relation: "contract" }
          ]
        }),
        this.contractRepository.find({
          where: {
            jobId: { inq: jobId }
          }
        })
      ]).then(async (res: any) => {

        let payments = res && res[0] || []
        let contract = res && res[1] || []
        let contractData = contract && contract.length && _.groupBy(contract, function (val: any) { return val.jobId })
        let jobWorkIds = _.map(payments, v => v.jobWorkId)

        let jobWorkDetails = await this.jobworkRepository.find({
          where: {
            id: { inq: jobWorkIds }
          }
        })
        let jobWorkData = jobWorkDetails && jobWorkDetails && jobWorkDetails.length && _.groupBy(jobWorkDetails, function (val: any) { return val.id })
        const totalEscroAmount = _.sumBy(escrowDetails, function (v: any) { return v.amount });

        _.forEach(payments, function (val: any) {
          if (val && val.state && val.state === 'refunded') {
            refundPayment.push(val)
          }
          let obj: AnyObject = Object.assign({}, val);
          obj.jobWorkDetails = jobWorkData && jobWorkData[val.jobWorkId!] && Object.assign({}, jobWorkData[val.jobWorkId!][0]) || {};
          paidPayments.push(obj)
        })

        let refund = _.sumBy(refundPayment, v => v.amount);
        const paidPayment = _.sumBy(payments, function (val: any) { return val.amount });

        _.forEach(escrowDetails, function (val: any) {
          let obj: AnyObject = Object.assign({}, val);
          obj.contractDetails = contractData && contractData[val.jobId!] && Object.assign({}, contractData[val.jobId!][0]) || {};
          reportDetails.push(obj);
        })
        let escroReport = {
          ecsroCount: escrowDetails && escrowDetails.length || 0,
          reportDetails,
          escroAmount: totalEscroAmount,
          totalPaidAmount: (paidPayment - refund),
          paidPaymentData: paidPayments
        }
        return escroReport
      })
    } else {
      const payments = await this.paymentRepository.find({
        where: {
          byMemberId: currentUser[securityId],
          state: { inq: ["success", "refunded"] }
        }, fields: { id: true, amount: true, memberId: true, jobId: true, created: true, modified: true, state: true, refunded: true },
        order: ['created DESC'],
        include: [
          { relation: "job" },
          { relation: "contract" }
        ]
      })
      _.forEach(payments, function (val: any) {
        if (val && val.state && val.state === 'refunded') {
          refundPayment.push(val)
        }
      })

      let refund = _.sumBy(refundPayment, v => v.amount);
      const paidPayment = _.sumBy(payments, function (val: any) { return val.amount });
      let escroReport = {
        ecsroCount: escrowDetails && escrowDetails.length || 0,
        reportDetails,
        escroAmount: 0,
        totalPaidAmount: (paidPayment - refund),
        paidPaymentData: payments
      }
      return escroReport
    }
  }

  @post('/escrows/searchescrows', {
    responses: {
      '200': {
        description: 'Array of escrows model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Escrow) },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async searchescrows(
    @inject(SecurityBindings.USER) currentUser: UserProfile,
    @requestBody() data: AnyObject,
  ): Promise<any> {
    let resEscrows: Array<AnyObject> = [];
    let resescrowsData: AnyObject = [];
    let jobIds: Array<any> = [];
    let memberId: Array<any> = [];
    let query: AnyObject = { and: [] }
    let jobQuery: AnyObject = {}
    query.and.push({
      status: { inq: [0, 1] }
    })

    if (data) {
      if (data && data.title) {
        jobQuery = {
          title: new RegExp('.*' + data.title + '.*', 'i')
        }
        const jobs = await this.jobRepository.find({
          where: jobQuery
        });

        jobIds = _.map(jobs, (v) => { return String(v.id); });
        if (jobIds && jobIds.length) {
          query.and.push({
            jobId: { inq: jobIds }
          })
        }
      }
      if (data && data.name) {
        jobQuery = {
          name: new RegExp('.*' + data.name + '.*', 'i')
        }
        const members = await this.memberRepository.find({
          where: jobQuery
        });
        memberId = _.map(members, (w) => { return String(w.id) })
        if (memberId && memberId.length > 0) {
          query.and.push({
            memberId: { inq: memberId }
          })
        }
      }
    }

    if (data && data.state) {
      query.and.push({
        state: new RegExp('.*' + data.state + '.*', 'i')
      })
    }
    const Escrow = await this.escrowRepository.find({
      where: query,
      order: [data.order],
    });

    if (Escrow && Escrow.length) {
      let cJobIds = _.map(Escrow, v => { return v.jobId; });
      let cMemberIds = _.map(Escrow, v => { return v && v.memberId != '' && v.memberId || v.memberId; });
      return await Promise.all([
        this.jobRepository.find({
          where: {
            id: { inq: cJobIds }
          },
          fields: { id: true, title: true }
        }).then(res => res),
        this.memberRepository.find({
          where: {
            id: { inq: cMemberIds }
          },
          fields: { id: true, name: true, image: true }
        }).then(res => res)
      ]).then((res: any) => {
        const jobGroup = res && res[0] && res[0].length && _.groupBy(res[0], function (v) { return v.id; });
        const memberGroup = res && res[1] && res[1].length && _.groupBy(res[1], function (v) { return v.id; });
        _.forEach(Escrow, (val: Escrow) => {
          let obj: AnyObject = Object.assign({}, val);
          obj.job = jobGroup && jobGroup[val.jobId] && jobGroup[val.jobId][0] || {};
          obj.member = memberGroup && memberGroup[val.memberId] && memberGroup[val.memberId][0] || memberGroup && memberGroup[val.memberId] && memberGroup[val.memberId][0] || {}

          resEscrows.push(obj);
        });
        let totalCount = resEscrows && resEscrows.length || 0
        let totalamount = _.sumBy(resEscrows, function (v) { return v.amount })
        let totalClientsPaidAmount = _.sumBy(resEscrows, function (v) { return v.totalPaidamount })
        let totalChargeAmount = _.sumBy(resEscrows, function (v) { return v.breezeCharge })
        let start = data && data.skip || 0

        if (data && data.sorting === 'title') {
          resEscrows = _.orderBy(resEscrows, function (e) { return _.lowerCase(e.job.title) }, [data.sortBy]);
          resEscrows = _.slice(resEscrows, start, start + data.limit)
          resescrowsData = {
            escroCount: totalCount,
            ecsroTotalAmount: totalamount,
            totalClientsPaidAmount: totalClientsPaidAmount,
            totalChargeAmount: totalChargeAmount,
            data: resEscrows
          }
          return resescrowsData;
        } else if (data && data.sorting === 'cname') {

          resEscrows = _.orderBy(resEscrows, function (e) { return _.lowerCase(e.member.name) }, [data.sortBy]);
          resEscrows = _.slice(resEscrows, start, start + data.limit)
          resescrowsData = {
            escroCount: totalCount,
            ecsroTotalAmount: totalamount,
            totalClientsPaidAmount: totalClientsPaidAmount,
            totalChargeAmount: totalChargeAmount,
            data: resEscrows
          }
          return resescrowsData;
        } else {
          resEscrows = _.slice(resEscrows, start, start + data.limit)
          resescrowsData = {
            escroCount: totalCount,
            ecsroTotalAmount: totalamount,
            totalClientsPaidAmount: totalClientsPaidAmount,
            totalChargeAmount: totalChargeAmount,
            data: resEscrows
          }
          return resescrowsData;
        }
      }).catch((err: any) => {
        throw new HttpErrors.InternalServerError('Something went wrong!')
      });
    } else {
      return [];
    }
  }

  @get('/escrows/getEscrowsPayments', {
    responses: {
      '200': {
        description: 'Escrow model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Escrow) } },
      },
    },
  })
  async getEscrowsPayments(
  ): Promise<any> {
    let contractData: Array<AnyObject> = []
    const siteOption = common.getSiteOptions();
    const stripe = new Stripe(siteOption.stripe.secretKey);
    const clientCharge = siteOption.charges.clientCharge;

    const contractDetail = await this.contractRepository.find({
      where: {
        state: "active"
      }
    })

    if (contractDetail && contractDetail.length) {
      let jobIds = _.map(contractDetail, v => v.jobId)
      let clientIds = _.map(contractDetail, v => v.clientId)

      return Promise.all([
        this.jobRepository.find({
          where: {
            id: { inq: jobIds },
          }, fields: { id: true, jobObject: true, jobType: true, timePeriod: true, timeSlots: true, title: true, budget: true }
        }),
        this.memberRepository.find({
          where: {
            id: { inq: clientIds }
          }, fields: { id: true, jobManager: true, jobSeeker: true, stripeAccount: true, stripeCustomerId: true, fcmToken: true }
        }),
        this.escrowRepository.find({
          where: {
            jobId: { inq: jobIds }
          }, fields: { id: true, jobId: true, memberId: true, amount: true, escroCreatedAt: true },
          order: ['created DESC']
        }),
        this.paymentRepository.find({
          where: {
            jobId: { inq: jobIds },
            state: { inq: ['pending', 'success'] }
          }, fields: { id: true, jobId: true, state: true, amount: true }
        }),
        this.jobworkRepository.find({
          where: {
            jobId: { inq: jobIds },
            isReleased: false
          }
        })
      ]).then(async res => {
        let jobs = res && res[0] || []
        let clients = res && res[1] || []
        let escrows = res && res[2] || []
        let payments = res && res[3] || []
        let jobWorks = res && res[4] || []
        let job = jobs && jobs.length && _.groupBy(jobs, v => v.id);
        let client = clients && clients.length && _.groupBy(clients, v => v.id);
        let escrow = escrows && escrows.length && _.groupBy(escrows, v => v.jobId);
        let payment = payments && payments.length && _.groupBy(payments, v => v.jobId)
        let jobWork = jobWorks && jobWorks.length && _.groupBy(jobWorks, v => v.jobId)

        _.forEach(contractDetail, function (val: any) {
          let obj: AnyObject = Object.assign({}, val)
          obj.jobDetails = job && job[val.jobId!] && Object.assign({}, job[val.jobId!][0]) || {};
          obj.clientDetails = client && client[val.clientId!] && Object.assign({}, client[val.clientId!][0]) || {};
          obj.escrowDetails = escrow && escrow[val.jobId!] && Object.assign({}, escrow[val.jobId!][0]) || {};
          obj.escrows = escrow && escrow[val.jobId!] || [];
          obj.paymentDetails = payment && payment[val.jobId!] || [];
          obj.jobWorkDetails = jobWork && jobWork[val.jobId!] || [];

          contractData.push(obj)
        })
        //return contractData
        let _this = this;
        let jobWorkPayments = 0
        _.forEach(contractData, async function (val: any, index) {
          if (val && val.jobDetails && val.jobDetails.jobType === "Budget Contract" || val.jobDetails.jobType === "Hourly Contract") {
            //if (val && val.jobWorkDetails && val.jobWorkDetails.length) {
            if (val.jobDetails.jobType === "Hourly Contract") {

              if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.selectedHoursType === "Monthly Hours") {

                let escrowDate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+7, 'days').endOf('day').unix()
                let currentDate = moment().startOf('day').unix()

                let totalDays = (Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.timeUnitAndQuantity && val.jobDetails.jobObject.timeUnitAndQuantity.quantity) * 30)
                if (escrowDate > currentDate) {
                  let perWeekPayment = Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.unitHours) / 30

                  jobWorkPayments = (val && val.jobDetails && val.jobDetails.budget) * (perWeekPayment) * 7
                }
              }
              else if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.selectedHoursType === "Weekly Hours") {

                let escrowDate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+7, 'days').unix()
                let currentDate = moment().startOf('day').unix()

                let totalDays = (Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.timeUnitAndQuantity && val.jobDetails.jobObject.timeUnitAndQuantity.quantity) * 7)
                let expDate = moment().add(+totalDays, 'days').unix()
                if (escrowDate > currentDate && expDate >= currentDate) {

                  //let perWeekPayment = Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.unitHours) / totalDays
                  let unitHours = Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.unitHours) * 7
                  let totalPayments = (val && val.jobDetails && val.jobDetails.budget) * unitHours
                  jobWorkPayments = totalPayments
                }
              }

              else if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.selectedHoursType === "Yearly Hours") {

                let escrowDate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+7, 'days').unix()
                let currentDate = moment().startOf('day').unix()

                if (escrowDate > currentDate) {

                  let unitHours = Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.unitHours) * 7 / 365
                  let totalPayments = (val && val.jobDetails && val.jobDetails.budget) * unitHours
                  jobWorkPayments = totalPayments
                }
              }

              else if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.selectedHoursType === "Fortnight Hours") {

                let escrowDate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+7, 'days').unix()
                let currentDate = moment().startOf('day').unix()

                let totalDays = (Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.timeUnitAndQuantity && val.jobDetails.jobObject.timeUnitAndQuantity.quantity) * 7)
                let expDate = moment().add(+totalDays, 'days').unix()
                if (escrowDate > currentDate && expDate >= currentDate) {

                  //let perWeekPayment = Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.unitHours) / totalDays
                  let unitHours = Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.unitHours) * 7 / 14
                  jobWorkPayments = (val && val.jobDetails && val.jobDetails.budget) * unitHours
                }
              }

              if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.selectedHoursType === "Daily Hours") {

                let totalDays = (Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.timeUnitAndQuantity && val.jobDetails.jobObject.timeUnitAndQuantity.quantity))

                let totalDaysPayment = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+totalDays, 'days').format('YYYY-MM-DD')
                let escrowDate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+7, 'days').endOf('day').unix()
                let currentDate = moment().startOf('day').unix()
                let payDays = moment(totalDaysPayment)
                let paidPaydate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).format('YYYY-MM-DD')
                let leftDays = moment(paidPaydate).diff(payDays, 'days')
                if (escrowDate > currentDate) {
                  let perDaysPayment = 0
                  if (leftDays > 7) {
                    perDaysPayment * 7
                  } else {
                    perDaysPayment = leftDays
                  }
                  let perWeekPayment = Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.unitHours) * perDaysPayment

                  jobWorkPayments = (val && val.jobDetails && val.jobDetails.budget) * perWeekPayment
                }
              }

              if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.selectedHoursType === "Custom") {
                let escrowDate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+7, 'days').unix()
                let currentDate = moment().startOf('day').unix()

                if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.activeDays && val.jobDetails.jobObject.activeDays.length) {
                  if (escrowDate >= currentDate) {
                    let paymentDays = (val && val.jobDetails && val.jobDetails.budget) / Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.totalDaysCount)
                    let totalWeekPayment = (paymentDays) * (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.activeDays && val.jobDetails.jobObject.activeDays.length)
                    jobWorkPayments = totalWeekPayment
                  }
                } else {
                  if (escrowDate >= currentDate) {
                    let totalDaysPayment = Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.dailyHours) * 7
                    jobWorkPayments = (val && val.amount) * totalDaysPayment
                  }
                }

              }
            }
            if (val && val.jobDetails && val.jobDetails.jobType === "Budget Contract") {

              // if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.selectedHoursType === "Hours") {

              //   let escrowDate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+7, 'days').endOf('day').unix()
              //   let currentDate = moment().startOf('day').unix()
              //   if (currentDate > escrowDate) {
              //     jobWorkPayments = (val && val.jobDetails && val.jobDetails.budget / Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.totalHoursCount))
              //   }
              // }
              if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.selectedHoursType === "Weeks") {
                let startDate = moment(val && val.jobDetails && val.jobDetails.timePeriod && val.jobDetails.timePeriod.from).format("YYYY.MM.DD")
                let endDate = moment(val && val.jobDetails && val.jobDetails.timePeriod && val.jobDetails.timePeriod.to).format("YYYY.MM.DD")
                var startDate1 = moment(startDate);
                var endDate1 = moment(endDate);

                let diffrentDays = endDate1.diff(startDate1, 'days')

                let totalDays = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+diffrentDays, 'days').format('YYYY-MM-DD')
                let payDate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+7, 'days').unix()
                let escrowDate = moment().endOf('day').unix()
                let paymentDate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).format('YYYY-MM-DD')
                let paymentDate1 = moment(paymentDate)
                let totalDays1 = moment(totalDays)
                let leftDays = paymentDate1.diff(totalDays1, 'days')
                let piadDaysPayments = 0
                if (leftDays > 7) {
                  piadDaysPayments = 7
                } else {
                  piadDaysPayments = leftDays
                }
                if (escrowDate >= payDate) {
                  jobWorkPayments = (val && val.amount) / Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.timeUnitAndQuantity && val.jobDetails.jobObject.timeUnitAndQuantity.quantity)
                }
              }

              if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.selectedHoursType === "Quarters") {

                //let totalDateDays = moment(val.escroCreatedAt).add(+Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.timeUnitAndQuantity && val.jobDetails.jobObject.timeUnitAndQuantity.quantity) - 1 * 30, 'days').endOf('day').unix()
                let payDate = moment(val.escroCreatedAt).add(+7, 'days').unix()
                let escrowDate = moment().unix()
                if (escrowDate >= payDate) {
                  let totalQuarters = Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.timeUnitAndQuantity && val.jobDetails.jobObject.timeUnitAndQuantity.quantity) * 7 / 90
                  let quartersPAyment = (val && val.amount) * totalQuarters

                  jobWorkPayments = quartersPAyment
                }
              }
              if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.selectedHoursType === "Days") {

                //let startDate = moment(val && val.jobDetails && val.jobDetails.timePeriod && val.jobDetails.timePeriod.from)
                // let endDate = moment(val && val.jobDetails && val.jobDetails.timePeriod && val.jobDetails.timePeriod.to)

                // let diffrentDays = endDate.diff(startDate, 'days')
                let escrowDate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+7, 'days').unix()
                let currentDate = moment().startOf('day').unix()

                if (escrowDate >= currentDate) {
                  jobWorkPayments = (val && val.amount / Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.timeUnitAndQuantity && val.jobDetails.jobObject.timeUnitAndQuantity.quantity)) * 7
                }
              }
              if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.selectedHoursType === "Yearly Hours") {

                let escrowDate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+7, 'days').unix()
                let currentDate = moment().startOf('day').unix()

                if (escrowDate > currentDate) {

                  let unitHours = Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.unitHours) * 7 / 365
                  let totalPayments = (val && val.jobDetails && val.jobDetails.budget) * unitHours
                  jobWorkPayments = totalPayments
                }
              }

              if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.selectedHoursType === "Custom Range") {
                let escrowDate = moment(val.escrowDetails && val.escrowDetails.escroCreatedAt).add(+7, 'days').unix()
                let currentDate = moment().startOf('day').unix()

                if (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.activeDays && val.jobDetails.jobObject.activeDays.length) {
                  if (escrowDate >= currentDate) {
                    let paymentDays = (val && val.jobDetails && val.jobDetails.budget) / Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.totalDaysCount)
                    let totalWeekPayment = (paymentDays) * (val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.activeDays && val.jobDetails.jobObject.activeDays.length)
                    jobWorkPayments = totalWeekPayment
                  }
                } else {
                  if (escrowDate >= currentDate) {
                    jobWorkPayments = (val && val.amount / Number(val && val.jobDetails && val.jobDetails.jobObject && val.jobDetails.jobObject.totalDaysCount)) * 7
                  }
                }

              }
            }
            let paidPayment = _.sumBy(val.paymentDetails, function (w: any) { return w.amount })
            let escrowAmount = _.sumBy(val.escrows, function (v: any) { return v.amount })
            let escrowLeftAmount = escrowAmount - paidPayment
            let getEscrowAmount = jobWorkPayments - escrowLeftAmount

            if (getEscrowAmount && getEscrowAmount > 0 && paidPayment > 0) {
              let taxAmount = (getEscrowAmount * clientCharge) / 100;
              let amount = (getEscrowAmount + taxAmount);
              let chargeAmount = Math.round(amount * 100);
              try {
                const stripeCharge = await stripe.charges.create({
                  amount: chargeAmount,
                  currency: 'aud',
                  customer: val && val.clientDetails && val.clientDetails.stripeCustomerId,
                  metadata: {
                    contractId: val && val.id,
                    jobId: val && val.jobDetails && val && val.jobDetails.id
                  }
                });

                if (stripeCharge && stripeCharge.id && stripeCharge.status === 'succeeded') {
                  let escrowObj = {
                    memberId: val && val.clientDetails && val.clientDetails.id,
                    amount: getEscrowAmount,
                    jobId: val && val.jobDetails && val && val.jobDetails.id,
                    chargeId: stripeCharge.id,
                    stripeCharges: 0,
                    breezeCharge: taxAmount,
                    totalPaidamount: amount,
                    escroCreatedAt: moment().add(+7, 'days').endOf('day').toISOString(),
                    state: 'success'
                  }
                  if (val && val.clientDetails && val.clientDetails.fcmToken) {
                    var message = {
                      to: val && val.clientDetails && val.clientDetails.fcmToken,
                      data: {
                        memberId: val && val.clientDetails && val.clientDetails.id,
                        jobId: val && val.jobDetails && val && val.jobDetails.id,
                        type: 'debit Escrow',
                        userType: "jobManager",
                        notificationObject: { contractId: val.id }
                      },
                      notification: {
                        title: 'Hoobie',
                        body: "Escrow payment debit successfully",
                        priority: "high",
                        sound: "default",
                        vibrate: true,
                      }
                    };
                    await _this.fcmServices.sendNotification({ message: message })
                    let notification = {
                      title: "Escrow payment debit successfully",
                      message: "Your escrow payment debit successfully for " + (val && val.jobDetails && val && val.jobDetails.title),
                      memberId: val && val.clientDetails && val.clientDetails.id,
                      type: "debit Escrow",
                      userType: "jobManager",
                      notificationObject: { contractId: val.id }
                    }
                    await _this.notificationRepository.create(notification)
                  }
                  return _this.escrowRepository.create(escrowObj);
                } else {
                  if (val && val.clientDetails && val.clientDetails.fcmToken) {
                    var message = {
                      to: val && val.clientDetails && val.clientDetails.fcmToken,
                      data: {
                        memberId: val && val.clientDetails && val.clientDetails.id,
                        jobId: val && val.jobDetails && val && val.jobDetails.id,
                        type: 'debit failed!',
                        userType: "jobManager",
                        notificationObject: { contractId: val.id }
                      },
                      notification: {
                        title: 'Hoobie',
                        body: "Escrow payment has been failed. please check your card details",
                        priority: "high",
                        sound: "default",
                        vibrate: true,
                      }
                    };
                    await _this.fcmServices.sendNotification({ message: message })
                    let notification = {
                      title: "Escrow payment debit failed!",
                      message: "Escrow payment has been failed. please check your card details!",
                      memberId: val && val.clientDetails && val.clientDetails.id,
                      type: "debit failed",
                      userType: "jobManager",
                      notificationObject: { contractId: val.id }
                    }
                    await _this.notificationRepository.create(notification)
                    console.log('Error in processing stripe charge.')
                  }
                }
              } catch (err) {
                console.log('Error catch.')
              }

            } else {
              console.log('amount should be greater then zero.')
              //throw new HttpErrors.NotFound('amount should be greater then zero.');
            }
            //}
          }
        })
      })
    } else {
      throw new HttpErrors.NotFound("Contracts not found")
    }
  }

}
