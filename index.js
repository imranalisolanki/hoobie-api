const application = require('./dist');
const configOption = require(`./dist/config/config${process.env.NODE_ENV || ''}.json`);
const fs = require('fs')

module.exports = application;

if (require.main === module) {
  // Run the application
  const config = {
    rest: {
      protocol: 'https',
      key: fs.readFileSync('../pem-cert/hoobie.com.au-key.pem', 'utf8'),
      cert: fs.readFileSync('../pem-cert/hoobie.com.au-crt.pem', 'utf8'),
      ca: fs.readFileSync('../pem-cert/hoobie.bundle-crt.pem', 'utf8'),
      host: configOption.host,
      port: configOption.port,
      openApiSpec: {
        // useful when used with OpenAPI-to-GraphQL to locate your application
        setServersFromRequest: true,
      },
    },
  };
  application.main(config).catch(err => {
    console.error('Cannot start the application.', err);
    process.exit(1);
  });
}
